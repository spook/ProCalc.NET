﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Grammar
{
    class ExpressionGrammar : Irony.Parsing.Grammar
    {
        public const string UNARY_OPERATOR = "UnaryOperator";
        public const string UNARY_OPERATION = "UnaryOperation";
        public const string BINARY_OPERATOR = "BinaryOperator";
        public const string BINARY_OPERATION = "BinaryOperation";
        public const string VARIABLE = "Variable";
        public const string EXTERNAL_VARIABLE = "ExternalVariable";
        public const string ARGUMENTS = "Arguments";
        public const string FUNCTION_CALL = "FunctionCall";
        public const string MATRIX_ROW = "MatrixRow";
        public const string MATRIX_ROWS = "MatrixRows";
        public const string MATRIX = "Matrix";
        public const string LIST_ENTRIES = "ListEntries";
        public const string LIST = "List";
        public const string TERM = "Term";
        public const string SCALAR = "Scalar";
        public const string PARENTHESIS = "Parenthesis";
        public const string EXPRESSION = "Expression";
        public const string ROOT = "Root";
        public const string VARIABLE_ASSIGNMENT = "VariableAssignment";
        public const string FUNCTION_ASSIGNMENT = "FunctionAssignment";
        public const string INDEXER = "Indexer";
        public const string INDICES = "Indices";
        public const string INTEGER = "Integer";
        public const string REAL = "Real";
        public const string COMPLEX = "Complex";
        public const string SYS_INTEGER = "SysInteger";
        public const string DMS = "Dms";
        public const string STRING = "String";
        public const string BIG_NUM = "BigNum";
        public const string COMMA = "Comma";
        public const string SEMICOLON = "Semicolon";
        public const string CALCULATION = "Calculation";
        public const string IDENTIFIER = "Identifier";
        public const string CALL_PARAM = "CallParam";
        public const string VECTOR = "Vector";
        public const string REST_OF_COORDS = "RestOfCoords";

        public ExpressionGrammar()
        {
            // Construct non-terminals

            var unaryOperator = new NonTerminal(UNARY_OPERATOR);
            var unaryOperation = new NonTerminal(UNARY_OPERATION);
            var binaryOperator = new NonTerminal(BINARY_OPERATOR);
            var binaryOperation = new NonTerminal(BINARY_OPERATION);
            var variable = new NonTerminal(VARIABLE);
            var externalVariable = new NonTerminal(EXTERNAL_VARIABLE);

            var arguments = new NonTerminal(ARGUMENTS);
            var functionCall = new NonTerminal(FUNCTION_CALL);
            var matrixRow = new NonTerminal(MATRIX_ROW);
            var matrixRows = new NonTerminal(MATRIX_ROWS);
            var matrix = new NonTerminal(MATRIX);
            var listEntries = new NonTerminal(LIST_ENTRIES);
            var list = new NonTerminal(LIST);
            var term = new NonTerminal(TERM);
            var scalar = new NonTerminal(SCALAR);
            var parenthesis = new NonTerminal(PARENTHESIS);
            var expression = new NonTerminal(EXPRESSION);            
            var root = new NonTerminal(ROOT);
            var variableAssignment = new NonTerminal(VARIABLE_ASSIGNMENT);
            var functionAssignment = new NonTerminal(FUNCTION_ASSIGNMENT);
            var calculation = new NonTerminal(CALCULATION);
            var indexer = new NonTerminal(INDEXER);
            var indices = new NonTerminal(INDICES);
            var vector = new NonTerminal(VECTOR);
            var restOfCoords = new NonTerminal(REST_OF_COORDS);

            // Construct terminals

            var integer = new RegexBasedTerminal(INTEGER, "[0-9]+");
            var real = new RegexBasedTerminal(REAL, @"([0-9]+\.[0-9]+)|([0-9]+(\.[0-9]+)?e[\+\-]?[0-9]+)");
            var complex = new RegexBasedTerminal(COMPLEX, @"(([0-9]+(\.[0-9]+)?)|([0-9]+(\.[0-9]+)?e[\+\-]?[0-9]+))i");
            var sysInteger = new RegexBasedTerminal(SYS_INTEGER, "0((b[0-1]+)|(o[0-7]+)|(d[0-9]+)|(h[0-9a-fA-F]+))");
            var dms = new RegexBasedTerminal(DMS, @"[0-9]+:[0-5]?[0-9](:[0-5]?[0-9](\.[0-9]+)?)?");
            var @string = new RegexBasedTerminal(STRING, @"""(([^""])|(""""))*""");
            var bignum = new RegexBasedTerminal(BIG_NUM, @"[0-9]+(\.[0-9]+)?[lL]");
            var comma = ToTerm(",", COMMA);
            var semicolon = ToTerm(";", SEMICOLON);

            var identifier = new RegexBasedTerminal(IDENTIFIER, @"[a-zA-Z_@][a-zA-Z0-9\._@]*");
            var callparam = new RegexBasedTerminal(CALL_PARAM, @"_[0-9]+");
            var assign = ToTerm("=");

            // Configure non-terminals

            root.Rule = variableAssignment | functionAssignment | calculation;

            variableAssignment.Rule = identifier + assign + expression;

            functionAssignment.Rule = identifier + "(" + arguments + ")" + assign + expression;

            expression.Rule = term | unaryOperation | binaryOperation | indexer;

            term.Rule = scalar | @string | matrix | list | functionCall | variable | callparam | externalVariable | parenthesis | vector;
            scalar.Rule = integer | real | complex | sysInteger | dms | bignum;

            matrix.Rule = ToTerm("[") + matrixRows + "]";
            matrixRows.Rule = MakePlusRule(matrixRows, semicolon, matrixRow);
            matrixRow.Rule = MakePlusRule(matrixRow, comma, expression);

            list.Rule = ToTerm("{") + listEntries + "}";
            listEntries.Rule = MakePlusRule(listEntries, comma, expression);

            functionCall.Rule = identifier + "(" + arguments + ")";
            arguments.Rule = MakePlusRule(arguments, comma, expression);

            variable.Rule = identifier;

            externalVariable.Rule = ToTerm("$") + identifier;

            parenthesis.Rule = ToTerm("(") + expression + ")";

            vector.Rule = ToTerm("(") + expression + comma + restOfCoords + ")";
            restOfCoords.Rule = MakePlusRule(restOfCoords, comma, expression);

            unaryOperation.Rule = unaryOperator + term + ReduceHere();
            unaryOperator.Rule = ToTerm("-") | "!";

            binaryOperation.Rule = expression + binaryOperator + expression;
            binaryOperator.Rule = ToTerm("+") | "-" | "*" | "/" | @"\" | "%" | "^" | "<<" | ">>" | "<" | ">" | "<=" | ">=" | "==" | "!=" | "&" | "|" | "#";

            indexer.Rule = term + "[" + indices + "]";
            indices.Rule = MakePlusRule(indices, comma, expression);

            calculation.Rule = expression;

            // Configure operators

            RegisterOperators(50, "&", "|", "#", "<<", ">>");
            RegisterOperators(30, "^");
            RegisterOperators(20, "/", @"\", "%");
            RegisterOperators(15, "*");
            RegisterOperators(10, "+", "-");
            RegisterOperators(5, "<", ">", "<=", ">=", "==", "!=");

            // Clean up unnecessary terms

            MarkTransient(parenthesis, unaryOperator, binaryOperator);
            MarkPunctuation(",", ";", "(", ")", ")v", "[", "]", "{", "}", "=");

            this.Root = root;
        }
    }
}
