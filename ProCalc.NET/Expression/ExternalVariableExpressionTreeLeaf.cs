﻿using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class ExternalVariableExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected string extVarName;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(extVarName);
        }

        internal ExternalVariableExpressionTreeLeaf(BinaryReader reader)
            : base(-1, 0)
        {
            extVarName = reader.ReadString();
        }

        public ExternalVariableExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            extVarName = "";
        }

        public override BaseExpressionTreeItem Clone()
        {
            ExternalVariableExpressionTreeLeaf result = new ExternalVariableExpressionTreeLeaf(-1, -1);
            result.ExtVarName = extVarName;

            return result;
        }

        public override void Verify()
        {
            if (!CommonTools.CheckVariableIdentifier(extVarName))
            {
                Invalidate();
                throw new CriticalException("Invalid identifier!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.ExternalVariable;

        public string ExtVarName
        {
            get => extVarName;
            set
            {
                this.extVarName = value;

                Invalidate();
            }
        }
    }
}
