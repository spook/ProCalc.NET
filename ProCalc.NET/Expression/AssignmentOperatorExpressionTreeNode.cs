﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class AssignmentOperatorExpressionTreeNode : BaseExpressionTreeNode
    {
        private class ParamResolver : IdentifierResolver
        {
            private List<String> @params = new List<string>();

            public void AddParam(string paramName)
            {
                @params.Add(paramName);
            }

            public override bool Resolve(IdentifierExpressionTreeLeaf identifier, out BaseExpressionTreeItem result)
            {
                result = null;

                int i = 0;
                while (i < @params.Count && @params[i].ToUpper() != identifier.IdentifierName.ToUpper())
			        i++;

                if (i == @params.Count())
			        return false;

                result = new ParameterExpressionTreeLeaf(identifier.Line, identifier.Column);
                    ((ParameterExpressionTreeLeaf)result).SetParamId(i);

                return true;
            }
        }

        protected BaseExpressionTreeItem left;
        protected BaseExpressionTreeItem right;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            left.Save(writer);
            right.Save(writer);
        }

        internal AssignmentOperatorExpressionTreeNode(BinaryReader reader)
            : base(-1, 0)
        {
            left = BaseExpressionTreeItem.Create(reader);
            right = BaseExpressionTreeItem.Create(reader);
        }

        public AssignmentOperatorExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            left = null;
            right = null;
        }

        public override BaseExpressionTreeItem Clone()
        {
            AssignmentOperatorExpressionTreeNode result = new AssignmentOperatorExpressionTreeNode(-1, -1);
            result.Left = left.Clone();
            result.Right = right.Clone();

            return result;
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            return false;
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            // Po lewej stronie operatora przypisania może się znaleźć tylko identyfikator lub funkcja użytkownika z podwieszonymi identyfikatorami
            if (left.ExpressionTreeNodeType == ExpressionTreeNodeType.UserFunction)
            {
                // W tym wypadku modyfikujemy stos resolverów o ParamResolver
                UserFunctionExpressionTreeNode fnNode = (UserFunctionExpressionTreeNode)left;

                ParamResolver resolver = null;

                resolver = new ParamResolver();

                // Węzeł jest zweryfikowany, czyli podwieszona funkcja ma prawidłową postać (w szczególności, ma przynajmniej jeden parametr).

                for (int i = 0, i_max = fnNode.ParamCount; i < i_max; i++)
                {
                    BaseExpressionTreeItem param = fnNode.GetParam(i);

                    // Węzeł jest zweryfikowany, czyli pod funkcję podwieszone są tylko identyfikatory.

                    resolver.AddParam(((IdentifierExpressionTreeLeaf)param).IdentifierName);
                }

                resolvers.Add(resolver);

                ResolveChild(ref right, resolvers);
            }
            else
            {
                ResolveChild(ref right, resolvers);
            }
        }

        public override void Verify()
        {
            if (left != null)
                left.Verify();
            else
            {
                Invalidate();
                throw new CriticalException("Left operand of assignment operator is not set!");
            }

            if (left.ExpressionTreeNodeType == ExpressionTreeNodeType.UserFunction)
            {
                // Sprawdzamy, czy konstruowana funkcja ma podwieszone tylko identyfikatory
                UserFunctionExpressionTreeNode fnNode = (UserFunctionExpressionTreeNode)left;

                if (fnNode.ParamCount == 0)
                    throw new SemanticException(Errors.PROCALC_ERROR_SEMANTIC_ASSIGNED_FUNCTION_HAS_NO_PARAMS, line, column);

                for (int i = 0, i_max = fnNode.ParamCount; i < i_max; i++)
                    if (fnNode.GetParam(i).ExpressionTreeNodeType != ExpressionTreeNodeType.Identifier)
                        throw new SemanticException(Errors.PROCALC_ERROR_SEMANTIC_INVALID_ASSIGNMENT_OPERATION, line, column);
            }
            else if (left.ExpressionTreeNodeType != ExpressionTreeNodeType.Identifier)
                throw new SemanticException(Errors.PROCALC_ERROR_SEMANTIC_INVALID_ASSIGNMENT_OPERATION, line, column);

            if (right != null)
                right.Verify();
            else
            {
                Invalidate();
                throw new CriticalException("Right operand of assignment operator is not set!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.AssignmentOperator;
        public BaseExpressionTreeItem Left { get => left; set => InternalSetChild(ref this.left, value); }

        public BaseExpressionTreeItem Right { get => right; set => InternalSetChild(ref this.right, value); }
    }
}
