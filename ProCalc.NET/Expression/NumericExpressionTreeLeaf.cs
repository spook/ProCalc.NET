﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class NumericExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected BaseNumeric numeric;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            numeric.Save(writer);
        }

        internal NumericExpressionTreeLeaf(BinaryReader reader)
            : base(-1, -1)
        {
            numeric = BaseNumeric.Create(reader);
        }

        public NumericExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            numeric = null;
        }

        public override BaseExpressionTreeItem Clone()
        {
            NumericExpressionTreeLeaf result = new NumericExpressionTreeLeaf(-1, -1);
            if (numeric != null)
                result.Numeric = numeric.Clone();

            return result;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.Numeric;

        public override void Verify()
        {
            if (numeric == null)
            {
                Invalidate();
                throw new CriticalException("No numeric element is attached to NumericExpressionTreeLeaf!");
            }

            verified = true;
        }

        public BaseNumeric Numeric
        {
            get => numeric;
            set
            {
                if (this.numeric == value)
                    return;

                this.numeric = value;

                Invalidate();
            }
        }
    }
}
