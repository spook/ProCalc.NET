﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class UnaryOperatorExpressionTreeNode : BaseExpressionTreeNode
    {
        protected UnaryOperatorMethod opMethod;
        protected string opString;
        protected BaseExpressionTreeItem operand;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(opString);
            operand.Save(writer);
        }

        internal UnaryOperatorExpressionTreeNode(BinaryReader reader)
            : base(-1, 0)
        {
            opString = reader.ReadString();
            opMethod = Arithmetics.GetUnaryOperatorMethod(opString);
            operand = BaseExpressionTreeItem.Create(reader);
        }

        public UnaryOperatorExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            operand = null;
            opMethod = null;
            opString = "";
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            ResolveChild(ref operand, resolvers);
        }

        public UnaryOperatorMethod GetOpMethod()
        {
            return opMethod;
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            if (operand != null && opMethod != null)
            {
                BaseExpressionTreeItem optimizeResult = null;
                if (operand.Optimize(ref optimizeResult))
                    InternalSetChild(ref operand, optimizeResult);

                // Optymalizacja jest możliwa tylko wówczas, gdy operandem jest element liczbowy
                if (operand.ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric)
                {
                    BaseNumeric tmpNumeric = null;
                    NumericExpressionTreeLeaf tmpNumericLeaf = null;

                    tmpNumeric = opMethod(((NumericExpressionTreeLeaf)operand).Numeric);

                    tmpNumericLeaf = new NumericExpressionTreeLeaf(-1, -1);
                    tmpNumericLeaf.                    Numeric = tmpNumeric;
                    tmpNumeric = null;

                    result = tmpNumericLeaf;
                    return true;
                }
			else 
				return false;
            }
            else
                return false;
        }

        public override void Verify()
        {
            if (operand != null)
                operand.Verify();
            else
            {
                Invalidate();
                throw new CriticalException("No operand supplied for unary operator!");
            }

            if (opMethod == null || opString == "")
            {
                Invalidate();
                throw new CriticalException("Unary operator method is not set!");
            }

            verified = true;
        }

        public override BaseExpressionTreeItem Clone()
        {
            UnaryOperatorExpressionTreeNode result = new UnaryOperatorExpressionTreeNode(-1, -1);
            result.Operand = operand.Clone();
            result.OpString = opString;

            return result;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.UnaryOperator;

        public string OpString
        {
            get => opString;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = "";
                    opMethod = null;
                }
                else
                {
                    var method = Arithmetics.GetUnaryOperatorMethod(value);

                    if (method == null)
                        throw new CriticalException("Invalid unary operator!");

                    this.opString = value;
                    this.opMethod = method;
                }

                Invalidate();
            }
        }

        public BaseExpressionTreeItem Operand { get => operand; set => InternalSetChild(ref operand, value); }
    }
}
