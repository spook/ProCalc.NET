﻿using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class IdentifierExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected string idName;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(idName);
        }

        internal IdentifierExpressionTreeLeaf(BinaryReader reader)
            : base(-1, 0)
        {
            idName = reader.ReadString();
        }

        public IdentifierExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            idName = "";
        }

        public override BaseExpressionTreeItem Clone()
        {
            IdentifierExpressionTreeLeaf result = new IdentifierExpressionTreeLeaf(-1, -1);
            result.IdentifierName = idName;

            return result;
        }

        public BaseExpressionTreeItem ResolveIdentifier(List<IdentifierResolver> resolvers)
        {
            bool resolved = false;
            BaseExpressionTreeItem result = null;

            for (int i = resolvers.Count() - 1; i >= 0 && !resolved; i--)
            {
                resolved = resolvers[i].Resolve(this, out result);

                if (!resolved && result != null)
                    throw new CriticalException("One of the resolvers doesn't work properly!");
            }

            if (!resolved)
                throw new CriticalException("Failed to resolve identifier!");

            return result;
        }

        public override void Verify()
        {
            if (!CommonTools.CheckVariableIdentifier(idName))
            {
                Invalidate();
                throw new CriticalException("Invalid identifier!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.Identifier;

        public string IdentifierName
        {
            get => idName;
            set
            {
                this.idName = value;

                Invalidate();
            }
        }
    }
}
