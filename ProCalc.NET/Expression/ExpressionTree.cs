﻿using Irony.Parsing;
using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Grammar;
using ProCalc.NET.Math;
using ProCalc.NET.Numerics;
using ProCalc.NET.Resolvers;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class ExpressionTree
    {
        // Podstawowy resolver, rozwiązujący każdy identyfikator jako zmienną.
        private class GenericIdentifierResolver : IdentifierResolver
        {
            public override bool Resolve(IdentifierExpressionTreeLeaf identifier, out BaseExpressionTreeItem result)
            {
                result = new VariableExpressionTreeLeaf(identifier.Line, identifier.Column);
                ((VariableExpressionTreeLeaf)result).                VariableName = identifier.IdentifierName;

                return true;
            }
        }

        // Resolver korzystający z mechanizmu rozpoznawania dostarczonego przez użytkownika
        private class UserIdentifierResolver : IdentifierResolver
		{
		    private BaseExternalIdentifierResolver externalIdentifierResolver;

            public UserIdentifierResolver(BaseExternalIdentifierResolver externalIdentifierResolver)
            {
                this.externalIdentifierResolver = externalIdentifierResolver;
            }

            public override bool Resolve(IdentifierExpressionTreeLeaf identifier, out BaseExpressionTreeItem result)
            {
                result = null;

                if (externalIdentifierResolver == null)
                    return false;

                BaseResolvedIdentifier userResult = externalIdentifierResolver.ResolveIdentifier(identifier.IdentifierName);

                if (userResult == null)
                    return false;

                IdentifierType resolvedIdentifierType = userResult.GetIdentifierType();

                if (resolvedIdentifierType == IdentifierType.itVariable)
                {
                    if (userResult is ResolvedVariable resolvedVariable)
                    {
                        result = new VariableExpressionTreeLeaf(identifier.Line, identifier.Column);
                        ((VariableExpressionTreeLeaf)result).VariableName = resolvedVariable.GetVariableName();

                        return true;
                    }
                    else
                        throw new CriticalException("Invalid IdentifierType returned by BaseResolvedIdentifier descendant!");
                }
                else if (resolvedIdentifierType == IdentifierType.itExternalVariable)
                {
                    if (userResult is ResolvedExternalVariable resolvedExternalVariable)
                    {
                        result = new ExternalVariableExpressionTreeLeaf(identifier.Line, identifier.Column);
                        ((ExternalVariableExpressionTreeLeaf)result).ExtVarName = resolvedExternalVariable.GetExternalVariableName();

                        return true;
                    }
                    else
                        throw new CriticalException("Invalid IdentifierType returned by BaseResolvedIdentifier descendant!");
                }
                else if (resolvedIdentifierType == IdentifierType.itCallParam)
                {
                    if (userResult is ResolvedCallParam resolvedCallParam)
                    {
                        result = new CallParamExpressionTreeLeaf(identifier.Line, identifier.Column);
                        ((CallParamExpressionTreeLeaf)result).ParamId = resolvedCallParam.GetCallParamIndex();

                        return true;
                    }
                    else
                        throw new CriticalException("Invalid IdentifierType returned by BaseResolvedIdentifier descendant!");
                }
                else
                    throw new CriticalException("Invalid result returned by external identifier resolver!");
            }
		};

        private BaseExpressionTreeItem root;

        private BaseExpressionTreeItem InternalBuildInteger(ParseTreeNode expressionItem)
        {
            if (!long.TryParse(expressionItem.Token.ValueString, out long resultValue))
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_INTEGER_VALUE, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);

            var numeric = new IntNumeric(resultValue);

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private BaseExpressionTreeItem InternalBuildReal(ParseTreeNode expressionItem)
        {
            if (!double.TryParse(expressionItem.Token.ValueString, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out double value))
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_FLOATING_POINT_VALUE, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);

            var numeric = new FloatNumeric(value);

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private BaseExpressionTreeItem InternalBuildComplex(ParseTreeNode expressionItem)
        {
            string val = expressionItem.Token.ValueString.Substring(0, expressionItem.Token.ValueString.Length - 1);

            if (!double.TryParse(val, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out double numValue))
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_COMPLEX_NUMBER, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);

            var numeric = new ComplexNumeric(0.0, numValue);

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private BaseExpressionTreeItem InternalBuildSysInteger(ParseTreeNode expressionItem)
        {
            string baseChar = expressionItem.Token.ValueString.Substring(1, 1).ToLower();

            int @base;

            if (baseChar == "b")
                @base = 2;
            else if (baseChar == "o")
                @base = 8;
            else if (baseChar == "d")
                @base = 10;
            else if (baseChar == "h")
                @base = 16;
            else
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_INTEGER_BASE, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);

            // Wyciągamy część liczbową
            string strVal = expressionItem.Token.ValueString.Substring(2, expressionItem.Token.ValueString.Length - 2);

            // Konwertujemy na integer
            long value;
            try
            {
                value = Convert.ToInt64(strVal, @base);
            }
            catch
            {
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_INTEGER_VALUE, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
            }

            var numeric = new IntNumeric(value);

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private BaseExpressionTreeItem InternalBuildDms(ParseTreeNode expressionItem)
        {
            BaseNumeric numeric = null;

            if (!NumericsTools.StrToDms(expressionItem.Token.ValueString, ref numeric))
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_DMS_VALUE, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private BaseExpressionTreeItem InternalBuildBigNum(ParseTreeNode expressionItem)
        {
            BigNum value;

            try
            {
                value = new BigNum(expressionItem.Token.ValueString.Substring(0, expressionItem.Token.ValueString.Length - 1));
            }
            catch             
            {
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_BIGNUM_VALUE, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
            }

            var numeric = new LargeNumberNumeric(value);

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private BaseExpressionTreeItem InternalBuildString(ParseTreeNode expressionItem)
        {
            string val;
            if (expressionItem.Token.ValueString.Length >= 3)
                val = expressionItem.Token.ValueString.Substring(1, expressionItem.Token.ValueString.Length - 2);
            else
                val = "";

            StringBuilder parsedVal = new StringBuilder();

            int i = 0;
            while (i < val.Length)
            {
                parsedVal.Append(val[i]);

                i++;
                if (i < val.Length && val[i] == '\"' && val[i - 1] == '\"')
                    i++;
            }

            var numeric = new StringNumeric(parsedVal.ToString());

            return new NumericExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Numeric = numeric
            };
        }

        private List<BaseExpressionTreeItem> InternalBuildMatrixRow(ParseTreeNode parseTreeNode)
        {
            var row = new List<BaseExpressionTreeItem>();

            for (int i = 0; i < parseTreeNode.ChildNodes.Count; i++)
                row.Add(InternalBuildExpressionTree(parseTreeNode.ChildNodes[i]));

            return row;
        }

        private BaseExpressionTreeItem InternalBuildMatrix(ParseTreeNode expressionItem)
        {
            var matrixRows = new List<List<BaseExpressionTreeItem>>();

            for (int i = 0; i < expressionItem.ChildNodes[0].ChildNodes.Count; i++)
                matrixRows.Add(InternalBuildMatrixRow(expressionItem.ChildNodes[0].ChildNodes[i]));

            if (matrixRows.Skip(1).Any(r => r.Count != matrixRows[0].Count))
                throw new SyntaxException(Errors.PROCALC_ERROR_SEMANTIC_INVALID_MATRIX_ROW_COUNT_DOES_NOT_MATCH, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);

            var matrixTreeItem = new MatrixExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Width = matrixRows[0].Count,
                Height = matrixRows.Count
            };

            foreach (var row in matrixRows)
                foreach (var item in row)
                    matrixTreeItem.AddParam(item);

            return matrixTreeItem;
        }

        private BaseExpressionTreeItem InternalBuildList(ParseTreeNode expressionItem)
        {
            var listItems = new List<BaseExpressionTreeItem>();

            for (int i = 0; i < expressionItem.ChildNodes[0].ChildNodes.Count; i++)
                listItems.Add(InternalBuildExpressionTree(expressionItem.ChildNodes[0].ChildNodes[i]));

            var listTreeItem = new ListExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Count = listItems.Count
            };

            foreach (var item in listItems)
                listTreeItem.AddParam(item);

            return listTreeItem;
        }

        private BaseExpressionTreeItem InternalBuildFunctionCall(ParseTreeNode expressionItem)
        {
            string fnName = expressionItem.ChildNodes[0].Token.ValueString;

            List<BaseExpressionTreeItem> args = new List<BaseExpressionTreeItem>();

            for (int i = 0; i < expressionItem.ChildNodes[1].ChildNodes.Count; i++)
            {
                args.Add(InternalBuildExpressionTree(expressionItem.ChildNodes[1].ChildNodes[i]));
            }

            BaseParamExpressionTreeNode result;

            if (Arithmetics.FunctionIsBuiltin(fnName))
            {
                result = new BuiltinFunctionExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
                ((BuiltinFunctionExpressionTreeNode)result).FunctionName = fnName;
            }
            else
            {
                result = new UserFunctionExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
                ((UserFunctionExpressionTreeNode)result).FunctionName = fnName;
            }

            foreach (var item in args)
                result.AddParam(item);

            return result;
        }

        private BaseExpressionTreeItem InternalBuildVariable(ParseTreeNode expressionItem)
        {
            var name = expressionItem.ChildNodes[0].Token.ValueString;

            if (Arithmetics.IdentifierIsConstant(name))
            {
                return new ConstantExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
                {
                    ConstName = name
                };
            }
            else
            {
                return new IdentifierExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
                {
                    IdentifierName = expressionItem.ChildNodes[0].Token.ValueString
                };
            }
        }

        private BaseExpressionTreeItem InternalBuildCallParam(ParseTreeNode expressionItem)
        {
            int index = int.Parse(expressionItem.Token.ValueString.Substring(1));

            var result = new CallParamExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                ParamId = index
            };

            return result;
        }

        private BaseExpressionTreeItem InternalBuildExternalVariable(ParseTreeNode expressionItem)
        {
            var result = new ExternalVariableExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                ExtVarName = expressionItem.ChildNodes[1].Token.ValueString
            };

            return result;
        }

        private BaseExpressionTreeItem InternalBuildScalar(ParseTreeNode expressionItem)
        {
            var term = expressionItem.ChildNodes[0];

            if (term.Term.Name == ExpressionGrammar.INTEGER) // Collapsed scalar
                return InternalBuildInteger(term);
            else if (term.Term.Name == ExpressionGrammar.REAL) // Collapsed scalar
                return InternalBuildReal(term);
            else if (term.Term.Name == ExpressionGrammar.COMPLEX) // Collapsed scalar
                return InternalBuildComplex(term);
            else if (term.Term.Name == ExpressionGrammar.SYS_INTEGER) // Collapsed scalar
                return InternalBuildSysInteger(term);
            else if (term.Term.Name == ExpressionGrammar.DMS) // Collapsed scalar
                return InternalBuildDms(term);
            else if (term.Term.Name == ExpressionGrammar.BIG_NUM) // Collapsed scalar
                return InternalBuildBigNum(term);
            else
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
        }

        private BaseExpressionTreeItem InternalBuildVector(ParseTreeNode term)
        {
            // Vector has < and > signs kept - they cannot be removed
            // so that < and > operators are not trimmed

            var vectorItems = new List<BaseExpressionTreeItem>();

            vectorItems.Add(InternalBuildExpressionTree(term.ChildNodes[0]));

            for (int i = 0; i < term.ChildNodes[1].ChildNodes.Count; i++)
                vectorItems.Add(InternalBuildExpressionTree(term.ChildNodes[1].ChildNodes[i]));

            var vectorTreeItem = new VectorExpressionTreeNode(term.Span.Location.Line, term.Span.Location.Column)
            {
                Count = vectorItems.Count
            };

            foreach (var item in vectorItems)
                vectorTreeItem.AddParam(item);

            return vectorTreeItem;
        }

        private BaseExpressionTreeItem InternalBuildTerm(ParseTreeNode expressionItem)
        {
            var term = expressionItem.ChildNodes[0];

            if (term.Term.Name == ExpressionGrammar.SCALAR) // Collapsed scalar
                return InternalBuildScalar(term);
            else if (term.Term.Name == ExpressionGrammar.STRING)
                return InternalBuildString(term);
            else if (term.Term.Name == ExpressionGrammar.MATRIX)
                return InternalBuildMatrix(term);
            else if (term.Term.Name == ExpressionGrammar.LIST)
                return InternalBuildList(term);
            else if (term.Term.Name == ExpressionGrammar.FUNCTION_CALL)
                return InternalBuildFunctionCall(term);
            else if (term.Term.Name == ExpressionGrammar.VARIABLE)
                return InternalBuildVariable(term);
            else if (term.Term.Name == ExpressionGrammar.CALL_PARAM)
                return InternalBuildCallParam(term);
            else if (term.Term.Name == ExpressionGrammar.EXTERNAL_VARIABLE)
                return InternalBuildExternalVariable(term);
            else if (term.Term.Name == ExpressionGrammar.VECTOR)
                return InternalBuildVector(term);
            else if (term.Term.Name == ExpressionGrammar.EXPRESSION) // Collapsed parenthesis
                return InternalBuildExpressionTree(term);
            else
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
        }

        private BaseExpressionTreeItem InternalBuildBinaryOperation(ParseTreeNode expressionItem)
        {
            var left = InternalBuildExpressionTree(expressionItem.ChildNodes[0]);
            var opString = expressionItem.ChildNodes[1].Token.ValueString;
            var right = InternalBuildExpressionTree(expressionItem.ChildNodes[2]);

            return new BinaryOperatorExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Left = left,
                Right = right,
                OpString = opString
            };
        }

        private BaseExpressionTreeItem InternalBuildUnaryOperation(ParseTreeNode expressionItem)
        {
            var opString = expressionItem.ChildNodes[0].Token.ValueString;
            var expression = InternalBuildTerm(expressionItem.ChildNodes[1]);

            return new UnaryOperatorExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                OpString = opString,
                Operand = expression
            };
        }

        private BaseExpressionTreeItem InternalBuildIndexer(ParseTreeNode expressionItem)
        {
            var operand = InternalBuildTerm(expressionItem.ChildNodes[0]);

            var args = new List<BaseExpressionTreeItem>();
            for (int i = 0; i < expressionItem.ChildNodes[1].ChildNodes.Count; i++)
                args.Add(InternalBuildExpressionTree(expressionItem.ChildNodes[i].ChildNodes[i]));

            var result = new IndexerOperatorExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Operand = operand
            };

            foreach (var arg in args)
                result.AddParam(arg);

            return result;
        }

        private BaseExpressionTreeItem InternalBuildExpressionRecursive(ParseTreeNode expressionItem)
        {
            if (expressionItem.Term.Name == ExpressionGrammar.TERM)
                return InternalBuildTerm(expressionItem);
            else if (expressionItem.Term.Name == ExpressionGrammar.BINARY_OPERATION)
                return InternalBuildBinaryOperation(expressionItem);
            else if (expressionItem.Term.Name == ExpressionGrammar.UNARY_OPERATION)
                return InternalBuildUnaryOperation(expressionItem);
            else if (expressionItem.Term.Name == ExpressionGrammar.INDEXER)
                return InternalBuildIndexer(expressionItem);
            else
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, expressionItem.Span.Location.Line, expressionItem.Span.Location.Column);
        }

        private BaseExpressionTreeItem InternalBuildExpressionTree(ParseTreeNode expression)
        {
            return InternalBuildExpressionRecursive(expression.ChildNodes[0]);
        }

        private BaseExpressionTreeItem InternalBuildFunctionAssignmentTree(ParseTreeNode expressionItem)
        {
            string fnName = expressionItem.ChildNodes[0].Token.ValueString;

            List<BaseExpressionTreeItem> args = new List<BaseExpressionTreeItem>();

            for (int i = 0; i < expressionItem.ChildNodes[1].ChildNodes.Count; i++)
            {
                args.Add(InternalBuildExpressionTree(expressionItem.ChildNodes[1].ChildNodes[i]));
            }

            var invalidArg = args.FirstOrDefault(a => !(a is IdentifierExpressionTreeLeaf));
            if (invalidArg != null)
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, invalidArg.Line, invalidArg.Column);

            var userFunction = new UserFunctionExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                FunctionName = fnName
            };
            foreach (var arg in args)
                userFunction.AddParam(arg);

            var expression = InternalBuildExpressionTree(expressionItem.ChildNodes[2]);

            var result = new AssignmentOperatorExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Left = userFunction,
                Right = expression
            };

            return result;
        }

        private BaseExpressionTreeItem InternalBuildVariableAssignmentTree(ParseTreeNode expressionItem)
        {
            var varName = expressionItem.ChildNodes[0].Token.ValueString;

            var expression = InternalBuildExpressionTree(expressionItem.ChildNodes[1]);

            var identifier = new IdentifierExpressionTreeLeaf(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                IdentifierName = varName
            };

            var result = new AssignmentOperatorExpressionTreeNode(expressionItem.Span.Location.Line, expressionItem.Span.Location.Column)
            {
                Left = identifier,
                Right = expression
            };

            return result;
        }

        private BaseExpressionTreeItem InternalBuildCalculation(ParseTreeNode root)
        {
            return InternalBuildExpressionTree(root.ChildNodes[0]);
        }

        private BaseExpressionTreeItem InternalBuildExpressionTree(ParseTree parseTree)
        {
            var root = parseTree.Root.ChildNodes[0];

            if (root.Term.Name == ExpressionGrammar.VARIABLE_ASSIGNMENT)
            {
                return InternalBuildVariableAssignmentTree(root);
            }
            else if (root.Term.Name == ExpressionGrammar.FUNCTION_ASSIGNMENT)
            {
                return InternalBuildFunctionAssignmentTree(root);
            }
            else if (root.Term.Name == ExpressionGrammar.CALCULATION)
            {
                return InternalBuildCalculation(root);
            }
            else
                throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, root.Span.Location.Line, root.Span.Location.Column);
        }

        private void ResolveIdentifiers(ref BaseExpressionTreeItem treeRoot, BaseExternalIdentifierResolver externalIdentifierResolver)
        {
            if (treeRoot == null)
                throw new CriticalException("Empty expression tree root!");
            if (!treeRoot.GetVerified())
                throw new CriticalException("Expression tree is not verified!");

            List<IdentifierResolver> resolvers = new List<IdentifierResolver>
            {
                // Wrzucamy resolver generyczny, zamieniający wszystkie identyfikatory na zmienne
                new GenericIdentifierResolver(),

                // Wrzucamy resolver użytkownika (o ile taki istnieje)
                new UserIdentifierResolver(externalIdentifierResolver)
            };

            if (treeRoot.NodeKind == TreeNodeKind.tnkNode)
            {
                // Jeśli jest to węzeł, rozwiązujemy identyfikatory rekurencyjnie
                ((BaseExpressionTreeNode)treeRoot).ResolveIdentifiers(resolvers);
            }
            else
            {
                // Jeśli jest to liść, sprawdzamy, czy jest identyfikatorem
                if (treeRoot.ExpressionTreeNodeType == ExpressionTreeNodeType.Identifier)
                {
                    // Jeśli jest to identyfikator, rozwiązujemy go

                    BaseExpressionTreeItem result = ((IdentifierExpressionTreeLeaf)treeRoot).ResolveIdentifier(resolvers);

                    if (treeRoot != result)
                        treeRoot = result;
                }
            }

            if (treeRoot == null)
                throw new CriticalException("Identifier resolving process damaged tree root!");

            treeRoot.Verify();
        }

        private void Optimize(ref BaseExpressionTreeItem treeRoot)
        {
            if (treeRoot == null)
                throw new CriticalException("Root of expression tree is empty!");
            if (!treeRoot.GetVerified())
                throw new CriticalException("Expression tree is not verified!");

            BaseExpressionTreeItem optimizeResult = null;

            if (treeRoot.Optimize(ref optimizeResult))
                treeRoot = optimizeResult;

            if (treeRoot == null)
                throw new CriticalException("Optimization process damaged tree root!");

            treeRoot.Verify();
        }

        // TODO: Convert to static method
        public ExpressionTree(ParseTree parseTree, BaseExternalIdentifierResolver externalIdentifierResolver)
        {
            if (parseTree == null) 
                throw new ArgumentNullException(nameof(parseTree));

            BaseExpressionTreeItem newRoot = null;

            newRoot = InternalBuildExpressionTree(parseTree);
            newRoot.Verify();

            ResolveIdentifiers(ref newRoot, externalIdentifierResolver);

            Optimize(ref newRoot);

            root = newRoot;
        }

        // TODO: Convert to static method
        /*
        public ExpressionTree(SyntaxTree syntaxTree, BaseExternalIdentifierResolver externalIdentifierResolver)
        {
            if (syntaxTree == null)
                throw new CriticalException("Invalid syntax tree!");
            if (!syntaxTree.GetVerified())
                throw new CriticalException("Not verified syntax tree!");

            BaseExpressionTreeItem newRoot = null;

            // Budowanie drzewa
            newRoot = InternalBuildExpressionTree(syntaxTree.GetRoot());
            newRoot.Verify();

            // Rozwiązywanie identyfikatorów
            ResolveIdentifiers(ref newRoot, externalIdentifierResolver);

            // Optymalizacja
            Optimize(ref newRoot);

            root = newRoot;
        }
        */

        public ExpressionTree(BaseExpressionTreeItem treeRoot)
        {
            root = treeRoot ?? throw new CriticalException("Invalid expression tree root!");
        }

        public ExpressionTree(BinaryReader reader)
        {
            root = null;

            BaseExpressionTreeItem newRoot = null;
            newRoot = BaseExpressionTreeItem.Create(reader);
            newRoot.Verify();

            root = newRoot;
        }

        public BaseExpressionTreeItem GetRoot()
        {
            return root;
        }

        public bool GetVerified()
        {
            if (root != null)
                return root.GetVerified();
            else
                throw new CriticalException("The root of expression tree is null!");
        }

        public void Save(BinaryWriter writer)
        {
            if (root == null || !root.GetVerified())
                throw new CriticalException("Save can be called only on verified expression tree!");

            root.Save(writer);
        }
    }
}
