﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class BuiltinFunctionExpressionTreeNode : BaseParamExpressionTreeNode
    {
        protected FunctionMethod fnMethod;
        protected string fnString;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(fnString);
            SaveParams(writer);
        }

        internal BuiltinFunctionExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {
            fnString = reader.ReadString();
            fnMethod = Arithmetics.GetFunctionMethod(fnString);
        }

        public BuiltinFunctionExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            fnMethod = null;
            fnString = "";
        }

        public override BaseExpressionTreeItem Clone()
        {
            BuiltinFunctionExpressionTreeNode result = new BuiltinFunctionExpressionTreeNode(-1, -1);
            result.FunctionName = fnString;

            if (@params.Count > 0)
                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    result.AddParam(@params[i].Clone());

            return result;
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            bool optimizable = true;

            optimizable &= fnMethod != null;
            optimizable &= (@params.Count > 0);

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                optimizable &= (@params[i] != null);

            if (optimizable)
            {
                BaseExpressionTreeItem optimizeResult = null;

                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    if (@params[i].Optimize(ref optimizeResult))
                    {
                        @params[i] = optimizeResult;
                        if (@params[i] != null)
                            @params[i].Parent = this;

                        Invalidate();
                    }

                bool canTryOptimize = true;

                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    canTryOptimize &= (@params[i].ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric);

                if (canTryOptimize)
                {
                    List<BaseNumeric> functionParams = new List<BaseNumeric>();

                    for (int i = 0, i_max = @params.Count; i < i_max; i++)
                        functionParams.Add(((NumericExpressionTreeLeaf)@params[i]).Numeric);

                    BaseNumeric tmpNumeric = null;
                    NumericExpressionTreeLeaf tmpNumericLeaf = null;

                    // TODO: Dodać try/catch na operacjach optymalizacji + return false
                    // żeby błędy runtime faktycznie wychodziły w runtime
                    tmpNumeric = fnMethod(functionParams);

                    tmpNumericLeaf = new NumericExpressionTreeLeaf(-1, -1);

                    tmpNumericLeaf.
                    Numeric = tmpNumeric;
                    tmpNumeric = null;

                    result = tmpNumericLeaf;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
            {
                var item = @params[i];
                ResolveChild(ref item, resolvers);
                @params[i] = item;
            }
        }

        public override void Verify()
        {
            if (@params.Count == 0)
            {
                Invalidate();
                throw new CriticalException("Function parameters are missing!");
            }

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                if (@params[i] != null)
                    @params[i].Verify();
                else
                {
                    Invalidate();
                    throw new CriticalException("One of function parameters is null!");
                }

            if (fnMethod == null || fnString == "")
            {
                Invalidate();
                throw new CriticalException("Function method is not set!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.BuiltinFunction;

        public FunctionMethod FnMethod => fnMethod;

        public string FunctionName
        {
            get => fnString;
            set
            {
                if (value == "")
                {
                    fnString = "";
                    fnMethod = null;
                }
                else
                {
                    var method = Arithmetics.GetFunctionMethod(value);

                    if (method == null)
                        throw new CriticalException("Invalid builtin function!");

                    fnMethod = method;
                    fnString = value;
                }

                Invalidate();
            }
        }
    }
}
