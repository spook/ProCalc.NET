﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class ListExpressionTreeNode : BaseSequenceExpressionTreeNode
    {
        internal ListExpressionTreeNode(BinaryReader reader)
            : base(-1, -1, Arithmetics.GetBuildListMethod())
        {
            
        }

        public ListExpressionTreeNode(int line, int column)
            : base(line, column, Arithmetics.GetBuildListMethod())
        {

        }

        public override BaseExpressionTreeItem Clone()
        {
            ListExpressionTreeNode result = new ListExpressionTreeNode(-1, -1);
            result.Count = count;

            if (@params.Count > 0)
                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    result.AddParam(@params[i].Clone());

            return result;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.List;       
    }
}
