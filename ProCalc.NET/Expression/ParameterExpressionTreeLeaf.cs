﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class ParameterExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected int paramId;
        protected bool paramSet;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(paramId);
        }
         
        internal ParameterExpressionTreeLeaf(BinaryReader reader)
            : base(-1, 0)
        {
            paramId = reader.ReadInt32();
            paramSet = true;
        }

        public ParameterExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            paramId = 0;
            paramSet = false;
        }

        public void SetParamId(int paramId)
        {
            this.paramId = paramId;
            paramSet = true;

            Invalidate();
        }

        public int GetParamId()
        {
            return paramId;
        }

        public override void Verify()
        {
            if (!paramSet)
            {
                Invalidate();
                throw new CriticalException("Invalid parameter index!");
            }

            verified = true;
        }

        public override BaseExpressionTreeItem Clone()
        {
            ParameterExpressionTreeLeaf result = new ParameterExpressionTreeLeaf(-1, -1);
            result.SetParamId(paramId);

            return result;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.Parameter;
    }
}
