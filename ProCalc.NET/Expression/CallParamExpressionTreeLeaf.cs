﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class CallParamExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected int paramId;
        protected bool paramSet;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(paramId);            
        }

        internal CallParamExpressionTreeLeaf(BinaryReader reader)
            : base(-1, 0)
        {
            paramId = reader.ReadInt32();
            paramSet = true;
        }

        public CallParamExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            paramId = 0;
            paramSet = false;
        }

        public override BaseExpressionTreeItem Clone()
        {
            CallParamExpressionTreeLeaf result = new CallParamExpressionTreeLeaf(-1, -1);
            result.ParamId = paramId;

            return result;
        }

        public override void Verify()
        {
            if (!paramSet)
            {
                Invalidate();
                throw new CriticalException("Invalid call parameter index!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.CallParam;

        public int ParamId
        {
            get => paramId;
            set
            {
                this.paramId = value;
                paramSet = true;

                Invalidate();
            }
        }
    }
}
