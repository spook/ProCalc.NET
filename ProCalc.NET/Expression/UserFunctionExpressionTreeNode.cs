﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class UserFunctionExpressionTreeNode : BaseParamExpressionTreeNode
    {
        protected string fnName;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(fnName);
            SaveParams(writer);
        }

        internal UserFunctionExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {
            fnName = reader.ReadString();
            LoadParams(reader);
        }

        public UserFunctionExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            fnName = "";
        }

        public override BaseExpressionTreeItem Clone()
        {
            UserFunctionExpressionTreeNode result = new UserFunctionExpressionTreeNode(-1, -1);
            result.FunctionName = fnName;

            if (@params.Count > 0)
                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    result.AddParam(@params[i].Clone());

            return result;
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            return false;
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
            {
                var item = @params[i];
                ResolveChild(ref item, resolvers);
                @params[i] = item;
            }
        }

        public override void Verify()
        {
            if (@params.Count == 0)
            {
                Invalidate();
                throw new CriticalException("User function parameters are missing!");
            }

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                if (@params[i] != null)
                    @params[i].Verify();
                else
                {
                    Invalidate();
                    throw new CriticalException("One of user function parameters is null!");
                }

            if (Arithmetics.FunctionIsBuiltin(fnName))
            {
                Invalidate();
                throw new CriticalException("Invalid function identifier!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.UserFunction;
        public string FunctionName { get => fnName; set => this.fnName = value; }
    }
}
