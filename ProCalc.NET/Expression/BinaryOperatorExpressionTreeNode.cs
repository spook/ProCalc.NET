﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class BinaryOperatorExpressionTreeNode : BaseExpressionTreeNode
    {
        protected BaseExpressionTreeItem left;
        protected BinaryOperatorMethod opMethod;
        protected string opString;
        protected BaseExpressionTreeItem right;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(opString);

            left.Save(writer);
            right.Save(writer);
        }

        internal BinaryOperatorExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {
            opString = reader.ReadString();
            opMethod = Arithmetics.GetBinaryOperatorMethod(opString);

            left = BaseExpressionTreeItem.Create(reader);
            right = BaseExpressionTreeItem.Create(reader);
        }

        public BinaryOperatorExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            left = null;
            right = null;
            opString = "";
            opMethod = null;
        }

        public override BaseExpressionTreeItem Clone()
        {
            BinaryOperatorExpressionTreeNode result = new BinaryOperatorExpressionTreeNode(-1, -1);
            result.Left = left.Clone();
            result.Right = right.Clone();
            result.OpString = opString;

            return result;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.BinaryOperator;

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            if (left != null && right != null && opMethod != null)
            {
                BaseExpressionTreeItem optimizeResult = null;

                if (left.Optimize(ref optimizeResult))
                    InternalSetChild(ref left, optimizeResult);

                if (right.Optimize(ref optimizeResult))
                    InternalSetChild(ref right, optimizeResult);

                // Optymalizacja jest możliwa tylko wówczas, gdy operandami są elementy liczbowe
                if (left.ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric && right.ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric)
                {
                    BaseNumeric tmpNumeric = null;
                    NumericExpressionTreeLeaf tmpNumericLeaf = null;

                    tmpNumeric = opMethod(((NumericExpressionTreeLeaf)left).Numeric, ((NumericExpressionTreeLeaf)right).Numeric);

                    tmpNumericLeaf = new NumericExpressionTreeLeaf(-1, -1);
                    tmpNumericLeaf.                    Numeric = tmpNumeric;

                    result = tmpNumericLeaf;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            ResolveChild(ref left, resolvers);
            ResolveChild(ref right, resolvers);
        }

        public override void Verify()
        {
            if (left != null)
                left.Verify();
            else
            {
                Invalidate();
                throw new CriticalException("Left operand for binary operator is not set!");
            }

            if (right != null)
                right.Verify();
            else
            {
                Invalidate();
                throw new CriticalException("Right operand for binary operator is not set!");
            }

            if (opMethod == null || opString == "")
            {
                Invalidate();
                throw new CriticalException("Binary operator method is not set!");
            }

            verified = true;

        }

        public BaseExpressionTreeItem Left { get => left; set => InternalSetChild(ref this.left, value); }

        public BinaryOperatorMethod OpMethod => opMethod;

        public string OpString
        {
            get => opString;
            set
            {
                if (value == "")
                {
                    this.opString = "";
                    this.opMethod = null;
                }
                else
                {
                    var method = Arithmetics.GetBinaryOperatorMethod(value);

                    if (method == null)
                        throw new CriticalException("Invalid binary operator!");

                    this.opMethod = method;
                    this.opString = value;
                }

                Invalidate();
            }
        }
        public BaseExpressionTreeItem Right { get => right; set => InternalSetChild(ref this.right, value); }
    }
}
