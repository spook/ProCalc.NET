﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    abstract class BaseSequenceExpressionTreeNode : BaseParamExpressionTreeNode
    {
        protected int count;
        protected bool countSet;
        protected readonly BuildSequenceMethod buildMethod;

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(count);
            SaveParams(writer);
        }

        protected BaseSequenceExpressionTreeNode(BinaryReader reader, BuildSequenceMethod method) 
            : base(-1, -1)
        {
            if (method == null)
                throw new CriticalException("No build list method in arithmetics!");

            count = reader.ReadInt32();
            countSet = true;

            LoadParams(reader);

            if (count < 1 || @params.Count != count)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            buildMethod = method;
        }

        protected BaseSequenceExpressionTreeNode(int line, int column, BuildSequenceMethod method) 
            : base(line, column)
        {
            if (method == null)
                throw new CriticalException("No build list method in arithmetics!");

            count = 0;
            countSet = false;

            buildMethod = method;
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            bool optimizable = true;

            optimizable &= buildMethod != null;
            optimizable &= (@params.Count > 0);
            optimizable &= (count == @params.Count);
            optimizable &= (count > 0);

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                optimizable &= (@params[i] != null);

            if (optimizable)
            {
                BaseExpressionTreeItem optimizeResult = null;

                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    if (@params[i].Optimize(ref optimizeResult))
                    {
                        @params[i] = optimizeResult;
                        if (@params[i] != null)
                            @params[i].Parent = this;

                        Invalidate();
                    }

                bool canTryOptimize = true;

                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    canTryOptimize &= (@params[i].ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric);

                if (canTryOptimize)
                {
                    BaseNumeric[] listParams = new BaseNumeric[@params.Count];

                    for (int i = 0, i_max = @params.Count; i < i_max; i++)
                        listParams[i] = ((NumericExpressionTreeLeaf)@params[i]).Numeric;

                    BaseNumeric tmpNumeric = null;
                    NumericExpressionTreeLeaf tmpNumericLeaf = null;

                    try
                    {
                        tmpNumeric = buildMethod(count, listParams);

                        tmpNumericLeaf = new NumericExpressionTreeLeaf(-1, -1);

                        tmpNumericLeaf.Numeric = tmpNumeric;
                        tmpNumeric = null;
                    }
                    catch
                    {
                        // Optymalizacja nie powiodła się
                        return false;
                    }

                    result = tmpNumericLeaf;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override void Verify()
        {
            if (@params.Count == 0)
            {
                Invalidate();
                throw new CriticalException("Sequence parameters are missing!");
            }

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                if (@params[i] != null)
                    @params[i].Verify();
                else
                {
                    Invalidate();
                    throw new CriticalException("One of Sequence parameters is null!");
                }

            if (!countSet || @params.Count != count)
            {
                Invalidate();
                throw new CriticalException("Invalid count of Sequence parameters!");
            }

            if (buildMethod == null)
            {
                Invalidate();
                throw new CriticalException("Invalid Sequence construction method!");
            }

            verified = true;
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
            {
                var item = @params[i];
                ResolveChild(ref item, resolvers);
                @params[i] = item;
            }
        }

        public int Count
        {
            get => count;
            set
            {
                this.count = value;
                countSet = true;

                Invalidate();
            }
        }

        public BuildSequenceMethod BuildMethod => buildMethod;
    }
}
