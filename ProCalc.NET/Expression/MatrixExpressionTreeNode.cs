﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class MatrixExpressionTreeNode : BaseParamExpressionTreeNode
    {
        protected int width;
        protected bool widthSet;
        protected int height;
        protected bool heightSet;
        protected BuildMatrixMethod mtMethod;

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(width);
            writer.Write(height);
            SaveParams(writer);
        }

        internal MatrixExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {
            mtMethod = Arithmetics.GetBuildMatrixMethod();

            width = reader.ReadInt32();
            widthSet = true;
            height = reader.ReadInt32();
            heightSet = false;

            LoadParams(reader);

            if (width < 1 || height < 1 || @params.Count != width * height)
			{
                    throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);                
            }
        }

        public MatrixExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            width = 0;
            widthSet = false;
            height = 0;
            heightSet = false;
            mtMethod = Arithmetics.GetBuildMatrixMethod();

            if (mtMethod == null)
                throw new CriticalException("No build matrix method in arithmetics!");
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
            {
                // TODO ResolveParams
                var item = @params[i];
                ResolveChild(ref item, resolvers);
                @params[i] = item;
            }
        }

        public int Width
        {
            get => width;
            set
            {
                this.width = value;
                widthSet = true;
            }
        }

        public int Height
        {
            get => height;
            set
            {
                this.height = value;
                heightSet = true;
            }
        }

        public BuildMatrixMethod MtMethod => mtMethod;

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            bool optimizable = true;

		    optimizable &= mtMethod != null;
		    optimizable &= (@params.Count > 0);
		    optimizable &= (width * height == @params.Count);
		    optimizable &= (width > 0 && height > 0);

		    for (int i = 0, i_max = @params.Count; i < i_max; i++)
			    optimizable &= (@params[i] != null);

		    if (optimizable)
		    {
			    BaseExpressionTreeItem optimizeResult = null;

                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    if (@params[i].Optimize(ref optimizeResult))
                    {
                        @params[i] = optimizeResult;
                        if (@params[i] != null)
                            @params[i].                            Parent = this;

                        Invalidate();                        
                    }

			    bool canTryOptimize = true;

			    for (int i = 0, i_max = @params.Count; i < i_max; i++)
				    canTryOptimize &= (@params[i].ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric);

			    if (canTryOptimize)
			    {
				    BaseNumeric[] matrixParams = new BaseNumeric[@params.Count];

				    for (int i = 0, i_max = @params.Count; i < i_max; i++)
					    matrixParams[i] = ((NumericExpressionTreeLeaf)@params[i]).Numeric;

				    BaseNumeric tmpNumeric = null;
				    NumericExpressionTreeLeaf tmpNumericLeaf = null;

				    try
				    {
					    tmpNumeric = mtMethod(width, height, matrixParams);

					    tmpNumericLeaf = new NumericExpressionTreeLeaf(-1, -1);

					    tmpNumericLeaf.
					    Numeric = tmpNumeric;
					    tmpNumeric = null;
				    }
				    catch
				    {
					    // Optymalizacja nie powiodła się
					    return false;
				    }

				    result = tmpNumericLeaf;
				    return true;
			    }
			    else
				    return false;
		    }
		    else
			    return false;
        }

        public override void Verify()
        {
            if (@params.Count == 0)
		    {
			    Invalidate();
			    throw new CriticalException("Matrix construction parameters are missing!");
		    }

		    for (int i = 0, i_max = @params.Count; i < i_max; i++)
			    if (@params[i] != null)
				    @params[i].Verify();
			    else
			    {
				    Invalidate();
				    throw new CriticalException("One of matrix construction parameters is null!");
			    }
		
		    if (!widthSet || !heightSet || (@params.Count != width * height))
		    {
			    Invalidate();
			    throw new CriticalException("Invalid count of matrix elements!");
		    }

		    if (mtMethod == null)
		    {
			    Invalidate();
			    throw new CriticalException("Invalid matrix construction method!");
		    }
		
		    verified = true;
        }

        public override BaseExpressionTreeItem Clone()
        {
            MatrixExpressionTreeNode result = new MatrixExpressionTreeNode(-1, -1);
            result.            Width = width;
            result.            Height = height;

            if (@params.Count > 0)
			for (int i = 0, i_max = @params.Count; i < i_max; i++)
                result.AddParam(@params[i].Clone());

            return result;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.Matrix;
    }
}
