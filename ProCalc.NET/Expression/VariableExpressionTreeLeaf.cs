﻿using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class VariableExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected string varName;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(varName);
        }

        internal VariableExpressionTreeLeaf(BinaryReader reader)
            : base(-1, 0)
        {
            varName = reader.ReadString();
        }

        public VariableExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            varName = "";
        }

        public override BaseExpressionTreeItem Clone()
        {
            VariableExpressionTreeLeaf result = new VariableExpressionTreeLeaf(-1, -1);
            result.VariableName = varName;

            return result;
        }

        public override void Verify()
        {
            if (!CommonTools.CheckVariableIdentifier(varName))
            {
                Invalidate();
                throw new CriticalException("Invalid identifier!");
            }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.Variable;

        public string VariableName
        {
            get => varName;
            set
            {
                this.varName = value;
                Invalidate();
            }
        }
    }
}
