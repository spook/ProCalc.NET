﻿using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    abstract class BaseExpressionTreeLeaf : BaseExpressionTreeItem
    {
        public BaseExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
    
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            return false;
        }

        public override TreeNodeKind NodeKind => TreeNodeKind.tnkLeaf;
    }
}
