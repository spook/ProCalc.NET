﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    abstract class IdentifierResolver
    {
        // Metoda próbuje rozwiązać identyfikator (zdecydować, czy jest zmienną, czy parametrem).
        // Jeśli operacja ta powiedzie się, poprzez result zostaje zwrócona instancja nowego
        // elementu drzewa, zaś funkcja zwraca true. W przeciwnym wypadku funkcja zwraca false.
        public abstract bool Resolve(IdentifierExpressionTreeLeaf identifier, out BaseExpressionTreeItem result);
    }
}
