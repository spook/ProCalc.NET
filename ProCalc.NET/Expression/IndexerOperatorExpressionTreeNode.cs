﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class IndexerOperatorExpressionTreeNode : BaseParamExpressionTreeNode
    {
        protected IndexerMethod indexerMethod;
        protected BaseExpressionTreeItem operand;
        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            operand.Save(writer);
            SaveParams(writer);
        }

        internal IndexerOperatorExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {
            operand = BaseExpressionTreeItem.Create(reader);
            LoadParams(reader);

            indexerMethod = Arithmetics.GetIndexerMethod();
            if (indexerMethod == null)
                throw new CriticalException("No indexer method in arithmetics");
        }

        public IndexerOperatorExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            operand = null;
            indexerMethod = Arithmetics.GetIndexerMethod();
            if (indexerMethod == null)
                throw new CriticalException("No indexer method in arithmetics");
        }

        public override BaseExpressionTreeItem Clone()
        {
            IndexerOperatorExpressionTreeNode result = new IndexerOperatorExpressionTreeNode(-1, -1);
            result.Operand = operand.Clone();
            if (@params.Count > 0)
                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    result.AddParam(@params[i].Clone());

            return result;
        }

        public IndexerMethod GetIndexerMethod()
        {
            return indexerMethod;
        }

        public override bool Optimize(ref BaseExpressionTreeItem result)
        {
            bool optimizable = true;

            optimizable &= (operand != null);
            optimizable &= indexerMethod != null;
            optimizable &= (@params.Count > 0);

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                optimizable &= (@params[i] != null);

            if (optimizable)
            {
                BaseExpressionTreeItem optimizeResult = null;

                if (operand.Optimize(ref optimizeResult))
                    InternalSetChild(ref operand, optimizeResult);

                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    if (@params[i].Optimize(ref optimizeResult))
                    {

                        @params[i] = optimizeResult;
                        if (optimizeResult != null)
                            optimizeResult.Parent = this;

                        Invalidate();
                    }

                bool canTryOptimize = true;

                canTryOptimize &= (operand.ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric);
                for (int i = 0, i_max = @params.Count; i < i_max; i++)
                    canTryOptimize &= (@params[i].ExpressionTreeNodeType == ExpressionTreeNodeType.Numeric);

                if (canTryOptimize)
                {
                    List<BaseNumeric> indexerParams = new List<BaseNumeric>();

                    for (int i = 0, i_max = @params.Count; i < i_max; i++)
                        indexerParams.Add(((NumericExpressionTreeLeaf)@params[i]).Numeric);

                    BaseNumeric tmpNumeric = null;
                    NumericExpressionTreeLeaf tmpNumericLeaf = null;

                    tmpNumeric = indexerMethod(((NumericExpressionTreeLeaf)operand).Numeric, indexerParams);

                    tmpNumericLeaf = new NumericExpressionTreeLeaf(-1, -1);

                    tmpNumericLeaf.
                    Numeric = tmpNumeric;
                    tmpNumeric = null;

                    result = tmpNumericLeaf;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override void ResolveIdentifiers(List<IdentifierResolver> resolvers)
        {
            if (!verified)
                throw new CriticalException("Node is not verified!");

            ResolveChild(ref operand, resolvers);

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
            {
                var item = @params[i];
                ResolveChild(ref item, resolvers);
                @params[i] = item;
            }
        }
        public override void Verify()
        {
            // Musi być przypisana metoda indeksera
            if (indexerMethod == null)
                throw new CriticalException("Indexer method is not set!");

            if (operand != null)
                operand.Verify();
            else
            {
                Invalidate();
                throw new CriticalException("Indexer operand is not set!");
            }

            // Musi być przynajmniej jeden parametr
            if (@params.Count == 0)
            {
                Invalidate();
                throw new CriticalException("Indexer parameters are missing!");
            }

            for (int i = 0, i_max = @params.Count; i < i_max; i++)
                if (@params[i] != null)
                    @params[i].Verify();
                else
                {
                    Invalidate();
                    throw new CriticalException("One of indexer params is null!");
                }

            verified = true;
        }

        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.IndexerOperator;
        public BaseExpressionTreeItem Operand { get => operand; set => InternalSetChild(ref operand, value); }
    }
}
