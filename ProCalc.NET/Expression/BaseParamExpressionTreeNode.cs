﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    abstract class BaseParamExpressionTreeNode : BaseExpressionTreeNode
    {
        protected List<BaseExpressionTreeItem> @params = new List<BaseExpressionTreeItem>();

        protected void SaveParams(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Cannot save params if expression tree is not verified!");

            int len = @params.Count();

            writer.Write(len);

            for (int i = 0; i < @params.Count; i++)
                @params[i].Save(writer);
        }

        protected void LoadParams(BinaryReader reader)
        {
            int len = reader.ReadInt32();

            if (len < 0)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            @params.Clear();

            for (int i = 0; i < len; i++)
            {
                var item = BaseExpressionTreeItem.Create(reader);
                @params.Add(item);
            }
        }

        protected BaseParamExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {

        }

        public BaseParamExpressionTreeNode(int line, int column)
            : base(line, column)
        {
            
        }

        public void AddParam(BaseExpressionTreeItem param)
        {
            @params.Add(param);

            if (param != null)
                param.                Parent = this;

            Invalidate();
        }

        public void InsertParam(int pos, BaseExpressionTreeItem param)
        {
            if (pos < 0 || pos >= @params.Count)
                throw new CriticalException("Invalid index of inserted parameter!");

            @params.Insert(pos, param);

            if (param != null)
                param.                Parent = this;

            Invalidate();
        }

        public void DeleteParam(int index)
        {
            if (index < 0 || index >= @params.Count)
                throw new CriticalException("Invalid index of removed parameter!");

            @params.RemoveAt(index);

            Invalidate();
        }

        public int ParamCount => @params.Count;

        public BaseExpressionTreeItem GetParam(int index)
        {
            if (index < 0 || index >= @params.Count)
                throw new CriticalException("Invalid index of retrieved parameter!");

            return @params[index];
        }
    }
}
