﻿using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    abstract class BaseExpressionTreeNode : BaseExpressionTreeItem
    {
        protected void InternalSetChild(ref BaseExpressionTreeItem field, BaseExpressionTreeItem value)
        {
            if (field == value)
                return;

            field = value;

            if (value != null)
                value.Parent = this;

            Invalidate();
        }

        protected void ResolveChild(ref BaseExpressionTreeItem field, List<IdentifierResolver> resolvers)
        {
            if (field.NodeKind == TreeNodeKind.tnkNode)
            {
                ((BaseExpressionTreeNode)field).ResolveIdentifiers(resolvers);
            }
            else
            {
                if (field.ExpressionTreeNodeType == ExpressionTreeNodeType.Identifier)
                {
                    BaseExpressionTreeItem resolvedItem = ((IdentifierExpressionTreeLeaf)field).ResolveIdentifier(resolvers);

                    InternalSetChild(ref field, resolvedItem);
                }
            }
        }

        public BaseExpressionTreeNode(int line, int column)
            : base(line, column)
        {

        }

        public BaseExpressionTreeNode(BinaryReader reader)
            : base(-1, -1)
        {
   
        }

        public abstract void ResolveIdentifiers(List<IdentifierResolver> resolvers);

        public override TreeNodeKind NodeKind => TreeNodeKind.tnkNode;
    }
}
