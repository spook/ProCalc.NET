﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    abstract class BaseExpressionTreeItem
    {
        protected int column;
        protected int line;
        protected BaseExpressionTreeItem parent;
        protected bool verified;

        protected abstract void InternalSave(BinaryWriter writer);

        protected void Invalidate()
        {
            verified = false;
            if (parent != null)
                parent.Invalidate();
        }
        public BaseExpressionTreeItem(int line, int column)
        {
            this.line = line;
            this.column = column;
            parent = null;
            verified = false;
        }

        public static BaseExpressionTreeItem Create(BinaryReader reader)
        {
            ExpressionTreeNodeType nodeType;

            nodeType = (ExpressionTreeNodeType)reader.ReadInt32();

            BaseExpressionTreeItem result = null;

            switch (nodeType)
            {
                case ExpressionTreeNodeType.Numeric:
                    {
                        result = new NumericExpressionTreeLeaf(reader);
                        break;
                    }

                case ExpressionTreeNodeType.Identifier:
                    {
                        result = new IdentifierExpressionTreeLeaf(reader);
                        break;
                    }

                case ExpressionTreeNodeType.Parameter:
                    {
                        result = new ParameterExpressionTreeLeaf(reader);
                        break;
                    }

                case ExpressionTreeNodeType.CallParam:
                    {
                        result = new CallParamExpressionTreeLeaf(reader);
                        break;
                    }

                case ExpressionTreeNodeType.Variable:
                    {
                        result = new VariableExpressionTreeLeaf(reader);
                        break;
                    }

                case ExpressionTreeNodeType.ExternalVariable:
                    {
                        result = new ExternalVariableExpressionTreeLeaf(reader);
                        break;
                    }

                case ExpressionTreeNodeType.UnaryOperator:
                    {
                        result = new UnaryOperatorExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.BinaryOperator:
                    {
                        result = new BinaryOperatorExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.AssignmentOperator:
                    {
                        result = new AssignmentOperatorExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.IndexerOperator:
                    {
                        result = new IndexerOperatorExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.BuiltinFunction:
                    {
                        result = new BuiltinFunctionExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.UserFunction:
                    {
                        result = new UserFunctionExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.Matrix:
                    {
                        result = new MatrixExpressionTreeNode(reader);
                        break;
                    }

                case ExpressionTreeNodeType.List:
                    {
                        result = new ListExpressionTreeNode(reader);
                        break;
                    }
                case ExpressionTreeNodeType.Constant:
                    {
                        result = new ConstantExpressionTreeLeaf(reader);
                        break;
                    }
                default:
                    throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);
            }

            result.Verify();

            return result;
        }

        public abstract BaseExpressionTreeItem Clone();

        public bool GetVerified()
        {
            return verified;
        }

        public abstract bool Optimize(ref BaseExpressionTreeItem result);

        public void Save(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified tree!");

            var nodeType = ExpressionTreeNodeType;
            writer.Write((int)nodeType);

            InternalSave(writer);
        }

        public abstract void Verify();
        public abstract ExpressionTreeNodeType ExpressionTreeNodeType { get; }

        public abstract TreeNodeKind NodeKind { get; }
        public BaseExpressionTreeItem Parent
        {
            get => parent;
            set
            {
                if (this.parent != null)
                    throw new CriticalException("Parent of expression tree node may be set only once!");

                this.parent = value ?? throw new CriticalException("Invalid parent!");
            }
        }

        public int Line => line;

        public int Column => column;
    }
}
