﻿using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Expressions
{
    class ConstantExpressionTreeLeaf : BaseExpressionTreeLeaf
    {
        protected string constName;

        protected override void InternalSave(BinaryWriter writer)
        {
            if (!verified)
                throw new CriticalException("Save can be called only on verified expression tree!");

            writer.Write(constName);
        }

        internal ConstantExpressionTreeLeaf(BinaryReader reader)
            : base(-1, -1)
        {
            constName = reader.ReadString();
        }

        public ConstantExpressionTreeLeaf(int line, int column)
            : base(line, column)
        {
            constName = "";
        }

        public override BaseExpressionTreeItem Clone()
        {
            ConstantExpressionTreeLeaf result = new ConstantExpressionTreeLeaf(-1, -1);
            result.ConstName = constName;

            return result;
        }

        public override void Verify()
        {
            if (!CommonTools.CheckConstantIdentifier(constName))
            {
                Invalidate();
                throw new CriticalException("Invalid identifier!");
            }

            verified = true;
        }

        public string ConstName
        {
            get => constName;
            set
            {
                this.constName = value;
                Invalidate();
            }
        }
        public override ExpressionTreeNodeType ExpressionTreeNodeType => ExpressionTreeNodeType.Constant;
    }
}
