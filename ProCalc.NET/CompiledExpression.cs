﻿using ProCalc.NET.ExpressionContainers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET
{
    public class CompiledExpression
    {
        internal Expression expression;

        internal CompiledExpression(Expression expression)
        {
            this.expression = expression;
        }

        internal Expression Expression => expression;
    }
}
