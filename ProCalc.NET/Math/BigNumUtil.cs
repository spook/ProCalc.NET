﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Math
{
    class BigNumUtil
    {
        private const int MAX_FRACTION_DIGITS = 10;

        private static int Compare(BigNum left, BigNum right)
        {
            int leftMaxExponent = left.GetMaxExponent();
            int rightMaxExponent = right.GetMaxExponent();

            if (leftMaxExponent > rightMaxExponent)
                return 1;
            else if (leftMaxExponent < rightMaxExponent)
                return -1;
            else
            {
                int leftMinExponent = left.GetMinExponent();
                int rightMinExponent = right.GetMinExponent();

                for (int i = System.Math.Max(leftMaxExponent, rightMaxExponent); i >= System.Math.Min(leftMinExponent, rightMinExponent); i--)
                {
                    var leftDigit = left.GetDigit(i);
                    var rightDigit = right.GetDigit(i);

                    if (leftDigit > rightDigit)
                        return 1;
                    else if (leftDigit < rightDigit)
                        return -1;
                }

                return 0;
            }
        }

        private static BigNum AddAbsoluteValues(BigNum left, BigNum right)
        {
            // Uwaga: metoda dodaje wartości bezwzględne obu liczb, *ignorując ich znaki*

            byte carry = 0;

            int minExponent = System.Math.Min(left.GetMinExponent(), right.GetMinExponent());
            int maxExponent = System.Math.Max(left.GetMaxExponent(), right.GetMaxExponent());

            BigNum result = new BigNum();

            for (int e = minExponent; e <= maxExponent || carry > 0; e++)
            {
                byte leftDigit = left.GetDigit(e);
                byte rightDigit = right.GetDigit(e);
                carry = (byte)(leftDigit + rightDigit + carry);

                result.SetDigit(e, (byte)(carry % 10));
                carry /= 10;
            }

            return result;
        }

        private static BigNum SubtractAbsoluteValues(BigNum left, BigNum right)
        {
            if (Compare(left, right) < 0)
                throw new ArgumentException("left", "It is required, that left value is bigger than right!");

            // Obliczenia

            BigNum result = new BigNum();

            DigitContainer tmpLeft = new DigitContainer(left);

            int minExponent = System.Math.Min(tmpLeft.GetMinExponent(), right.GetMinExponent());
            int maxExponent = System.Math.Max(tmpLeft.GetMaxExponent(), right.GetMaxExponent());

            for (int i = minExponent; i <= maxExponent; i++)
            {
                if (tmpLeft.GetDigit(i) < right.GetDigit(i))
                {
                    // Szukamy pierwszej niezerowej cyfry na pozycji większej od i
                    int j = i + 1;
                    while (j <= tmpLeft.GetMaxExponent() && tmpLeft.GetDigit(j) == 0)
                        j++;

                    // To nie powinno wystąpić
                    if (j > tmpLeft.GetMaxExponent())
                        throw new CriticalException("Internal error: corrupted left value!");

                    // Pożyczamy jedną jednostkę dla mniejszych rzędów
                    while (j > i)
                    {
                        tmpLeft.SetDigit(j, (byte)(tmpLeft.GetDigit(j) - 1));
                        j--;
                        tmpLeft.SetDigit(j, (byte)(tmpLeft.GetDigit(j) + 10));
                    }
                }

                result.SetDigit(i, (byte)(tmpLeft.GetDigit(i) - right.GetDigit(i)));
            }

            return result;
        }

        private static BigNum InternalAddition(BigNum left, BigNum right, Sign leftSign, Sign rightSign)
        {
            if (leftSign == rightSign)
            {
                BigNum result = AddAbsoluteValues(left, right);
                result.SetSign(leftSign);

                return result;
            }
            else
            {
                if (Compare(left, right) > 0)
                {
                    BigNum result = SubtractAbsoluteValues(left, right);
                    result.SetSign(leftSign);

                    return result;
                }
                else
                {
                    BigNum result = SubtractAbsoluteValues(right, left);
                    result.SetSign(rightSign);

                    return result;
                }
            }
        }

        private static BigNum AddSeries(List<BigNum> nums)
        {
            BigNum result = new BigNum();

            for (int i = 0; i < (int)nums.Count; i++)
                result = result + nums[i];

            return result;
        }

        private static BigNum InternalPower(BigNum @base, long exponent)
        {
            if (exponent == 0)
                return new BigNum(1);
            else if (exponent == 1)
                return @base;
            else if (exponent == 2)
                return @base * @base;
            else
            {
                BigNum tmp = InternalPower(@base, exponent / 2);

                BigNum result = tmp * tmp;

                if (exponent % 2 == 1)
                    result = result * @base;

                return result;
            }
        }

        public static BigNum Add(BigNum left, BigNum right)
        {
            return InternalAddition(left, right, left.GetSign(), right.GetSign());
        }

        public static BigNum Subtract(BigNum left, BigNum right)
        {
            return InternalAddition(left, right, left.GetSign(), right.GetSign().Negate());
        }

        public static BigNum Multiply(BigNum left, BigNum right)
        {
            if (left.IsZero() || right.IsZero())
                return new BigNum(0);

            BigNum result = new BigNum();

            int maxExponent = left.GetMaxExponent() + right.GetMaxExponent() + 1;
            int minExponent = left.GetMinExponent() + right.GetMinExponent();
            int resultRange = maxExponent - minExponent + 1;
            int resultSteps = right.GetMaxExponent() - right.GetMinExponent() + 1;

            byte[] partialResults = new byte[resultRange * resultSteps];

            for (int r = right.GetMinExponent(); r <= right.GetMaxExponent(); r++)
            {
                int tmpCarry = 0;
                for (int l = left.GetMinExponent(); l <= left.GetMaxExponent() || tmpCarry != 0; l++)
                {
                    tmpCarry += (right.GetDigit(r) * left.GetDigit(l));

                    partialResults[(r - right.GetMinExponent()) * resultRange + ((r + l) - minExponent)] = (byte)(tmpCarry % 10);
                    tmpCarry /= 10;
                }
            }

            long carry = 0;
            for (int d = 0; d < resultRange; d++)
            {
                for (int i = 0; i < resultSteps; i++)
                    carry += partialResults[i * resultRange + d];

                result.SetDigit(d + minExponent, (byte)(carry % 10));
                carry /= 10;
            }

            if (carry != 0)
                throw new CriticalException("Multiplication algorithm is corrupted!");

            if (left.GetSign() == right.GetSign())
                result.SetSign(Sign.sPositive);
            else
                result.SetSign(Sign.sNegative);

            return result;
        }

        public static BigNum Power(BigNum @base, long exponent)
        {
            if (exponent < 0)
                return new BigNum(1) / (InternalPower(@base, -exponent));
            else if (exponent > 0)
                return InternalPower(@base, exponent);
            else
                return new BigNum(1);
        }

        public static BigNum Divide(BigNum left, BigNum right)
        {
            if (right.IsZero())
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            BigNum result = new BigNum();

            BigNum tmpLeft = left;
            tmpLeft.SetSign(Sign.sPositive);
            BigNum tmpRight = right;
            tmpRight.SetSign(Sign.sPositive);

            int fractionDigits = 0;
            int baseExponent = tmpLeft.GetMaxExponent() - tmpRight.GetMaxExponent();

            List<BigNum> multiples = new List<BigNum>();

            while (!(tmpLeft.IsZero()) && fractionDigits < MAX_FRACTION_DIGITS)
            {
                // Szukamy wielokrotności right, licząc od (baseExponent) pozycji. 
                byte multiple = 0;
                bool smaller = false;

                do
                {
                    multiple++;

                    // Korzystamy ze zcache'owanych wielokrotności (po co liczyć je kilka razy?)
                    while (multiples.Count <= multiple)
                    {
                        BigNum tmp = tmpRight * new BigNum((int)multiples.Count);
                        multiples.Add(tmp);
                    }

                    //smaller = (Compare(multiples[multiple] << baseExponent, tmpLeft)) <= 0;

                    int maxExponent = System.Math.Max(tmpLeft.GetMaxExponent(), multiples[multiple].GetMaxExponent() + baseExponent);
                    int minExponent = System.Math.Min(tmpLeft.GetMinExponent(), multiples[multiple].GetMinExponent() + baseExponent);

                    bool @checked = false;
                    for (int i = maxExponent; i >= minExponent && !@checked; i--)
                    {
                        if (multiples[multiple].GetDigit(i - baseExponent) > tmpLeft.GetDigit(i))
                        {
                            smaller = false;
                            @checked = true;
                        }
                        else if (multiples[multiple].GetDigit(i - baseExponent) < tmpLeft.GetDigit(i))
                        {
                            smaller = true;
                            @checked = true;
                        }
                    }

                    if (!@checked)

                        smaller = true;
                }
                while (smaller);

                // Znaleziona wartość multiple, to pierwsza, która przekracza fragment liczby left,
                // dlatego musimy zmniejszyć multiple o jeden.
                multiple--;

                if (multiple > 9)
                    throw new CriticalException("Division algorithm is corrupted!");

                // Multiple jest kolejną cyfrą wyniku.
                result.SetDigit(baseExponent, multiple);

                if (baseExponent < 0)
                    fractionDigits++;

                // TODO: Do zoptymalizowania
                tmpLeft = tmpLeft - (multiples[multiple] << baseExponent);

                baseExponent--;
            }

            if (left.GetSign() == right.GetSign())
                result.SetSign(Sign.sPositive);
            else
                result.SetSign(Sign.sNegative);

            return result;
        }

        public static BigNum UnaryMinus(BigNum value)
        {
            BigNum result = new BigNum(value);
            result.SetSign(result.GetSign().Negate());

            return result;
        }

        public static bool Equal(BigNum left, BigNum right)
        {
            return (Compare(left, right) == 0);
        }

        public static bool LessThan(BigNum left, BigNum right)
        {
            return (Compare(left, right) < 0);
        }

        public static bool MoreThan(BigNum left, BigNum right)
        {
            return (Compare(left, right) > 0);
        }

        public static bool LessThanOrEqual(BigNum left, BigNum right)
        {
            return (Compare(left, right) <= 0);
        }

        public static bool MoreThanOrEqual(BigNum left, BigNum right)
        {
            return (Compare(left, right) >= 0);
        }

        public static BigNum ShiftLeft(BigNum left, int right)
        {
            BigNum result = new BigNum(left);
            result.ShiftLeft(right);
            return result;
        }

        public static BigNum ShiftRight(BigNum left, int right)
        {
            BigNum result = new BigNum(left);
            result.ShiftRight(right);
            return result;
        }
    }
}
