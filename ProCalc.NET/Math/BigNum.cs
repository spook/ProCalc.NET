﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Math
{
    public class BigNum : DigitContainer
    {
        private Sign sign;

        private void Parse(string source)
        {
            int sourcePos = 0;

            intPart.Clear();
            fracPart.Clear();
            sign = Sign.sPositive;

            // Akceptowany format:
            // [-]?[0-9]+(\.[0-9]*)?

            // Znak
            if (sourcePos >= source.Length)
                throw new ArgumentException("source", "Invalid number format!");

            if (source[sourcePos] == '-')
            {
                sign = Sign.sNegative;
                sourcePos++;
            }
            else
                sign = Sign.sPositive;

            // Cyrfy
            if (sourcePos >= source.Length)
                throw new ArgumentException("source", "Invalid numeric format!");

            int intCount = 0;
            while (sourcePos < source.Length && source[sourcePos] >= '0' && source[sourcePos] <= '9')
            {
                intCount++;
                if (intCount > MAX_DIGITS)
                    throw new ArgumentException("source", "Number too large!");

                byte digit = (byte)(source[sourcePos] - '0');
                intPart.Insert(0, digit);
                sourcePos++;
            }

            // Opcjonalna część ułamkowa
            if (sourcePos < source.Length && source[sourcePos] == '.')
            {
                sourcePos++;

                int fracCount = 0;
                while (sourcePos < source.Length && source[sourcePos] >= '0' && source[sourcePos] <= '9')
                {
                    fracCount++;
                    if (fracCount > MAX_DIGITS)
                        throw new ArgumentException("source", "Number too large!");

                    byte digit = (byte)(source[sourcePos] - '0');
                    fracPart.Add(digit);
                    sourcePos++;
                }
            }

            // Spodziewany koniec ciągu
            if (sourcePos < source.Length)
                throw new ArgumentException("source", "Invalid numeric format!");
        }

        private void Parse(int source)
        {
            // Znak
            if (source < 0)
                sign = Sign.sNegative;
            else
                sign = Sign.sPositive;

            while (source != 0)
            {
                if (sign == Sign.sPositive)
                {
                    byte digit = (byte)(source % 10);
                    intPart.Add(digit);
                    source /= 10;
                }
                else
                {
                    byte digit = (byte)(-(source % 10));
                    intPart.Add(digit);
                    source /= 10;
                }
            }
        }

        private void Parse(long source)
        {
            // Znak
            if (source < 0)
                sign = Sign.sNegative;
            else
                sign = Sign.sPositive;

            while (source != 0)
            {
                if (sign == Sign.sPositive)
                {
                    byte digit = (byte)(source % 10);
                    intPart.Add(digit);
                    source /= 10;
                }
                else
                {
                    byte digit = (byte)(-(source % 10));
                    intPart.Add(digit);
                    source /= 10;
                }
            }
        }

        private void Compact()
        {
            while (intPart.Count > 0 && intPart.Last() == 0)
                intPart.RemoveAt(intPart.Count - 1);
            while (fracPart.Count > 0 && fracPart.Last() == 0)
                fracPart.RemoveAt(fracPart.Count - 1);
        }

        private void InternalShiftLeft(long steps)
        {
            if (intPart.Count > MAX_DIGITS || fracPart.Count > MAX_DIGITS)
                throw new CriticalException("Corrupted class!");

            if (steps < 0)
                throw new ArgumentException("steps", "steps cannot be smaller than zero!");

            if ((intPart.Count == 0 && fracPart.Count == 0) || steps == 0)
                return;

            Compact();

            int biggestExponent;
            if (intPart.Count > 0)
                biggestExponent = intPart.Count - 1;
            else
            {
                if (fracPart.Count > 0)
                {
                    biggestExponent = -1;
                    while (-(biggestExponent + 1) < fracPart.Count && biggestExponent >= -MAX_DIGITS && fracPart[-(biggestExponent + 1)] == 0)
                        biggestExponent--;

                    if (biggestExponent < -MAX_DIGITS)
                        throw new CriticalException("Corrupted class!");

                    if (biggestExponent + steps >= MAX_DIGITS)
                        throw new ArgumentException("steps", "Shifted value exceeds maximal digit count!");
                }
            }

            while (steps > 0 && fracPart.Count > 0)
            {
                intPart.Insert(0, fracPart[0]);
                fracPart.RemoveAt(0);
                steps--;
            }

            while (steps > 0)
            {
                intPart.Insert(0, 0);
                steps--;
            }
        }

        private void InternalShiftRight(long steps)
        {
            if (intPart.Count > MAX_DIGITS || fracPart.Count > MAX_DIGITS)
                throw new CriticalException("Corrupted class!");

            if (steps < 0)
                throw new ArgumentException("steps", "steps cannot be smaller than zero!");

            if ((intPart.Count == 0 && fracPart.Count == 0) || steps == 0)
                return;

            Compact();

            int smallestExponent;
            if (fracPart.Count > 0)
                smallestExponent = -(int)(fracPart.Count);
            else
            {
                if (intPart.Count > 0)
                {
                    smallestExponent = 0;
                    while (smallestExponent < (int)(intPart.Count) && smallestExponent < MAX_DIGITS && intPart[smallestExponent] == 0)
                        smallestExponent++;

                    if (smallestExponent >= MAX_DIGITS)
                        throw new CriticalException("Corrupted class!");

                    if (smallestExponent - steps < -MAX_DIGITS)
                        throw new ArgumentException("steps", "Shifted value exceeds maximal digit count!");
                }
            }

            while (steps > 0 && intPart.Count > 0)
            {
                fracPart.Insert(0, intPart[0]);
                intPart.RemoveAt(0);
                steps--;
            }

            while (steps > 0)
            {
                fracPart.Insert(0, 0);
                steps--;
            }
        }

        public BigNum()
        {
            sign = Sign.sPositive;
        }

        public BigNum(BigNum source)
                    : base(source)
        {

            sign = source.GetSign();
        }

        public BigNum(int source)
        {
            sign = Sign.sPositive;
            Parse(source);
        }

        public BigNum(long source)
        {
            sign = Sign.sPositive;
            Parse(source);
        }

        public BigNum(string source)
        {
            sign = Sign.sPositive;
            Parse(source);
        }

        public override void Clear()
        {
            base.Clear();
            sign = Sign.sPositive;
        }

        public Sign GetSign()
        {
            return sign;
        }

        public void SetSign(Sign newSign)
        {
            sign = newSign;
        }

        public override void SetDigit(int exponent, byte digit)
        {
            if (digit > 10)
                throw new ArgumentException("digit", $"Invalid digit ({digit})!");

            base.SetDigit(exponent, digit);

            Compact();
        }

        public bool IsZero()
        {
            return (intPart.Count == 0 && fracPart.Count == 0);
        }

        public void ShiftLeft(long steps)
        {
            if (steps > 0)
                InternalShiftLeft(steps);
            else
                InternalShiftRight(-steps);
        }

        public void ShiftRight(long steps)
        {
            if (steps > 0)
                InternalShiftRight(steps);
            else
                InternalShiftLeft(steps);
        }

        public BigNum Clone()
        {
            BigNum result = new BigNum();
            result.SetSign(sign);

            int minExponent = GetMinExponent();
            int maxExponent = GetMaxExponent();

            for (int i = minExponent; i <= maxExponent; i++)
                result.SetDigit(i, GetDigit(i));

            return result;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            if (sign == Sign.sNegative)
                result.Append("-");

            if (intPart.Count == 0)
            {
                result.Append("0");
            }
            else
            {
                for (int i = intPart.Count - 1; i >= 0; i--)
                    result.Append(intPart[i]);
            }

            if (fracPart.Count > 0)
            {
                result.Append(".");

                for (int i = 0; i < fracPart.Count; i++)
                    result.Append(fracPart[i]);
            }

            return result.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is BigNum && BigNumUtil.Equal(this, (BigNum)obj);
        }

        public override int GetHashCode() => base.GetHashCode();

        public static BigNum operator +(BigNum left, BigNum right)
        {
            return BigNumUtil.Add(left, right);
        }

        public static BigNum operator -(BigNum left, BigNum right)
        {
            return BigNumUtil.Subtract(left, right);
        }

        public static BigNum operator *(BigNum left, BigNum right)
        {
            return BigNumUtil.Multiply(left, right);
        }

        public static BigNum operator ^(BigNum @base, long exponent)
        {
            return BigNumUtil.Power(@base, exponent);
        }

        public static BigNum operator /(BigNum left, BigNum right)
        {
            return BigNumUtil.Divide(left, right);
        }

        public static BigNum operator -(BigNum value)
        {
            return BigNumUtil.UnaryMinus(value);
        }

        public static bool operator ==(BigNum left, BigNum right)
        {
            return BigNumUtil.Equal(left, right);
        }

        public static bool operator !=(BigNum left, BigNum right)
        {
            return !BigNumUtil.Equal(left, right);
        }

        public static bool operator <(BigNum left, BigNum right)
        {
            return BigNumUtil.LessThan(left, right);
        }

        public static bool operator >(BigNum left, BigNum right)
        {
            return BigNumUtil.MoreThan(left, right);
        }

        public static bool operator <=(BigNum left, BigNum right)
        {
            return BigNumUtil.LessThanOrEqual(left, right);
        }

        public static bool operator >=(BigNum left, BigNum right)
        {
            return BigNumUtil.MoreThanOrEqual(left, right);
        }

        public static BigNum operator <<(BigNum left, int right)
        {
            return BigNumUtil.ShiftLeft(left, right);
        }

        public static BigNum operator >>(BigNum left, int right)
        {
            return BigNumUtil.ShiftRight(left, right);
        }
    }
}
