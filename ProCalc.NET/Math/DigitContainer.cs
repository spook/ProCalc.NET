﻿using ProCalc.NET.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Math
{
    public class DigitContainer
    {
        protected const int MAX_DIGITS = 2000000;

        protected List<byte> intPart = new List<byte>();
        protected List<byte> fracPart = new List<byte>();

        public DigitContainer()
        {

        }

        public DigitContainer(DigitContainer source)
        {
            int minExponent = source.GetMinExponent();
            int maxExponent = source.GetMaxExponent();

            for (int i = minExponent; i <= maxExponent; i++)
                SetDigit(i, source.GetDigit(i));
        }

        public int GetMinExponent()
        {
            if (fracPart.Count == 0)
                return 0;

            // First non-zero digit of fractional part, counting from end
            int i = fracPart.Count - 1;
            while (i >= 0 && fracPart[i] == 0)
                i--;

            return -(i + 1);
        }

        public int GetMaxExponent()
        {
            if (intPart.Count == 0)
                return 0;

            // First nonzero digit of integral part, counting from end

            int i = intPart.Count - 1;
            while (i >= 0 && intPart[i] == 0)
                i--;

            return i;
        }

        public byte GetDigit(int exponent)
        {
            if (exponent >= MAX_DIGITS || exponent < -MAX_DIGITS)
                throw new ArgumentOutOfRangeException("exponent", "Exponent outside allowed digit count!");

            if (exponent >= 0)
            {
                if (exponent < intPart.Count)
                    return intPart[exponent];
                else
                    return 0;
            }
            else
            {
                int index = -(exponent + 1);
                if (index < fracPart.Count)
                    return fracPart[index];
                else
                    return 0;
            }
        }

        public virtual void SetDigit(int exponent, byte digit)
        {
            if (exponent >= MAX_DIGITS || exponent < -MAX_DIGITS)
                throw new ArgumentOutOfRangeException("exponent", "Exponent outside allowed digit count!");

            void setDigit(List<byte> v, int exp, byte d)
            {
                if (exp < v.Count)
                    v[exp] = d;
                else
                {
                    if (d != 0)
                        while (v.Count < exp)
                            v.Add(0);

                    v.Add(d);
                }
            }

            if (exponent >= 0)
                setDigit(intPart, exponent, digit);
            else
            {
                int index = -(exponent + 1);

                setDigit(fracPart, index, digit);
            }
        }

        public virtual void Clear()
        {
            intPart.Clear();
            fracPart.Clear();
        }
    }
}
