﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Math;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Arithmetic
{
    delegate BaseNumeric UnaryOperatorMethod(BaseNumeric op);

    delegate BaseNumeric BinaryOperatorMethod(BaseNumeric op1, BaseNumeric op2);

    delegate BaseNumeric FunctionMethod(List<BaseNumeric> args);

    delegate BaseNumeric IndexerMethod(BaseNumeric value, List<BaseNumeric> args);

    delegate BaseNumeric BuildMatrixMethod(int width, int height, BaseNumeric[] data);

    delegate BaseNumeric BuildSequenceMethod(int count, BaseNumeric[] data);

    public class Arithmetics
    {
        // Private fields -----------------------------------------------------

        private static readonly Dictionary<string, FunctionMethod> functionMappings = new Dictionary<string, FunctionMethod>
        {
            { "sin", Sin },
            { "cos", Cos },
            { "tan", Tan },
            { "ctg", Ctg },
            { "arcsin", ArcSin },
            { "arccos", ArcCos },
            { "arctan", ArcTan },
            { "arcctg", ArcCtg },
            { "abs", Abs },
            { "round", Round },
            { "trunc", Trunc },
            { "frac", Frac },
            { "sqrt", Sqrt },
            { "sqr", Sqr },
            { "ln", Ln },
            { "length", Length },
            { "copy", Copy },
            { "pos", Pos },
            { "insert", Insert },
            { "delete", Delete },
            { "uppercase", Uppercase },
            { "lowercase", Lowercase },
            { "strtoint", StrToInt },
            { "inttostr", IntToStr },
            { "dot", Dot },
            { "distance", Distance },
            { "project", Project },
        };

        private static readonly Dictionary<string, BinaryOperatorMethod> binaryOperatorMappings = new Dictionary<string, BinaryOperatorMethod>
        {
            { "+", Add },
            { "-", Subtract },
            { "*", Multiply },
            { "/", Divide },
            { "\\", IntDivide },
            { "%", Modulo },
            { "^", Power },
            { "<<", ShiftLeft },
            { ">>", ShiftRight },
            { "<", LessThan },
            { ">", MoreThan },
            { "<=", LessThanOrEqual },
            { ">=", MoreThanOrEqual },
            { "==", Equal },
            { "!=", Different },
            { "&", And },
            { "|", Or },
            { "#", Xor }
        };

        private static readonly Dictionary<string, UnaryOperatorMethod> unaryOperatorMappings = new Dictionary<string, UnaryOperatorMethod>
        {
            { "-", UnaryMinus },
            { "!", Not }
        };

        private static readonly Dictionary<string, BaseNumeric> identifierMappings = new Dictionary<string, BaseNumeric>
        {
            { "pi", new FloatNumeric(System.Math.PI) },
            { "e", new FloatNumeric(System.Math.E) },
            { "true", new BoolNumeric(true) },
            { "false", new BoolNumeric(false) }
        };

        // Private methods ----------------------------------------------------

        #region Helpers

        private static void ValidateSpatialSizesEqal(params SpatialNumeric[] numeric)
        {
            for (int i = 1; i < numeric.Count(); i++)
                if (numeric[i].Count != numeric[0].Count)
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_VECTOR_SIZE);
        }

        private static BaseNumeric EvalVectorLength(VectorNumeric vector)
        {
            BaseNumeric two = new IntNumeric(2);
            BaseNumeric squareSum = Power(vector[0], two);
           
            for (int i = 1; i < vector.Count; i++)
            {
                squareSum = Add(squareSum, Power(vector[i], two));
            }

            var result = Power(squareSum, new FloatNumeric(1.0 / vector.Count));

            return result;
        }

        private static BaseNumeric DotProduct(SpatialNumeric vector1, SpatialNumeric vector2)
        {
            ValidateSpatialSizesEqal(vector1, vector2);

            BaseNumeric result = Multiply(vector1[0], vector2[0]);

            for (int i = 1; i < vector1.Count; i++)
            {
                var product = Multiply(vector1[i], vector2[i]);
                result = Add(result, product);
            }

            return result;
        }

        private static BaseNumeric MultiplyMatrixByScalar(MatrixNumeric matrix, ScalarNumeric scalar)
        {
            int tmpWidth = matrix.Width;
            int tmpHeight = matrix.Height;

            BaseNumeric[] tmpData = new BaseNumeric[tmpWidth * tmpHeight];

            // TODO optimize

            for (int i = 0; i < tmpWidth * tmpHeight; i++)
                tmpData[i] = Multiply(matrix[i], scalar);

            return new MatrixNumeric(tmpWidth, tmpHeight, tmpData);
        }

        private static BaseNumeric MultiplyScalarByMatrix(ScalarNumeric scalar, MatrixNumeric matrix)
        {
            int tmpWidth = matrix.Width;
            int tmpHeight = matrix.Height;

            BaseNumeric[] tmpData = new BaseNumeric[tmpWidth * tmpHeight];

            for (int i = 0, i_max = tmpWidth * tmpHeight; i < i_max; i++)
                tmpData[i] = Multiply(scalar, matrix[i]);

            return new MatrixNumeric(tmpWidth, tmpHeight, tmpData);
        }

        private static BaseNumeric MultiplyMatrixByMatrix(MatrixNumeric left, MatrixNumeric right)
        {
            if (left.Width < 1 || left.Height < 1 || right.Width < 1 || right.Height < 1)
                throw new CriticalException("Invalid dimensions of multiplied matrices!");

            if (left.Width != right.Height)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_MULTIPLIED_MATRIX_SIZES);

            int newWidth = right.Width;
            int newHeight = left.Height;
            int commonSize = left.Width;

            BaseNumeric[] tmpData = new BaseNumeric[newWidth * newHeight];

            for (int row = 0, row_max = left.Height; row < row_max; row++)
            {
                for (int col = 0, col_max = right.Width; col < col_max; col++)
                {
                    BaseNumeric subSum = null;
                    BaseNumeric tmpSum = null;
                    BaseNumeric subProduct = null;

                    subSum = Multiply(left[0, row], right[col, 0]);

                    if (commonSize > 1)
                        for (int i = 1; i < commonSize; i++)
                        {
                            subProduct = Multiply(left[i, row], right[col, i]);
                            tmpSum = subSum;
                            subSum = Add(tmpSum, subProduct);
                        }

                    tmpData[row * newWidth + col] = subSum;
                }
            }

            return new MatrixNumeric(newWidth, newHeight, tmpData);
        }

        private static BaseNumeric RealPow(double left, double right)
        {
            if (left > 0)
            {
                double result = System.Math.Pow(left, right);
                if (Double.IsInfinity(result))
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                return new FloatNumeric(result);
            }
            else
            {
                // Ujemną liczbę podnoszoną do rzeczywistej potęgi traktujemy jak zespoloną

                double complexModule = (double)System.Math.Abs(left);
                double complexAngle = System.Math.PI;

                double modulePower = System.Math.Pow(complexModule, right);
                double angleMultiplier = complexAngle * right;

                double realPart = modulePower * System.Math.Cos(angleMultiplier);
                double imPart = modulePower * System.Math.Sin(angleMultiplier);

                if (Double.IsInfinity(realPart) || Double.IsInfinity(imPart))
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                return new ComplexNumeric(realPart, imPart);
            }
        }

        private static BaseNumeric ComplexPow(double realPart, double imPart, double exponent)
        {
            double module = System.Math.Sqrt(realPart * realPart + imPart * imPart);
            double angle = System.Math.Acos(realPart / module);

            if (imPart < 0)
                angle = 2 * System.Math.PI - angle;

            double modulePower = System.Math.Pow(module, exponent);
            double angleMultiplier = angle * exponent;

            double realResult = modulePower * System.Math.Cos(angleMultiplier);
            double imResult = modulePower * System.Math.Sin(angleMultiplier);

            if (Double.IsInfinity(realResult) || Double.IsInfinity(imPart))
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

            return new ComplexNumeric(realResult, imResult);
        }

        private static BaseNumeric AddVectorToVector(VectorNumeric s1, VectorNumeric s2)
        {
            ValidateSpatialSizesEqal(s1, s2);

            ScalarNumeric[] result = new ScalarNumeric[s1.Count];

            for (int i = 0; i < s1.Count; i++)
                result[i] = (ScalarNumeric)Add(s1[i], s2[i]);

            return new VectorNumeric(result);
        }

        private static BaseNumeric SubtractVectors(VectorNumeric s1, VectorNumeric s2)
        {
            ValidateSpatialSizesEqal(s1, s2);

            ScalarNumeric[] result = new ScalarNumeric[s1.Count];

            for (int i = 0; i < s1.Count; i++)
                result[i] = (ScalarNumeric)Subtract(s1[i], s2[i]);

            return new VectorNumeric(result);
        }

        private static BaseNumeric MultiplyVectorByScalar(VectorNumeric s1, ScalarNumeric s2)
        {
            ScalarNumeric[] result = new ScalarNumeric[s1.Count];

            for (int i = 0; i < s1.Count; i++)
                result[i] = (ScalarNumeric)Multiply(s1[i], s2);

            return new VectorNumeric(result);
        }

        private static BaseNumeric DivideVectorByScalar(VectorNumeric s1, ScalarNumeric s2)
        {
            ScalarNumeric[] result = new ScalarNumeric[s1.Count];

            for (int i = 0; i < s1.Count; i++)
                result[i] = (ScalarNumeric)Divide(s1[i], s2);

            return new VectorNumeric(result);
        }

        private static BaseNumeric ProjectPointToLine(VectorNumeric point, VectorNumeric lineStart, VectorNumeric spanningVector)
        {
            VectorNumeric normalizationVector = (VectorNumeric)UnaryMinus(lineStart);

            VectorNumeric normalizedPoint = (VectorNumeric)Add(point, normalizationVector);

            var normalizedProjection = (VectorNumeric)Multiply(Divide(DotProduct(spanningVector, normalizedPoint), DotProduct(spanningVector, spanningVector)), spanningVector);

            return Subtract(normalizedProjection, normalizationVector);
        }

        private static BaseNumeric ProjectPointToLine(VectorNumeric point, VectorNumeric spanningVector)
        {
            return (VectorNumeric)Multiply(Divide(DotProduct(spanningVector, point), DotProduct(spanningVector, spanningVector)), spanningVector);
        }

        #endregion

        #region Binary operators       

        private static BaseNumeric Add(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (SafeArithmetics.SafeSignedAdd(lval.Value, rval.Value, out long result))
                                    {
                                        return new IntNumeric(result);
                                    }
                                    else
                                    {
                                        double doubleResult = (double)(lval.Value) + (double)(rval.Value);
                                        if (Double.IsInfinity(doubleResult))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(doubleResult);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.Value + rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long tmpDenominator = 0;
                                    long tmpNumerator = 0;
                                    long tmpHalfwayResult = 0;

                                    bool operationSafe = SafeArithmetics.SafeSignedMultiply(lval.Value, rval.Denominator, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedAdd(tmpHalfwayResult, rval.Numerator, out tmpNumerator);
                                    tmpDenominator = rval.Denominator;

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.Value + rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double result = lval.Value + rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, rval.ImValue);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    BigNum result = new BigNum(lval.Value) + rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = lval.Value + rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.Value + rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    double result = lval.Value + rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double result = lval.Value + rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, rval.ImValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long tmpNumerator = 0;
                                    long tmpDenominator = 0;
                                    long tmpHalfwayResult = 0;

                                    bool operationSafe = SafeArithmetics.SafeSignedMultiply(rval.Value, lval.Denominator, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedAdd(lval.Numerator, tmpHalfwayResult, out tmpNumerator);
                                    tmpDenominator = lval.Denominator;

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue + rval.Value;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.RealValue + rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long tmpNumerator = 0;
                                    long tmpDenominator = 0;

                                    bool operationSafe;

                                    if (lval.Denominator == rval.Denominator)
                                    {
                                        operationSafe = SafeArithmetics.SafeSignedAdd(lval.Numerator, rval.Numerator, out tmpNumerator);
                                        tmpDenominator = lval.Denominator;
                                    }
                                    else
                                    {
                                        long lrGCD = MathTools.GCD(lval.Denominator, rval.Denominator);

                                        long leftMultiplier = 0;
                                        long rightMultiplier = 0;
                                        long commonDenominator = 0;
                                        long halfwayResult = 0;
                                        long leftSide = 0;
                                        long rightSide = 0;

                                        operationSafe = SafeArithmetics.SafeSignedDivide(rval.Denominator, lrGCD, out leftMultiplier) &&
                                            SafeArithmetics.SafeSignedDivide(lval.Denominator, lrGCD, out rightMultiplier) &&
                                            SafeArithmetics.SafeSignedMultiply(leftMultiplier, rightMultiplier, out halfwayResult) &&
                                            SafeArithmetics.SafeSignedMultiply(halfwayResult, lrGCD, out commonDenominator) &&
                                            SafeArithmetics.SafeSignedMultiply(lval.Numerator, leftMultiplier, out leftSide) &&
                                            SafeArithmetics.SafeSignedMultiply(rval.Numerator, rightMultiplier, out rightSide) &&
                                            SafeArithmetics.SafeSignedAdd(leftSide, rightSide, out tmpNumerator);
                                        tmpDenominator = commonDenominator;
                                    }

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue + rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double result = lval.RealValue + rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, rval.ImValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric lval = (ComplexNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = lval.RealValue + rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, lval.ImValue);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.RealValue + rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, lval.ImValue);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    double result = lval.RealValue + rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, lval.ImValue);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double realResult = lval.RealValue + rval.RealValue;
                                    double imResult = lval.ImValue + rval.ImValue;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.List:
                    {
                        ListNumeric lval = (ListNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.List:
                                {
                                    ListNumeric rval = (ListNumeric)right;

                                    int lCount = lval.Count;
                                    int rCount = rval.Count;

                                    BaseNumeric[] tmpData = new BaseNumeric[lCount + rCount];

                                    for (int i = 0; i < lCount; i++)
                                        tmpData[i] = lval[i].Clone();

                                    for (int i = 0; i < rCount; i++)
                                        tmpData[i + lCount] = rval[i].Clone();

                                    return new ListNumeric(tmpData);

                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Matrix:
                    {
                        MatrixNumeric lval = (MatrixNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    if (lval.Width != rval.Width || lval.Height != rval.Height)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_ADDED_MATRIX_SIZES);

                                    int tmpWidth = lval.Width;
                                    int tmpHeight = lval.Height;

                                    BaseNumeric[] tmpData = new BaseNumeric[lval.Width * lval.Height];

                                    for (int i = 0, i_max = tmpWidth * tmpHeight; i < i_max; i++)
                                        tmpData[i] = null;

                                    for (int i = 0, i_max = tmpWidth * tmpHeight; i < i_max; i++)
                                        tmpData[i] = Add(lval[i], rval[i]);

                                    return new MatrixNumeric(tmpWidth, tmpHeight, tmpData);

                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.String:
                    {
                        StringNumeric lval = (StringNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.String:
                                {
                                    StringNumeric rval = (StringNumeric)right;

                                    return new StringNumeric(lval.Value + rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.LargeNumber:
                    {
                        LargeNumberNumeric lval = (LargeNumberNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    BigNum result = lval.Value + new BigNum(rval.Value);

                                    return new LargeNumberNumeric(result);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    BigNum result = lval.Value + rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }                        
                    }
                case NumericType.Vector:
                    {
                        VectorNumeric vector = (VectorNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Vector:
                                {
                                    VectorNumeric vector2 = (VectorNumeric)right;

                                    return AddVectorToVector(vector, vector2);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Subtract(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long result;

                                    if (SafeArithmetics.SafeSignedSubtract(lval.Value, rval.Value, out result))
                                        return new IntNumeric(result);
                                    else
                                    {
                                        double doubleResult = (double)(lval.Value) - (double)(rval.Value);
                                        if (Double.IsInfinity(doubleResult))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(doubleResult);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.Value - rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long tmpNumerator = 0;
                                    long tmpDenominator = 0;
                                    long tmpHalfwayResult = 0;

                                    bool operationSafe = SafeArithmetics.SafeSignedMultiply(lval.Value, rval.Denominator, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedSubtract(tmpHalfwayResult, rval.Numerator, out tmpNumerator);
                                    tmpDenominator = rval.Denominator;

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = (double)lval.Value - rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double result = lval.Value - rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, -rval.ImValue);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    BigNum result = new BigNum(lval.Value) - rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = lval.Value - rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.Value - rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    double result = lval.Value - rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double result = lval.Value - rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, -rval.ImValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long tmpNumerator = 0; // = lval.GetNumerator() - rval.GetValue() * lval.GetDenominator();
                                    long tmpDenominator = 0; // = lval.GetDenominator();
                                    long tmpHalfwayResult = 0;

                                    bool operationSafe = SafeArithmetics.SafeSignedMultiply(rval.Value, lval.Denominator, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedSubtract(lval.Numerator, tmpHalfwayResult, out tmpNumerator);
                                    tmpDenominator = lval.Denominator;

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue - (double)rval.Value;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.RealValue - rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long tmpNumerator = 0;
                                    long tmpDenominator = 0;

                                    bool operationSafe;

                                    if (lval.Denominator == rval.Denominator)
                                    {
                                        operationSafe = SafeArithmetics.SafeSignedSubtract(lval.Numerator, rval.Numerator, out tmpNumerator);
                                        tmpDenominator = lval.Denominator;
                                    }
                                    else
                                    {
                                        long lrGCD = MathTools.GCD(lval.Denominator, rval.Denominator);
                                        long tmpHalfwayResult = 0;
                                        long leftMultiplier = 0;
                                        long rightMultiplier = 0;
                                        long commonDenominator = 0;
                                        long leftSide = 0;
                                        long rightSide = 0;

                                        operationSafe = SafeArithmetics.SafeSignedDivide(rval.Denominator, lrGCD, out leftMultiplier) &&
                                            SafeArithmetics.SafeSignedDivide(lval.Denominator, lrGCD, out rightMultiplier) &&
                                            SafeArithmetics.SafeSignedMultiply(leftMultiplier, rightMultiplier, out tmpHalfwayResult) &&
                                            SafeArithmetics.SafeSignedMultiply(tmpHalfwayResult, lrGCD, out commonDenominator) &&
                                            SafeArithmetics.SafeSignedMultiply(lval.Numerator, leftMultiplier, out leftSide) &&
                                            SafeArithmetics.SafeSignedMultiply(rval.Numerator, rightMultiplier, out rightSide) &&
                                            SafeArithmetics.SafeSignedSubtract(leftSide, rightSide, out tmpNumerator);
                                        tmpDenominator = commonDenominator;
                                    }

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue - rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double result = lval.RealValue - rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, -rval.ImValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric lval = (ComplexNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = lval.RealValue - rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, lval.ImValue);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.RealValue - rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, lval.ImValue);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    double result = lval.RealValue - rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(result, lval.ImValue);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double realResult = lval.RealValue - rval.RealValue;
                                    double imResult = lval.ImValue - rval.ImValue;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Matrix:
                    {
                        MatrixNumeric lval = (MatrixNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    if (lval.Width != rval.Width || lval.Height != rval.Height)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_SUBTRACTED_MATRIX_SIZES);

                                    int tmpWidth = lval.Width;
                                    int tmpHeight = lval.Height;

                                    BaseNumeric[] tmpData = new BaseNumeric[lval.Width * lval.Height];

                                    for (int i = 0, i_max = tmpWidth * tmpHeight; i < i_max; i++)
                                        tmpData[i] = null;

                                    for (int i = 0, i_max = tmpWidth * tmpHeight; i < i_max; i++)
                                        tmpData[i] = Subtract(lval[i], rval[i]);

                                    return new MatrixNumeric(tmpWidth, tmpHeight, tmpData);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.LargeNumber:
                    {
                        LargeNumberNumeric lval = (LargeNumberNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    BigNum result = lval.Value - new BigNum(rval.Value);

                                    return new LargeNumberNumeric(result);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    BigNum result = lval.Value - rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Vector:
                    {
                        VectorNumeric vector = (VectorNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Vector:
                                {
                                    VectorNumeric vector2 = (VectorNumeric)right;

                                    return SubtractVectors(vector, vector2);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Multiply(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long result;

                                    if (SafeArithmetics.SafeSignedMultiply(lval.Value, rval.Value, out result))
                                        return new IntNumeric(result);
                                    else
                                    {
                                        double doubleResult = (double)(lval.Value) * (double)(rval.Value);
                                        if (Double.IsInfinity(doubleResult))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(doubleResult);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.Value * rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long tmpGCD = MathTools.GCD(lval.Value, rval.Denominator);

                                    long tmpNumerator = 0;
                                    long tmpDenominator = 0;
                                    long tmpHalfwayResult = 0;

                                    bool operationSafe = SafeArithmetics.SafeSignedDivide(lval.Value, tmpGCD, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedMultiply(tmpHalfwayResult, rval.Numerator, out tmpNumerator) &&
                                        SafeArithmetics.SafeSignedDivide(rval.Denominator, tmpGCD, out tmpDenominator);

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = (double)lval.Value * rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double realResult = lval.Value * rval.RealValue;
                                    double imResult = lval.Value * rval.ImValue;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(lval.Value * rval.RealValue, lval.Value * rval.ImValue);
                                }
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    return MultiplyScalarByMatrix(lval, rval);
                                }
                            case NumericType.String:
                                {
                                    StringNumeric rval = (StringNumeric)right;

                                    string tmpString = "";

                                    for (long i = 0, i_max = lval.Value; i < i_max; i++)
                                        tmpString += rval.Value;

                                    return new StringNumeric(tmpString);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    BigNum result = new BigNum(lval.Value) * rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            case NumericType.Vector:
                                {
                                    VectorNumeric rval = (VectorNumeric)right;

                                    return MultiplyVectorByScalar(rval, lval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = lval.Value * rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.Value * rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    double result = lval.Value * rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double realResult = lval.Value * rval.RealValue;
                                    double imResult = lval.Value * rval.ImValue;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    return MultiplyScalarByMatrix(lval, rval);
                                }
                            case NumericType.Vector:
                                {
                                    VectorNumeric rval = (VectorNumeric)right;

                                    return MultiplyVectorByScalar(rval, lval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long tmpGCD = MathTools.GCD(lval.Denominator, rval.Value);

                                    long tmpHalfwayResult = 0;
                                    long tmpNumerator = 0;// = lval.GetNumerator() * (rval.GetValue() / tmpGCD);
                                    long tmpDenominator = 0;// = lval.GetDenominator() / tmpGCD;

                                    bool operationSafe = SafeArithmetics.SafeSignedDivide(rval.Value, tmpGCD, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedMultiply(lval.Numerator, tmpHalfwayResult, out tmpNumerator) &&
                                        SafeArithmetics.SafeSignedDivide(lval.Denominator, tmpGCD, out tmpDenominator);

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue * (double)rval.Value;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double result = lval.RealValue * rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long tmpLeftNum = lval.Numerator;
                                    long tmpLeftDenom = lval.Denominator;
                                    long tmpRightNum = rval.Numerator;
                                    long tmpRightDenom = rval.Denominator;

                                    MathTools.ReduceFraction(ref tmpLeftNum, ref tmpRightDenom);
                                    MathTools.ReduceFraction(ref tmpRightNum, ref tmpLeftDenom);

                                    long tmpNumerator = 0;// = tmpLeftNum * tmpRightNum;
                                    long tmpDenominator = 0;// = tmpLeftDenom * tmpRightDenom;

                                    bool operationSafe = SafeArithmetics.SafeSignedMultiply(tmpLeftNum, tmpRightNum, out tmpNumerator) &&
                                        SafeArithmetics.SafeSignedMultiply(tmpLeftDenom, tmpRightDenom, out tmpDenominator);

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue * rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double realResult = lval.RealValue * rval.RealValue;
                                    double imResult = lval.RealValue * rval.ImValue;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    return MultiplyScalarByMatrix(lval, rval);
                                }
                            case NumericType.Vector:
                                {
                                    VectorNumeric rval = (VectorNumeric)right;

                                    return MultiplyVectorByScalar(rval, lval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric lval = (ComplexNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double realResult = lval.RealValue * rval.Value;
                                    double imResult = lval.ImValue * rval.Value;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    double realResult = lval.RealValue * rval.Value;
                                    double imResult = lval.ImValue * rval.Value;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    double realResult = lval.RealValue * rval.RealValue;
                                    double imResult = lval.ImValue * rval.RealValue;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double tmpLeftRe = lval.RealValue;
                                    double tmpLeftIm = lval.ImValue;
                                    double tmpRightRe = rval.RealValue;
                                    double tmpRightIm = rval.ImValue;

                                    double realResult = tmpLeftRe * tmpRightRe - tmpLeftIm * tmpRightIm;
                                    double imResult = tmpLeftIm * tmpRightRe + tmpLeftRe * tmpRightIm;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    return MultiplyScalarByMatrix(lval, rval);
                                }
                            case NumericType.Vector:
                                {
                                    VectorNumeric rval = (VectorNumeric)right;

                                    return MultiplyVectorByScalar(rval, lval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.String:
                    {
                        StringNumeric lval = (StringNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    string tmpString = "";

                                    for (long i = 0, i_max = rval.Value; i < i_max; i++)
                                        tmpString += lval.Value;

                                    return new StringNumeric(tmpString);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Matrix:
                    {
                        MatrixNumeric lval = (MatrixNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                            case NumericType.Float:
                            case NumericType.IntFraction:
                            case NumericType.Complex:
                                {
                                    ScalarNumeric rval = (ScalarNumeric)right;

                                    return MultiplyMatrixByScalar(lval, rval);
                                }
                            case NumericType.Matrix:
                                {
                                    MatrixNumeric rval = (MatrixNumeric)right;

                                    return MultiplyMatrixByMatrix(lval, rval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.LargeNumber:
                    {
                        LargeNumberNumeric lval = (LargeNumberNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    BigNum result = lval.Value * new BigNum(rval.Value);

                                    return new LargeNumberNumeric(result);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    BigNum result = lval.Value * rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Vector:
                    {
                        VectorNumeric lval = (VectorNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                            case NumericType.Float:
                            case NumericType.IntFraction:
                            case NumericType.Complex:
                                {
                                    ScalarNumeric rval = (ScalarNumeric)right;

                                    return MultiplyVectorByScalar(lval, rval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }

                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Divide(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;
                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (rval.Value == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    long tmpNumerator = lval.Value;
                                    long tmpDenominator = rval.Value;

                                    MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                    if (tmpNumerator == 0)
                                        return new IntNumeric(0);
                                    else if (tmpDenominator == 1)
                                        return new IntNumeric(tmpNumerator);
                                    else
                                        return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    if (System.Math.Abs(rval.Value) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double result = lval.Value / rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    if (rval.Numerator == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    long tmpGCD = MathTools.GCD(lval.Value, rval.Numerator);

                                    long tmpHalfwayResult = 0;
                                    long tmpNumerator = 0;// = (lval.GetValue() / tmpGCD) * rval.GetDenominator();
                                    long tmpDenominator = 0;// = (rval.GetNumerator() / tmpGCD);

                                    bool operationSafe = SafeArithmetics.SafeSignedDivide(lval.Value, tmpGCD, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedMultiply(tmpHalfwayResult, rval.Denominator, out tmpNumerator) &&
                                        SafeArithmetics.SafeSignedDivide(rval.Numerator, tmpGCD, out tmpDenominator);

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = (double)lval.Value / rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    // a + bi   (ac + bd) + (bc - ad)i
                                    // ------ = ----------------------
                                    // c + di         c^2 + d^2

                                    long leftReal = lval.Value;
                                    double rightReal = rval.RealValue;
                                    double rightIm = rval.ImValue;

                                    double tmpDenominator = (rightReal * rightReal + rightIm * rightIm);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal) / tmpDenominator;
                                    double imResult = -(leftReal * rightIm) / tmpDenominator;

                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    if (rval.Value.IsZero())
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    BigNum result = new BigNum(lval.Value) / rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (rval.Value == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double result = lval.Value / rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    if (System.Math.Abs(rval.Value) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double result = lval.Value / rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    if (rval.Numerator == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double result = lval.Value / rval.RealValue;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    // a + bi   (ac + bd) + (bc - ad)i
                                    // ------ = ----------------------
                                    // c + di         c^2 + d^2

                                    double leftReal = lval.Value;
                                    double rightReal = rval.RealValue;
                                    double rightIm = rval.ImValue;

                                    double tmpDenominator = (rightReal * rightReal + rightIm * rightIm);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal) / tmpDenominator;
                                    double imResult = -(leftReal * rightIm) / tmpDenominator;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (rval.Value == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    long tmpGCD = MathTools.GCD(lval.Numerator, rval.Value);

                                    long tmpHalfwayResult = 0;
                                    long tmpNumerator = 0;// = lval.GetNumerator() / tmpGCD;
                                    long tmpDenominator = 0;// = lval.GetDenominator() * (rval.GetValue() / tmpGCD);

                                    bool operationSafe = SafeArithmetics.SafeSignedDivide(lval.Numerator, tmpGCD, out tmpNumerator) &&
                                        SafeArithmetics.SafeSignedDivide(rval.Value, tmpGCD, out tmpHalfwayResult) &&
                                        SafeArithmetics.SafeSignedMultiply(lval.Denominator, tmpHalfwayResult, out tmpDenominator);

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue / (double)rval.Value;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    if (System.Math.Abs(rval.Value) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double result = lval.RealValue / rval.Value;
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    if (rval.Numerator == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    long tmpLeftNum = lval.Numerator;
                                    long tmpLeftDenom = lval.Denominator;
                                    long tmpRightNum = rval.Numerator;
                                    long tmpRightDenom = rval.Denominator;

                                    MathTools.ReduceFraction(ref tmpLeftNum, ref tmpRightNum);
                                    MathTools.ReduceFraction(ref tmpLeftDenom, ref tmpRightDenom);

                                    long tmpNumerator = 0;// = tmpLeftNum * tmpRightDenom;
                                    long tmpDenominator = 0;// = tmpLeftDenom * tmpRightNum;

                                    bool operationSafe = SafeArithmetics.SafeSignedMultiply(tmpLeftNum, tmpRightDenom, out tmpNumerator) &&
                                        SafeArithmetics.SafeSignedMultiply(tmpLeftDenom, tmpRightNum, out tmpDenominator);

                                    if (operationSafe)
                                    {
                                        MathTools.ReduceFraction(ref tmpNumerator, ref tmpDenominator);

                                        if (tmpNumerator == 0)
                                            return new IntNumeric(0);
                                        else if (tmpDenominator == 1)
                                            return new IntNumeric(tmpNumerator);
                                        else
                                            return new IntFractionNumeric(tmpNumerator, tmpDenominator);
                                    }
                                    else
                                    {
                                        double result = lval.RealValue / rval.RealValue;
                                        if (Double.IsInfinity(result))
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                        return new FloatNumeric(result);
                                    }
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    // a + bi   (ac + bd) + (bc - ad)i
                                    // ------ = ----------------------
                                    // c + di         c^2 + d^2

                                    double leftReal = lval.RealValue;
                                    double rightReal = rval.RealValue;
                                    double rightIm = rval.ImValue;

                                    double tmpDenominator = (rightReal * rightReal + rightIm * rightIm);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal) / tmpDenominator;
                                    double imResult = -(leftReal * rightIm) / tmpDenominator;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric lval = (ComplexNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    // a + bi   (ac + bd) + (bc - ad)i
                                    // ------ = ----------------------
                                    // c + di         c^2 + d^2

                                    double leftReal = lval.RealValue;
                                    double leftIm = lval.ImValue;
                                    long rightReal = rval.Value;

                                    double tmpDenominator = (double)(rightReal * rightReal);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal) / tmpDenominator;
                                    double imResult = (leftIm * rightReal) / tmpDenominator;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    // a + bi   (ac + bd) + (bc - ad)i
                                    // ------ = ----------------------
                                    // c + di         c^2 + d^2

                                    double leftReal = lval.RealValue;
                                    double leftIm = lval.ImValue;
                                    double rightReal = rval.Value;

                                    double tmpDenominator = (rightReal * rightReal);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal) / tmpDenominator;
                                    double imResult = (leftIm * rightReal) / tmpDenominator;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    // a + bi   (ac + bd) + (bc - ad)i
                                    // ------ = ----------------------
                                    // c + di         c^2 + d^2

                                    double leftReal = lval.RealValue;
                                    double leftIm = lval.ImValue;
                                    double rightReal = rval.RealValue;

                                    double tmpDenominator = (rightReal * rightReal);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal) / tmpDenominator;
                                    double imResult = (leftIm * rightReal) / tmpDenominator;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            case NumericType.Complex:
                                {
                                    ComplexNumeric rval = (ComplexNumeric)right;

                                    double leftReal = lval.RealValue;
                                    double leftIm = lval.ImValue;
                                    double rightReal = rval.RealValue;
                                    double rightIm = rval.ImValue;

                                    double tmpDenominator = (rightReal * rightReal + rightIm * rightIm);

                                    if (System.Math.Abs(tmpDenominator) < Double.Epsilon)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    double realResult = (leftReal * rightReal + leftIm * rightIm) / tmpDenominator;
                                    double imResult = (leftIm * rightReal - leftReal * rightIm) / tmpDenominator;
                                    if (Double.IsInfinity(realResult) || Double.IsInfinity(imResult))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new ComplexNumeric(realResult, imResult);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.LargeNumber:
                    {
                        LargeNumberNumeric lval = (LargeNumberNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (rval.Value == 0)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    BigNum result = lval.Value / new BigNum(rval.Value);

                                    return new LargeNumberNumeric(result);
                                }
                            case NumericType.LargeNumber:
                                {
                                    LargeNumberNumeric rval = (LargeNumberNumeric)right;

                                    if (rval.Value.IsZero())
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

                                    BigNum result = lval.Value / rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Vector:
                    {
                        VectorNumeric lval = (VectorNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                            case NumericType.Float:
                            case NumericType.IntFraction:
                            case NumericType.Complex:
                                {
                                    ScalarNumeric rval = (ScalarNumeric)right;

                                    return DivideVectorByScalar(lval, rval);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Modulo(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new IntNumeric(lval.Value % rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric IntDivide(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long result;

                                    if (SafeArithmetics.SafeSignedDivide(lval.Value, rval.Value, out result))
                                        return new IntNumeric(result);
                                    else
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INTEGER_OVERFLOW);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Power(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = System.Math.Pow((double)(lval.Value), (double)(rval.Value));
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return RealPow((double)lval.Value, (double)rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return RealPow((double)lval.Value, rval.RealValue);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = System.Math.Pow((double)(lval.Value), (double)(rval.Value));
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return RealPow(lval.Value, rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return RealPow(lval.Value, rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    double result = System.Math.Pow((double)(lval.RealValue), (double)(rval.Value));
                                    if (Double.IsInfinity(result))
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                                    return new FloatNumeric(result);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return RealPow(lval.RealValue, rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return RealPow(lval.RealValue, rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric lval = (ComplexNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return ComplexPow(lval.RealValue, lval.ImValue, (double)rval.Value);
                                }

                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return ComplexPow(lval.RealValue, lval.ImValue, rval.Value);
                                }

                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return ComplexPow(lval.RealValue, lval.ImValue, rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.LargeNumber:
                    {
                        LargeNumberNumeric lval = (LargeNumberNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    BigNum result = lval.Value ^ rval.Value;

                                    return new LargeNumberNumeric(result);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric MoreThan(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value > rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value > rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value > rval.RealValue);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value > rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value > rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value > rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.RealValue > rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.RealValue > rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.RealValue > rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric LessThan(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value < rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value < rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value < rval.RealValue);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value < rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value < rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value < rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.RealValue < rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.RealValue < rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.RealValue < rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric MoreThanOrEqual(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value >= rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value >= rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value >= rval.RealValue);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value >= rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value >= rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value >= rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.RealValue >= rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.RealValue >= rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.RealValue >= rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);

            }
        }

        private static BaseNumeric LessThanOrEqual(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value <= rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value <= rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value <= rval.RealValue);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value <= rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.Value <= rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.Value <= rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.RealValue <= rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(lval.RealValue <= rval.Value);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(lval.RealValue <= rval.RealValue);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Equal(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value == rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.Value) < Double.Epsilon);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long rNum = rval.Numerator;
                                    long rDenom = rval.Denominator;
                                    MathTools.ReduceFraction(ref rNum, ref rDenom);

                                    return new BoolNumeric(lval.Value == rNum && rDenom == 1);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.Value) < Double.Epsilon);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.Value) < Double.Epsilon);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.RealValue) < Double.Epsilon);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long lNum = lval.Numerator;
                                    long lDenom = lval.Denominator;
                                    MathTools.ReduceFraction(ref lNum, ref lDenom);

                                    return new BoolNumeric(lNum == rval.Value && lDenom == 1);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.RealValue - rval.Value) < Double.Epsilon);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long lNum = lval.Numerator;
                                    long lDenom = lval.Denominator;
                                    long rNum = rval.Numerator;
                                    long rDenom = rval.Denominator;

                                    MathTools.ReduceFraction(ref lNum, ref lDenom);
                                    MathTools.ReduceFraction(ref rNum, ref rDenom);

                                    return new BoolNumeric(lNum == rNum && lDenom == rDenom);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);

            }
        }

        private static BaseNumeric Different(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(lval.Value != rval.Value);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.Value) >= Double.Epsilon);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long rNum = rval.Numerator;
                                    long rDenom = rval.Denominator;
                                    MathTools.ReduceFraction(ref rNum, ref rDenom);

                                    return new BoolNumeric(lval.Value != rNum || rDenom != 1);
                                }

                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Float:
                    {
                        FloatNumeric lval = (FloatNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.Value) >= Double.Epsilon);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.Value) >= Double.Epsilon);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.Value - rval.RealValue) >= Double.Epsilon);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric lval = (IntFractionNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    long lNum = lval.Numerator;
                                    long lDenom = lval.Denominator;
                                    MathTools.ReduceFraction(ref lNum, ref lDenom);

                                    return new BoolNumeric(lNum != rval.Value || lDenom != 1);
                                }
                            case NumericType.Float:
                                {
                                    FloatNumeric rval = (FloatNumeric)right;

                                    return new BoolNumeric(System.Math.Abs(lval.RealValue - rval.Value) >= Double.Epsilon);
                                }
                            case NumericType.IntFraction:
                                {
                                    IntFractionNumeric rval = (IntFractionNumeric)right;

                                    long lNum = lval.Numerator;
                                    long lDenom = lval.Denominator;
                                    long rNum = rval.Numerator;
                                    long rDenom = rval.Denominator;

                                    MathTools.ReduceFraction(ref lNum, ref lDenom);
                                    MathTools.ReduceFraction(ref rNum, ref rDenom);

                                    return new BoolNumeric(lNum != rNum || lDenom != rDenom);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);

            }
        }

        private static BaseNumeric ShiftLeft(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (rval.Value > Int32.MaxValue || rval.Value < Int32.MinValue)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INTEGER_OVERFLOW);

                                    return new IntNumeric(lval.Value << (int)(rval.Value));
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric ShiftRight(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    if (rval.Value > Int32.MaxValue || rval.Value < Int32.MinValue)
                                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INTEGER_OVERFLOW);

                                    return new IntNumeric(lval.Value >> (int)(rval.Value));
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric And(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new IntNumeric(lval.Value & rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Bool:
                    {
                        BoolNumeric lval = (BoolNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Bool:
                                {
                                    BoolNumeric rval = (BoolNumeric)right;

                                    return new BoolNumeric(lval.Value && rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Or(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new IntNumeric(lval.Value | rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Bool:
                    {
                        BoolNumeric lval = (BoolNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Bool:
                                {
                                    BoolNumeric rval = (BoolNumeric)right;

                                    return new BoolNumeric(lval.Value || rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric Xor(BaseNumeric left, BaseNumeric right)
        {
            switch (left.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric lval = (IntNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric rval = (IntNumeric)right;

                                    return new IntNumeric(lval.Value ^ rval.Value);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                case NumericType.Bool:
                    {
                        BoolNumeric lval = (BoolNumeric)left;

                        switch (right.NumericType)
                        {
                            case NumericType.Bool:
                                {
                                    BoolNumeric rval = (BoolNumeric)right;

                                    return new BoolNumeric((!(lval.Value) && (rval.Value)) ||
                                        ((lval.Value) && !(rval.Value)));
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        #endregion

        #region Unary operators

        private static BaseNumeric Not(BaseNumeric value)
        {
            switch (value.NumericType)
            {
                case NumericType.Bool:
                    {
                        BoolNumeric val = (BoolNumeric)value;

                        return new BoolNumeric(!(val.Value));
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        private static BaseNumeric UnaryMinus(BaseNumeric value)
        {
            switch (value.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric val = (IntNumeric)value;

                        return new IntNumeric(-(val.Value));
                    }
                case NumericType.Float:
                    {
                        FloatNumeric val = (FloatNumeric)value;

                        double result = -(val.Value);
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric val = (IntFractionNumeric)value;

                        return new IntFractionNumeric(-(val.Numerator), val.Denominator);
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric val = (ComplexNumeric)value;

                        return new ComplexNumeric(-(val.RealValue), -(val.ImValue));
                    }
                case NumericType.Matrix:
                    {
                        MatrixNumeric val = (MatrixNumeric)value;

                        BaseNumeric[] values = null;

                        values = new BaseNumeric[val.Height * val.Width];

                        for (int i = 0, i_max = val.Height * val.Width; i < i_max; i++)
                        {
                            values[i] = UnaryMinus(val[i]);
                        }

                        return new MatrixNumeric(val.Width, val.Height, values);
                    }
                case NumericType.Vector:
                    {
                        VectorNumeric val = (VectorNumeric)value;

                        ScalarNumeric[] values = new ScalarNumeric[val.Count];

                        for (int i = 0; i < val.Count; i++)
                        {
                            values[i] = (ScalarNumeric)UnaryMinus(val[i]);
                        }

                        return new VectorNumeric(values);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_OPERATOR_NOT_SUPPORTING_OPERANDS);
            }
        }

        #endregion

        #region Functions

        private static BaseNumeric Sin(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Sin((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Sin((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Sin((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Cos(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Cos((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Cos((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Cos((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Tan(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Tan((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Tan((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Tan((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Ctg(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = 1 / System.Math.Tan((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = 1 / System.Math.Tan((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = 1 / System.Math.Tan((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric ArcSin(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Asin((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Asin((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Asin((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }

                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric ArcCos(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Acos((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Acos((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Acos((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric ArcTan(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Atan((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Atan((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Atan((double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric ArcCtg(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Atan(1 / (double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Atan(1 / (double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Atan(1 / (double)(argval.RealValue));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Abs(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        return new IntNumeric(System.Math.Abs(argval.Value));
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Abs((double)(argval.Value));
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        return new IntFractionNumeric(System.Math.Abs(argval.Numerator), System.Math.Abs(argval.Denominator));
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric argval = (ComplexNumeric)arg;

                        double result = System.Math.Sqrt(argval.RealValue * argval.RealValue + argval.ImValue * argval.ImValue);
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Round(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        return argval.Clone();
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result = System.Math.Round(argval.Value);

                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);

                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result = System.Math.Round(argval.RealValue);

                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Trunc(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        return argval.Clone();
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result;
                        if (argval.Value > 0)
                            result = System.Math.Floor(argval.Value);
                        else
                            result = System.Math.Ceiling(argval.Value);


                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result;
                        if (argval.RealValue > 0)
                            result = System.Math.Floor(argval.RealValue);
                        else
                            result = System.Math.Ceiling(argval.RealValue);

                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Frac(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        return new FloatNumeric(0.0);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        double result;
                        if (argval.Value > 0)
                        {
                            result = argval.Value - System.Math.Floor(argval.Value);
                        }
                        else
                        {
                            result = argval.Value - System.Math.Ceiling(argval.Value);
                        }


                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        double result;
                        if (argval.RealValue > 0)
                        {
                            result = argval.RealValue - System.Math.Floor(argval.RealValue);
                        }
                        else
                        {
                            result = argval.RealValue - System.Math.Ceiling(argval.RealValue);
                        }


                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Sqrt(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        if (argval.Value >= 0)
                        {
                            double result = System.Math.Sqrt((double)argval.Value);
                            if (Double.IsInfinity(result))
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                            return new FloatNumeric(result);
                        }
                        else
                            return RealPow((double)argval.Value, 0.5);
                    }
                case NumericType.Float:
                    {
                        FloatNumeric argval = (FloatNumeric)arg;

                        if (argval.Value >= 0.0)
                        {
                            double result = System.Math.Sqrt(argval.Value);
                            if (Double.IsInfinity(result))
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                            return new FloatNumeric(result);
                        }
                        else
                            return RealPow(argval.Value, 0.5);
                    }
                case NumericType.IntFraction:
                    {
                        IntFractionNumeric argval = (IntFractionNumeric)arg;

                        if (argval.RealValue >= 0.0)
                        {
                            double result = System.Math.Sqrt(argval.RealValue);
                            if (Double.IsInfinity(result))
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                            return new FloatNumeric(result);
                        }
                        else
                            return RealPow(argval.RealValue, 0.5);
                    }
                case NumericType.Complex:
                    {
                        ComplexNumeric argval = (ComplexNumeric)arg;

                        return ComplexPow(argval.RealValue, argval.ImValue, 0.5);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Sqr(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            return Multiply(arg, arg);
        }

        private static BaseNumeric Ln(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                case NumericType.Float:
                case NumericType.IntFraction:
                    {
                        IntNumeric argval = (IntNumeric)arg;

                        double result = System.Math.Log(argval.RealValue);
                        if (Double.IsInfinity(result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FLOATING_POINT_OVERFLOW);

                        return new FloatNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Length(List<BaseNumeric> args)
        {

            if (args.Count == 1)
            {
                BaseNumeric arg = args[0];

                switch (arg.NumericType)
                {
                    case NumericType.String:
                        {
                            StringNumeric argval = (StringNumeric)arg;
                            string argString = argval.Value;

                            return new IntNumeric(argString.Length);
                        }
                    case NumericType.Vector:
                        {
                            VectorNumeric argval = (VectorNumeric)arg;
                            return EvalVectorLength(argval);
                        }
                    default:
                        throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                }
            }
            else if (args.Count == 2)
            {
                BaseNumeric arg0 = args[0];
                BaseNumeric arg1 = args[1];

                if (arg0 is VectorNumeric p1 && arg1 is VectorNumeric p2)
                {
                    ValidateSpatialSizesEqal(p1, p2);

                    var diff = (VectorNumeric)Subtract(p2, p1);
                    return EvalVectorLength(diff);
                }
                else
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);
            }
            else
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);
        }

        private static BaseNumeric Copy(List<BaseNumeric> args)
        {
            if (args.Count != 3)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg0 = args[0];
            BaseNumeric arg1 = args[1];
            BaseNumeric arg2 = args[2];

            switch (arg0.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric arg0val = (StringNumeric)arg0;
                        string argString = arg0val.Value;

                        switch (arg1.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric arg1val = (IntNumeric)arg1;

                                    switch (arg2.NumericType)
                                    {
                                        case NumericType.Int:
                                            {
                                                IntNumeric arg2val = (IntNumeric)arg2;

                                                int start;
                                                if (arg1val.Value < 0)
                                                    start = 0;
                                                else
                                                    start = (int)arg1val.Value;

                                                int len;
                                                if (arg2val.Value < 0)
                                                    len = 0;
                                                else
                                                    len = (int)arg2val.Value;

                                                if (start >= argString.Length)
                                                    return new StringNumeric("");
                                                if (len == 0)
                                                    return new StringNumeric("");

                                                return new StringNumeric(argString.Substring(start, len));
                                            }
                                        default:
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                                    }
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Pos(List<BaseNumeric> args)
        {
            if (args.Count != 2)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg0 = args[0];
            BaseNumeric arg1 = args[1];

            switch (arg0.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric arg0val = (StringNumeric)arg0;
                        string argString = arg0val.Value;

                        switch (arg1.NumericType)
                        {
                            case NumericType.String:
                                {
                                    StringNumeric arg1val = (StringNumeric)arg1;
                                    string searchedString = arg1val.Value;

                                    var pos = argString.IndexOf(searchedString);
                                    return new IntNumeric(pos);
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Insert(List<BaseNumeric> args)
        {
            if (args.Count != 3)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg0 = args[0];
            BaseNumeric arg1 = args[1];
            BaseNumeric arg2 = args[2];

            switch (arg0.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric arg0val = (StringNumeric)arg0;
                        string insertedString = arg0val.Value;

                        switch (arg1.NumericType)
                        {
                            case NumericType.String:
                                {
                                    StringNumeric arg1val = (StringNumeric)arg1;
                                    string sourceString = arg1val.Value;

                                    switch (arg2.NumericType)
                                    {
                                        case NumericType.Int:
                                            {
                                                IntNumeric arg2val = (IntNumeric)arg2;

                                                int start;
                                                if (arg2val.Value < 0)
                                                    start = 0;
                                                else
                                                    start = (int)arg2val.Value;

                                                string result;

                                                if (start >= sourceString.Length)
                                                    result = sourceString + insertedString;
                                                else
                                                {
                                                    result = sourceString;
                                                    result = result.Insert(start, insertedString);
                                                }

                                                return new StringNumeric(result);
                                            }
                                        default:
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                                    }
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Delete(List<BaseNumeric> args)
        {
            if (args.Count != 3)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg0 = args[0];
            BaseNumeric arg1 = args[1];
            BaseNumeric arg2 = args[2];

            switch (arg0.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric arg0val = (StringNumeric)arg0;
                        string argString = arg0val.Value;

                        switch (arg1.NumericType)
                        {
                            case NumericType.Int:
                                {
                                    IntNumeric arg1val = (IntNumeric)arg1;

                                    switch (arg2.NumericType)
                                    {
                                        case NumericType.Int:
                                            {
                                                IntNumeric arg2val = (IntNumeric)arg2;

                                                int start;
                                                if (arg1val.Value < 0)
                                                    start = 0;
                                                else
                                                    start = (int)arg1val.Value;

                                                int len;
                                                if (arg2val.Value < 0)
                                                    len = 0;
                                                else
                                                    len = (int)arg2val.Value;

                                                if (start >= argString.Length)
                                                    return new StringNumeric(argString);
                                                if (len == 0)
                                                    return new StringNumeric(argString);

                                                string result = argString;
                                                result = result.Remove(start, len);

                                                return new StringNumeric(result);
                                            }
                                        default:
                                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                                    }
                                }
                            default:
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
                        }
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Uppercase(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric argval = (StringNumeric)arg;
                        string argString = argval.Value;

                        return new StringNumeric(argString.ToUpper());
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Lowercase(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric argval = (StringNumeric)arg;
                        string argString = argval.Value;

                        return new StringNumeric(argString.ToLower());
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric StrToInt(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric argval = (StringNumeric)arg;
                        string argString = argval.Value;

                        for (int i = 0, i_max = argString.Length; i < i_max; i++)
                            if (argString[i] < '0' || argString[i] > '9')
                                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_NUMBER_FORMAT);

                        int result = 0;
                        if (!int.TryParse(argString, out result))
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_NUMBER_FORMAT);

                        return new IntNumeric(result);
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric IntToStr(List<BaseNumeric> args)
        {
            if (args.Count != 1)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg = args[0];

            switch (arg.NumericType)
            {
                case NumericType.Int:
                    {
                        StringNumeric argval = (StringNumeric)arg;

                        return new StringNumeric(argval.Value.ToString());
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
        }

        private static BaseNumeric Dot(List<BaseNumeric> args)
        {
            if (args.Count != 2)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);

            BaseNumeric arg0 = args[0];
            BaseNumeric arg1 = args[1];

            if (arg0 is SpatialNumeric v1 && arg1 is SpatialNumeric v2)
            {
                return DotProduct(v1, v2);
            }
            else
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
        }

        private static BaseNumeric Distance(List<BaseNumeric> args)
        {
            if (args.Count == 2)
            {
                BaseNumeric arg0 = args[0];
                BaseNumeric arg1 = args[1];

                if (arg0 is VectorNumeric p1 && arg1 is VectorNumeric p2)
                {
                    ValidateSpatialSizesEqal(p1, p2);

                    var diff = (VectorNumeric)Subtract(p2, p1);
                    return EvalVectorLength(diff);
                }
                if (arg0 is VectorNumeric p12 && arg1 is VectorNumeric p22)
                {
                    var projection = (VectorNumeric)ProjectPointToLine(p12, p22);
                    var vector = (VectorNumeric)Subtract(p12, projection);
                    return EvalVectorLength(vector);
                }
                else
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
            else if (args.Count == 3)
            {
                BaseNumeric arg0 = args[0];
                BaseNumeric arg1 = args[1];
                BaseNumeric arg2 = args[2];

                if (arg0 is VectorNumeric point && arg1 is VectorNumeric lineStart && arg2 is VectorNumeric spanningVector)
                {
                    var projection = (VectorNumeric)ProjectPointToLine(point, lineStart, spanningVector);
                    var vector = (VectorNumeric)Subtract(point, projection);
                    return EvalVectorLength(vector);
                }
                else
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
            else
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);
        }

        private static BaseNumeric Project(List<BaseNumeric> args)
        {
            if (args.Count == 2)
            {
                BaseNumeric arg0 = args[0];
                BaseNumeric arg1 = args[1];

                if (arg0 is VectorNumeric point && arg1 is VectorNumeric vector)
                {
                    return ProjectPointToLine(point, vector);
                }
                else
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
            else if (args.Count == 3)
            {
                BaseNumeric arg0 = args[0];
                BaseNumeric arg1 = args[1];
                BaseNumeric arg2 = args[2];
                
                if (arg0 is VectorNumeric point1 && arg1 is VectorNumeric lineStart1 && arg2 is VectorNumeric lineEnd1)
                {
                    VectorNumeric spanningVector = (VectorNumeric)Subtract(lineEnd1, lineStart1);

                    return ProjectPointToLine(point1, lineStart1, spanningVector);
                }
                else if (arg0 is VectorNumeric point2 && arg1 is VectorNumeric lineStart2 && arg2 is VectorNumeric spanningVector2)
                {
                    return ProjectPointToLine(point2, lineStart2, spanningVector2);                        
                }
                else
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_FUNCTION_NOT_SUPPORTING_PARAMETERS);
            }
            else
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_PARAMETER_COUNT);
        }

        #endregion

        #region Indexer

        private static BaseNumeric Indexer(BaseNumeric value, List<BaseNumeric> args)
        {
            switch (value.NumericType)
            {
                case NumericType.String:
                    {
                        StringNumeric lval = (StringNumeric)value;

                        if (args.Count != 1)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_DIMENSIONS);
                        if (args[0].NumericType != NumericType.Int)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_TYPE);

                        long tmpIndex = ((IntNumeric)args[0]).Value;
                        int tmpIntIndex = (int)tmpIndex;

                        if (tmpIntIndex < 0 || (int)tmpIntIndex >= lval.Length)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_VALUE);

                        string tmpString = lval[tmpIntIndex].ToString();

                        return new StringNumeric(tmpString);
                    }
                case NumericType.List:
                    {
                        ListNumeric lval = (ListNumeric)value;

                        if (args.Count != 1)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_DIMENSIONS);
                        if (args[0].NumericType != NumericType.Int)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_TYPE);

                        long tmpIndex = ((IntNumeric)args[0]).Value;
                        int tmpIntIndex = (int)tmpIndex;

                        if (tmpIndex < 0 || tmpIndex >= lval.Count)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_VALUE);

                        return lval[tmpIntIndex].Clone();
                    }
                case NumericType.Matrix:
                    {
                        MatrixNumeric lval = (MatrixNumeric)value;

                        if (args.Count != 2)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_DIMENSIONS);
                        if (args[0].NumericType != NumericType.Int || args[1].NumericType != NumericType.Int)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_TYPE);

                        long tmpX = ((IntNumeric)args[0]).Value;
                        long tmpY = ((IntNumeric)args[1]).Value;

                        if (tmpX < 0 || tmpY < 0 || tmpX >= lval.Width || tmpY >= lval.Height)
                            throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INVALID_INDEX_VALUE);

                        int tmpIntX = (int)tmpX;
                        int tmpIntY = (int)tmpY;

                        return lval[tmpIntX, tmpIntY].Clone();
                    }
                default:
                    throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_INDEXER_NOT_SUPPORTING_OPERAND);
            }
        }

        #endregion

        #region Complex structure builders

        private static BaseNumeric BuildMatrix(int width, int height, BaseNumeric[] data)
        {
            if (width < 1 || height < 1 || data.Length != width * height)
                throw new CriticalException("Nieprawidłowe rozmiary konstruowanej macierzy!");

            return new MatrixNumeric(width, height, data);
        }

        private static BaseNumeric BuildList(int count, BaseNumeric[] data)
        {
            if (count < 1 || data.Length != count)
                throw new CriticalException("Nieprawidłowe rozmiary konstruowanej listy!");

            return new ListNumeric(data);
        }

        private static BaseNumeric BuildVector(int count, BaseNumeric[] data)
        {
            if (count < 2 || data.Length != count)
                throw new CriticalException("Invalid size of built vector!");

            if (data.Any(d => !(d is ScalarNumeric)))
                throw new SemanticException(Errors.PROCALC_ERROR_SEMANTIC_NON_SCALAR_IN_VECTOR_DEFINITION, -1, -1);

            return new VectorNumeric(data.Cast<ScalarNumeric>().ToArray());
        }

        #endregion

        // Internal methods ---------------------------------------------------

        internal static UnaryOperatorMethod GetUnaryOperatorMethod(string op)
        {
            unaryOperatorMappings.TryGetValue(op, out var method);
            return method;
        }

        internal static BinaryOperatorMethod GetBinaryOperatorMethod(string op)
        {
            binaryOperatorMappings.TryGetValue(op, out var method);
            return method;
        }

        internal static bool IdentifierIsConstant(string identifier)
        {
            return identifierMappings.Keys.Contains(identifier.ToLower());
        }

        internal static BaseNumeric GetConstValue(string identifier)
        {
            identifierMappings.TryGetValue(identifier.ToLower(), out var value);
            return value;            
        }

        internal static bool FunctionIsBuiltin(string fn)
        {
            return functionMappings.Keys.Contains(fn.ToLower());
        }

        internal static FunctionMethod GetFunctionMethod(string fn)
        {
            functionMappings.TryGetValue(fn.ToLower(), out var result);
            return result;
        }

        internal static IndexerMethod GetIndexerMethod()
        {
            return Indexer;
        }

        internal static BuildMatrixMethod GetBuildMatrixMethod()
        {
            return BuildMatrix;
        }

        internal static BuildSequenceMethod GetBuildListMethod()
        {
            return BuildList;
        }

        internal static BuildSequenceMethod GetBuildVectorMethod()
        {
            return BuildVector;
        }
    }
}
