﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Arithmetic
{
    class FractionComparison
    {
        static bool FractionMoreThan(long LNum, long LDenom, long RNum, long RDenom)
        {
            // Tą operację można z powodzeniem przeprowadzić na liczbach rzeczywistych

            if (LDenom == 0 || RDenom == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            return LNum / LDenom > RNum / RDenom;
        }

        static bool FractionLessThan(long LNum, long LDenom, long RNum, long RDenom)
        {
            // Tą operację można z powodzeniem przeprowadzić na liczbach rzeczywistych

            if (LDenom == 0 || RDenom == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            return LNum / LDenom < RNum / RDenom;
        }

        static bool FractionMoreThanOrEqual(long LNum, long LDenom, long RNum, long RDenom)
        {
            // Sprowadzamy do wspólnego mianownika
            long tmpGCD = MathTools.GCD(LDenom, RDenom);

            return (LNum * (RDenom / tmpGCD)) >= (RNum * (LDenom / tmpGCD));
        }

        static bool FractionLessThanOrEqual(long LNum, long LDenom, long RNum, long RDenom)
        {
            // Sprowadzamy do wspólnego mianownika
            long tmpGCD = MathTools.GCD(LDenom, RDenom);

            return (LNum * (RDenom / tmpGCD)) <= (RNum * (LDenom / tmpGCD));
        }

        static bool FractionEqual(long LNum, long LDenom, long RNum, long RDenom)
        {
            // Sprowadzamy do wspólnego mianownika
            long tmpGCD = MathTools.GCD(LDenom, RDenom);

            return (LNum * (RDenom / tmpGCD)) == (RNum * (LDenom / tmpGCD));
        }

        static bool FractionInequal(long LNum, long LDenom, long RNum, long RDenom)
        {
            // Sprowadzamy do wspólnego mianownika
            long tmpGCD = MathTools.GCD(LDenom, RDenom);

            return (LNum * (RDenom / tmpGCD)) != (RNum * (LDenom / tmpGCD));
        }
    }
}
