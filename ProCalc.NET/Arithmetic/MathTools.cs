﻿using ProCalc.NET.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Arithmetic
{
    static class MathTools
    {
        public static long GCD(long a, long b)
        {
            if (a == 0 || b == 0)
                return 1;

            long c;

            while (b != 0)
            {
                c = a % b;
                a = b;
                b = c;
            }

            return a;
        }

        public static void ReduceFraction(ref long num, ref long denom)
        {
            long tmpGCD = GCD(num, denom);

            if (tmpGCD != 1)
            {
                num /= tmpGCD;
                denom /= tmpGCD;
            }
        }

    }
}
