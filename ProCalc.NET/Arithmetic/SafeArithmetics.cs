﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Arithmetic
{
    class SafeArithmetics
    {
        internal static bool SafeUnsignedAdd(uint a, uint b, out uint result)
        {
            if (a <= uint.MaxValue - b)
            {
                result = a + b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal static bool SafeUnsignedAdd(ulong a, ulong b, out ulong result)
        {
            if (a <= ulong.MaxValue - b)
            {
                result = a + b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal bool SafeUnsignedSubtract(uint a, uint b, out uint result)
        {
            if (b <= a)
            {
                result = a - b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal static bool SafeUnsignedSubtract(ulong a, ulong b, out ulong result)
        {
            if (b <= a)
            {
                result = a - b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal static bool SafeUnsignedMultiply(uint a, uint b, out uint result)
        {
            if (b == 0 || a <= uint.MaxValue / b)
            {
                result = a * b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal static bool SafeUnsignedMultiply(ulong a, ulong b, out ulong result)
        {
            if (b == 0 || a <= ulong.MaxValue / b)
            {
                result = a * b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal static bool SafeUnsignedDivide(uint a, uint b, out uint result)
        {
            if (b == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            result = a / b;
            return true;
        }

        internal static bool SafeUnsignedDivide(ulong a, ulong b, out ulong result)
        {
            if (b == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            result = a / b;
            return true;
        }

        internal static bool SafeSignedAdd(int a, int b, out int result)
        {
            if (a > 0 && b > 0)
            {
                if (a <= int.MaxValue - b)
                {
                    result = a + b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else if (a < 0 && b < 0)
            {
                if (a >= int.MinValue - b)
                {
                    result = a + b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else
            {
                result = a + b;
                return true;
            }
        }

        internal static bool SafeSignedAdd(long a, long b, out long result)
        {
            if (a > 0 && b > 0)
            {
                if (a <= long.MaxValue - b)
                {
                    result = a + b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else if (a < 0 && b < 0)
            {
                if (a >= long.MinValue - b)
                {
                    result = a + b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else
            {
                result = a + b;
                return true;
            }
        }

        internal static bool SafeSignedSubtract(int a, int b, out int result)
        {
            if (a >= 0 && b < 0)
            {
                if (a <= int.MaxValue + b)
                {
                    result = a - b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else if (a < 0 && b >= 0)
            {
                if (a >= int.MinValue + b)
                {
                    result = a - b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else
            {
                result = a - b;
                return true;
            }
        }

        internal static bool SafeSignedSubtract(long a, long b, out long result)
        {
            if (a >= 0 && b < 0)
            {
                if (a <= long.MaxValue + b)
                {
                    result = a - b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else if (a < 0 && b >= 0)
            {
                if (a >= long.MinValue + b)
                {
                    result = a - b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else
            {
                result = a - b;
                return true;
            }
        }

        internal static bool SafeSignedMultiply(int a, int b, out int result)
        {
            if (a == 0 || b == 0)
            {
                result = 0;
                return true;
            }
            else if (a > 0 && b > 0)
            {
                if (a <= int.MaxValue / b)
                {
                    result = a * b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else if (a < 0 && b < 0)
            {
                if (a >= int.MaxValue / b)
                {
                    result = a * b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else
            {
                int smaller = a < b ? a : b;
                int greater = a >= b ? a : b;

                // If smaller is equal to -1, multiplication can always be made
                // because abs(int.MinValue) > abs(int.MaxValue). However
                // the next test crashes, so we need an additional check.
                if (smaller == -1 || greater <= int.MinValue / smaller)
                {
                    result = a * b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
        }

        internal static bool SafeSignedMultiply(long a, long b, out long result)
        {
            if (a == 0 || b == 0)
            {
                result = 0;
                return true;
            }
            else if (a > 0 && b > 0)
            {
                if (a <= long.MaxValue / b)
                {
                    result = a * b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else if (a < 0 && b < 0)
            {
                if (a >= long.MaxValue / b)
                {
                    result = a * b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
            else
            {
                long smaller = a < b ? a : b;
                long greater = a >= b ? a : b;

                // If smaller is equal to -1, multiplication can always be made
                // because abs(long.MinValue) > abs(long.MaxValue). However
                // the next test crashes, so we need an additional check.
                if (smaller == -1 || greater <= long.MinValue / smaller)
                {
                    result = a * b;
                    return true;
                }
                else
                {
                    result = 0;
                    return false;
                }
            }
        }

        internal static bool SafeSignedDivide(int a, int b, out int result)
        {
            if (b == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            if (a != int.MinValue || b != -1)
            {
                result = a / b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }

        internal static bool SafeSignedDivide(long a, long b, out long result)
        {
            if (b == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            if (a != long.MinValue || b != -1)
            {
                result = a / b;
                return true;
            }
            else
            {
                result = 0;
                return false;
            }
        }
    }
}
