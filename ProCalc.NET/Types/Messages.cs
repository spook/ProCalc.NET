﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Types
{
    public enum Messages
    {
        PROCALC_MESSAGE_FUNCTION_ADDED = 1,
        PROCALC_MESSAGE_VARIABLE_ADDED = 2
    }
}
