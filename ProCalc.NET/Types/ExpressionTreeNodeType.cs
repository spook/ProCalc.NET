﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Types
{
    enum ExpressionTreeNodeType
    {
        Numeric = 0,
        Identifier = 1,
        Parameter = 2,
        CallParam = 3,
        Variable = 4,
        ExternalVariable = 5,
        UnaryOperator = 6,
        BinaryOperator = 7,
        AssignmentOperator = 8,
        IndexerOperator = 9,
        BuiltinFunction = 10,
        UserFunction = 11,
        Matrix = 12,
        List = 13,
        Point = 14,
        Vector = 15,
        Constant = 16
    }
}
