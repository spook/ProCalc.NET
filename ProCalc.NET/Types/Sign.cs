﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Types
{
    public enum Sign
    {
        sPositive,
        sNegative
    }

    public static class SignExtensions
    {
        public static Sign Negate(this Sign sign)
        {
            if (sign == Sign.sNegative)
                return Sign.sPositive;
            else
                return Sign.sNegative;
        }
    }
}
