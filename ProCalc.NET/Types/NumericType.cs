﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Types
{
    public enum NumericType
    {
        Int = 0,
        Float = 1,
        IntFraction = 2,
        Complex = 3,
        Matrix = 4,
        List = 5,
        String = 6,
        Bool = 7,
        Message = 8,
        LargeNumber = 9,
        Vector = 10
    }
}
