﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Common
{
    public static class Converter
    {
        public static string ToDms(double val)
        {
            string result;

            if (val < 0)
            {
                result = "-";
                val = System.Math.Abs(val);
            }
            else
            {
                result = "";
            }

            // Degrees
            result += ((long)val).ToString("00");

            // Minutes
            val = (val - System.Math.Truncate(val)) * 60;
            result = result + ":" + ((long)val).ToString("00");

            // Seconds
            val = (val - System.Math.Truncate(val)) * 60;
            result = result + ":" + ((long)val).ToString("00");

            // Hundredths of second
            val = (val - System.Math.Truncate(val)) * 100;
            result = result + "." + ((long)val).ToString("00");

            return result;
        }
    }
}
