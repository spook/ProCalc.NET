﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProCalc.NET.Common
{
    class CommonTools
    {
        private static readonly Regex identifierRegex = new Regex("^[A-Za-z_@][A-Za-z0-9\\._@]*$");

        internal static bool CheckIdentifier(string id)
        {
            return identifierRegex.IsMatch(id);
        }

        internal static bool CheckVariableIdentifier(string id)
        {
            return identifierRegex.IsMatch(id);
        }

        internal static bool CheckConstantIdentifier(string id)
        {
            string upCaseId = id.ToUpper();

            return upCaseId == "PI" ||
                   upCaseId == "E" ||
                   upCaseId == "TRUE" ||
                   upCaseId == "FALSE";
        }
    }
}
