﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Resolvers
{
    public class ResolvedCallParam : BaseResolvedIdentifier
    {
        private int callParamIndex;

        public ResolvedCallParam(int callParamIndex)
        {
            this.callParamIndex = callParamIndex;
        }

        public override IdentifierType GetIdentifierType()
        {
            return IdentifierType.itCallParam;
        }

        public int GetCallParamIndex()
        {
            return callParamIndex;
        }
    };
}
