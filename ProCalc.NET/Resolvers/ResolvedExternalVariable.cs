﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Resolvers
{
    public class ResolvedExternalVariable : BaseResolvedIdentifier
    {
        private string externalVariableName;

        public ResolvedExternalVariable(string externalVariableName)
        {
            this.externalVariableName = externalVariableName;
        }

        public override IdentifierType GetIdentifierType()
        {
            return IdentifierType.itExternalVariable;
        }

        public string GetExternalVariableName()
        {
            return externalVariableName;
        }
    };
}
