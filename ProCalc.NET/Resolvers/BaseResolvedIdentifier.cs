﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Resolvers
{
    public abstract class BaseResolvedIdentifier
    {
        internal BaseResolvedIdentifier()
        {

        }

        public abstract IdentifierType GetIdentifierType();
    };
}
