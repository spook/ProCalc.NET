﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Resolvers
{
    public class ResolvedVariable : BaseResolvedIdentifier
    {
        private string variableName;

        public ResolvedVariable(string variableName)
        {
            this.variableName = variableName;
        }

        public override IdentifierType GetIdentifierType()
        {
            return IdentifierType.itVariable;
        }

        public string GetVariableName()
        {
            return variableName;
        }
    };
}
