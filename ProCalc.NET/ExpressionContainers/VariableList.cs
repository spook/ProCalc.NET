﻿using ProCalc.NET.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.ExpressionContainers
{
    class VariableList
    {
        private List<Variable> varList = new List<Variable>();
        private List<Variable> pendingList = new List<Variable>();

        public VariableList()
        {

        }

        public void AddVariable(Variable newVariable)
        {
            if (newVariable == null)
                throw new CriticalException("Attempt to add empty variable!");

            string newName = newVariable.GetName().ToUpper();

            int i = 0;
            while (i < varList.Count && varList[i].GetName().ToUpper() != newName)
                i++;

            if (i < varList.Count)
                varList[i] = newVariable;
            else
                varList.Add(newVariable);
        }

        public void DeleteVariable(int index)
        {
            if (index < 0 || index >= varList.Count)
                throw new CriticalException("Invalid index of deleted variable!");

            varList.RemoveAt(index);
        }

        public void DeleteVariable(string varName)
        {
            varName = varName.ToUpper();

            int i = 0;
            while (i < varList.Count && varList[i].GetName().ToUpper() != varName)
                i++;

            if (i < varList.Count)
                varList.RemoveAt(i);
        }

        public void Clear()
        {
            varList.Clear();
            pendingList.Clear();
        }

        public int GetVariableCount()
        {
            return varList.Count;
        }

        public Variable GetVariable(int index)
        {
            if (index < 0 || index >= varList.Count)
                throw new CriticalException("Invalid index of deleted variable!");

            return varList[index];
        }

        public Variable GetVariable(string varName)
        {
            varName = varName.ToUpper();

            int i = 0;
            while (i < varList.Count && varList[i].GetName().ToUpper() != varName)
                i++;

            if (i < varList.Count)
                return varList[i];
            else
                return null;
        }

        public void AddPending(Variable newVariable)
        {
            if (newVariable == null)
                throw new CriticalException("Attempt to add empty variable to pending list!");

            pendingList.Add(newVariable);
        }

        public void AcceptPending()
        {
            for (int i = 0; i < pendingList.Count; i++)
                AddVariable(pendingList[i]);

            pendingList.Clear();
        }

        public void RejectPending()
        {
            pendingList.Clear();
        }

        public void Save(BinaryWriter writer)
        {
            if (pendingList.Count > 0)
                throw new CriticalException("Pending list is not empty, cannot save!");

            int len = varList.Count;
            writer.Write(len);

            for (int i = 0; i < varList.Count; i++)
                varList[i].Save(writer);
        }

        public void Load(BinaryReader reader)
        {
            Clear();

            int len = reader.ReadInt32();

            try
            {
                for (int i = 0; i < len; i++)
                {
                    Variable variable = new Variable(reader);
                    AddPending(variable);
                }

                AcceptPending();
            }
            catch
            {
                RejectPending();

                throw;
            }
        }
    }
}
