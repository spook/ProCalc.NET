﻿using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Numerics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.ExpressionContainers
{
    class Variable
    {
        private string varName;
        private BaseNumeric numeric;

        public Variable(string newVarName, BaseNumeric newNumeric)
        {
            if (!CommonTools.CheckVariableIdentifier(newVarName))
                throw new CriticalException("Invalid added variable name!");
            if (newNumeric == null)
                throw new CriticalException("Attempt to assign empty numeric object to variable!");

            varName = newVarName;
            numeric = newNumeric;
        }

        public Variable(BinaryReader reader)
        {
            varName = "";
            numeric = null;

            varName = reader.ReadString();
            numeric = BaseNumeric.Create(reader);            
        }

        public void Save(BinaryWriter writer)
        {
            if (numeric == null)
                throw new CriticalException("Corrupted variable container!");

            writer.Write(varName);
            numeric.Save(writer);
        }

        public string GetName()
        {
            return varName;
        }

        public BaseNumeric GetNumeric()
        {
            return numeric;
        }
    }
}
