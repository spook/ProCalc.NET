﻿using ProCalc.NET.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.ExpressionContainers
{
    class FunctionList
    {
        private List<Function> fnList = new List<Function>();
        private List<Function> pendingList = new List<Function>();

        public FunctionList()
        {
        }

        public void AddFunction(Function newFunction)
        {
            if (newFunction == null)
                throw new CriticalException("Attempt to add empty function to list!");

            string newName = newFunction.GetName().ToUpper();

            // TODO Zamienić na Dictionary
            int i = 0;
            while (i < fnList.Count && fnList[i].GetName().ToUpper() != newName)
                i++;

            if (i < fnList.Count)
                fnList[i] = newFunction;
            else
                fnList.Add(newFunction);
        }

        public void DeleteFunction(int index)
        {
            if (index < 0 || index >= fnList.Count)
                throw new CriticalException("Invalid index of deleted function!");

            fnList.RemoveAt(index);
        }

        public void DeleteFunction(string fnName)
        {
            fnName = fnName.ToUpper();

            int i = 0;
            while (i < fnList.Count && fnList[i].GetName().ToUpper() != fnName)
                i++;

            if (i < fnList.Count)
                fnList.RemoveAt(i);
        }

        public void Clear()
        {
            fnList.Clear();
            pendingList.Clear();
        }

        public int GetFunctionCount()
        {
            return fnList.Count;
        }

        public Function GetFunction(int index)
        {
            if (index < 0 || index >= fnList.Count)
                throw new CriticalException("Invalid index of retrieved function!");

            return fnList[index];
        }

        public Function GetFunction(string fnName)
        {
            fnName = fnName.ToUpper();

            int i = 0;
            while (i < fnList.Count && fnList[i].GetName().ToUpper() != fnName)
                i++;

            if (i < fnList.Count)
                return fnList[i];
            else
                return null;
        }

        public void AddPending(Function newFunction)
        {
            if (newFunction == null)
                throw new CriticalException("Attempt to add empty function to pending list!");

            pendingList.Add(newFunction);
        }

        public void AcceptPending()
        {
            for (int i = 0; i < pendingList.Count; i++)
            {
                AddFunction(pendingList[i]);
            }

            pendingList.Clear();
        }

        public void RejectPending()
        {
            pendingList.Clear();
        }

        public void Save(BinaryWriter writer)
        {
            if (pendingList.Count > 0)
                throw new CriticalException("Pending list is not empty, cannot save!");

            int len = fnList.Count;
            writer.Write(len);

            for (int i = 0; i < fnList.Count; i++)
            {
                fnList[i].Save(writer);
            }
        }

        public void Load(BinaryReader reader)
        {
            Clear();

            int len = reader.ReadInt32();

            try
            {
                for (int i = 0; i < len; i++)
                {
                    Function fn = new Function(reader);
                    AddPending(fn);
                }

                AcceptPending();
            }
            catch
            {
                RejectPending();

                throw;
            }
        }
    }
}
