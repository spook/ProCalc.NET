﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.ExpressionContainers
{
    class Expression
    {
        private ExpressionTree tree;

        private bool InternalCheckControlExpression(BaseExpressionTreeItem node)
        {
            var assignmentOperator = node as AssignmentOperatorExpressionTreeNode;

            if (assignmentOperator != null)
                return true;

            var unaryOperator = node as UnaryOperatorExpressionTreeNode;
            if (unaryOperator != null)
            {
                if (InternalCheckControlExpression(unaryOperator.Operand))
                    return true;
                else
                    return false;
            }

            var binaryOperator = node as BinaryOperatorExpressionTreeNode;
            if (binaryOperator != null)
            {
                if (InternalCheckControlExpression(binaryOperator.Left))
                    return true;
                else if (InternalCheckControlExpression(binaryOperator.Right))
                    return true;
                else return false;
            }

            var paramExpression = node as BaseParamExpressionTreeNode;
            if (paramExpression != null)
            {
                int i = 0;
                while (i < paramExpression.ParamCount)
                {
                    if (InternalCheckControlExpression(paramExpression.GetParam(i)))
                        return true;

                    i++;
                }

                return false;
            }

            return false;
        }

        private bool InternalUsesVariable(BaseExpressionTreeItem node, string name)
        {
            var variable = node as VariableExpressionTreeLeaf;
            if (variable != null)
            {
                if (variable.VariableName.ToUpper() == name.ToUpper())
                    return true;
                else
                    return false;
            }

            var unaryOperator = node as UnaryOperatorExpressionTreeNode;
            if (unaryOperator != null)
            {
                if (InternalUsesVariable(unaryOperator.Operand, name))
                    return true;
                else
                    return false;
            }

            var binaryOperator = node as BinaryOperatorExpressionTreeNode;
            if (binaryOperator != null)
            {
                if (InternalUsesVariable(binaryOperator.Left, name))
                    return true;
                else if (InternalUsesVariable(binaryOperator.Right, name))
                    return true;
                else return false;
            }

            var paramExpression = node as BaseParamExpressionTreeNode;
            if (paramExpression != null)
            {
                int i = 0;
                while (i < paramExpression.ParamCount)
                {
                    if (InternalUsesVariable(paramExpression.GetParam(i), name))
                        return true;

                    i++;
                }

                return false;
            }

            return false;
        }

        public Expression(ExpressionTree newTree)
        {
            if (newTree == null)
                throw new CriticalException("Attempt to create expression with empty tree!");
            if (!newTree.GetVerified())
                throw new CriticalException("Attempt to create expression with non-verified tree!");

            tree = newTree;
        }

        public ExpressionTree GetExpressionTree()
        {
            return tree;
        }

        public bool IsControlExpression()
        {
            return InternalCheckControlExpression(tree.GetRoot());
        }

        public bool UsesVariable(string varName)
        {
            return InternalUsesVariable(tree.GetRoot(), varName);
        }
    }
}
