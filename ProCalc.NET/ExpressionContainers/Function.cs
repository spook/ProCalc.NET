﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Expressions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.ExpressionContainers
{
    class Function
    {
        private string fnName;
        private int fnParamCount;
        private ExpressionTree tree;

        public Function(string newName, int newFnParamCount, ExpressionTree newTree)
        {
            if (Arithmetics.FunctionIsBuiltin(newName))
                throw new CriticalException("Invalid function identifier!");
            if (newFnParamCount < 1)
                throw new CriticalException("Invalid number of parameters of added function!");
            if (newTree == null)
                throw new CriticalException("Invalid expression tree!");
            if (!newTree.GetVerified())
                throw new CriticalException("Expression tree is not verified!");

            fnName = newName;
            fnParamCount = newFnParamCount;
            tree = newTree;
        }

        public Function(BinaryReader reader)
        {
            fnName = reader.ReadString();
            fnParamCount = reader.ReadInt32();
            tree = new ExpressionTree(reader);            
        }

        public void Save(BinaryWriter writer)
        {
            if (tree == null)
                throw new CriticalException("Corrupted function container!");

            writer.Write(fnName);
            writer.Write(fnParamCount);
            tree.Save(writer);
        }

        public string GetName()
        {
            return fnName;
        }

        public int GetParamCount()
        {
            return fnParamCount;
        }

        public ExpressionTree GetExpressionTree()
        {
            return tree;
        }
    }
}
