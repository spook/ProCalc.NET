﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Exceptions
{
    public class EngineException : ProCalcException
    {
        public EngineException(string message)
            : base(message)
        {

        }
    }
}
