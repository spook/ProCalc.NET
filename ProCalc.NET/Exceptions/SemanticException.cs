﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProCalc.NET.Types;

namespace ProCalc.NET.Exceptions
{
    public class SemanticException : ExpressionException
    {
        public SemanticException(Errors errorCode, int line, int column)
            : base(errorCode, line, column)
        {

        }
    }
}
