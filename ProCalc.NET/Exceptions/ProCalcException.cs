﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Exceptions
{
    public class ProCalcException : Exception
    {
        public ProCalcException()
            : base()
        {

        }

        public ProCalcException(string message)
            : base(message)
        {

        }
    }
}
