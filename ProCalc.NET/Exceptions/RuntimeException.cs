﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProCalc.NET.Types;

namespace ProCalc.NET.Exceptions
{
    public class RuntimeException : ExpressionException
    {
        public RuntimeException(Errors errorCode)
            : base(errorCode, 0, 0)
        {

        }

        public RuntimeException(Errors errorCode, int line, int column)
            : base(errorCode, line, column)
        {

        }
    }
}
