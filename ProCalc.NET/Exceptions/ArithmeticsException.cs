﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProCalc.NET.Types;

namespace ProCalc.NET.Exceptions
{
    public class ArithmeticsException : InternalException
    {
        public ArithmeticsException(Errors errorCode)
            : base(errorCode)
        {

        }
    }
}
