﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProCalc.NET.Types;

namespace ProCalc.NET.Exceptions
{
    public class ExpressionException : UserException
    {
        public ExpressionException(Errors errorCode, int line, int column)
            : base(errorCode)
        {
            this.Line = line;
            this.Column = column;
        }

        public int Line { get; }
        public int Column { get; }
    }
}
