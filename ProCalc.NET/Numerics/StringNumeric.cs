﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("StringNumeric {AsString}")]
    public class StringNumeric : BaseNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly string value;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(value);
            writer.Write(bytes.Length);
            writer.Write(bytes);
        }

        // Internal methods ---------------------------------------------------

        internal StringNumeric(BinaryReader reader)
        {
            int byteCount = reader.ReadInt32();
            if (byteCount < 0)
                throw new CriticalException("Invalid byte count!");

            byte[] bytes = new byte[byteCount];
            reader.Read(bytes, 0, byteCount);

            value = Encoding.ASCII.GetString(bytes);
        }

        // Public methods -----------------------------------------------------

        public StringNumeric(string value)
        {
            this.value = value;
        }

        public override BaseNumeric Clone()
        {
            return new StringNumeric(value);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("\"");
                for (int i = 0; i < value.Length; i++)
                {
                    if (value[i] != '"')
                        sb.Append(value[i]);
                    else
                        sb.Append("\"\"");
                }

                sb.Append("\"");

                return sb.ToString();
            }
        }

        public int Length => value.Length;

        public override NumericType NumericType => NumericType.String;

        public char this[int index] => value[index];

        public string Value => value;
    }
}
