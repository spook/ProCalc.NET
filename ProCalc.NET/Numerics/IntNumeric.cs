﻿using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("IntNumeric {AsString}")]
    public class IntNumeric : FloatValueNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly long value;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(value);
        }

        // Internal methods ---------------------------------------------------

        internal IntNumeric(BinaryReader reader)
        {
            value = reader.ReadInt64();
        }

        // Public methods -----------------------------------------------------

        public IntNumeric(long value)
        {
            this.value = value;
        }

        public override BaseNumeric Clone()
        {
            return new IntNumeric(value);
        }

        // Public properties --------------------------------------------------

        public long Value => value;

        public override string AsBin
        {
            get
            {
                if (value < 0)
                    return "-0b" + Convert.ToString(value, 2);
                else
                    return "0b" + Convert.ToString(value, 2);
            }
        }

        public override string AsDec
        {
            get
            {
                if (value < 0)
                    return "-0d" + Convert.ToString(value);
                else
                    return "0d" + Convert.ToString(value);
            }
        }

        public override string AsDMS => value.ToString("00") + ":00:00.000";

        public override string AsHex
        {
            get
            {
                if (value < 0)
                    return "-0h" + Convert.ToString(value, 16);
                else
                    return "0h" + Convert.ToString(value, 16);
            }
        }

        public override string AsIntFraction => value.ToString() + "/1";

        public override string AsOct
        {
            get
            {
                if (value < 0)
                    return "-0o" + Convert.ToString(value, 8);
                else
                    return "0o" + Convert.ToString(value, 8);
            }
        }

        public override string AsString => value.ToString();

        public override NumericType NumericType => NumericType.Int;

        public override double RealValue => (double)value;
    }
}
