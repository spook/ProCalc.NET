﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    public abstract class SpatialNumeric : BaseNumeric
    {
        public abstract int Count { get; }
        public abstract ScalarNumeric this[int index] { get; }
    }
}
