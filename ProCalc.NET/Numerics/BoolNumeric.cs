﻿using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("BoolNumeric {AsString}")]
    public class BoolNumeric : BaseNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly bool value;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(value);
        }

        // Internal methods ---------------------------------------------------

        internal BoolNumeric(BinaryReader reader)
        {
            value = reader.ReadBoolean();
        }

        // Public methods -----------------------------------------------------

        public BoolNumeric(bool value)
        {
            this.value = value;
        }

        public BoolNumeric(BoolNumeric source)
        {
            this.value = source.Value;
        }

        public override BaseNumeric Clone()
        {
            return new BoolNumeric(value);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString => value.ToString();

        public override NumericType NumericType => NumericType.Bool;

        public bool Value => value;
    }
}
