﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Math;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("LargeNumberNumeric {AsString}")]
    public class LargeNumberNumeric : BaseNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly BigNum value;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            if (value.GetSign() == Sign.sPositive)
                writer.Write(1);
            else
                writer.Write(-1);

            writer.Write(value.GetMinExponent());
            writer.Write(value.GetMaxExponent());

            for (int i = value.GetMinExponent(); i <= value.GetMaxExponent(); i++)
                writer.Write(value.GetDigit(i));
        }

        // Internal methods ---------------------------------------------------

        internal LargeNumberNumeric(BinaryReader reader)
        {
            value = new BigNum();

            int sign = reader.ReadInt32();

            if (sign > 0)
                value.SetSign(Sign.sPositive);
            else if (sign < 0)
                value.SetSign(Sign.sNegative);
            else
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            int minExponent = reader.ReadInt32();
            int maxExponent = reader.ReadInt32();

            if (maxExponent < minExponent)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            for (int i = minExponent; i <= maxExponent; i++)
            {
                byte digit = reader.ReadByte();
                value.SetDigit(i, digit);
            }
        }

        // Public methods -----------------------------------------------------

        public LargeNumberNumeric(BigNum newValue)
        {
            value = newValue;
        }

        public override BaseNumeric Clone()
        {
            return new LargeNumberNumeric(value);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString => value.ToString() + "L";

        public override NumericType NumericType => NumericType.LargeNumber;

        public BigNum Value => value.Clone();
    }
}
