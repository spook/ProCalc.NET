﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("MatrixNumeric {AsString}")]
    public class MatrixNumeric : BaseNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly BaseNumeric[] data;
        private readonly int height;
        private readonly int width;
        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(width);
            writer.Write(height);

            for (int i = 0; i < data.Length; i++)
            {
                data[i].Save(writer);
            }
        }

        // Internal methods ---------------------------------------------------

        internal MatrixNumeric(BinaryReader reader)
        {
            width = reader.ReadInt32();
            height = reader.ReadInt32();

            if (width < 1 || height < 1)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            data = new BaseNumeric[width * height];
            for (int i = 0; i < data.Length; i++)
            {
                BaseNumeric numeric = BaseNumeric.Create(reader);
                data[i] = numeric;
            }
        }

        // Public methods -----------------------------------------------------

        public MatrixNumeric(int width, int height, BaseNumeric[] data)
        {
            if (width < 1 || height < 1)
                throw new CriticalException("Invalid matrix size!");

            if (data.Length < width * height)
                throw new CriticalException("Invalid size of data!");

            if (data.Any(i => i == null))
                throw new CriticalException("Null entry in data!");

            this.width = width;
            this.height = height;
            this.data = data.ToArray();
        }

        public override BaseNumeric Clone()
        {
            return new MatrixNumeric(width, height, data);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString
        {
            get
            {
                return "[" + String.Join("; ", Enumerable.Range(0, height)
                    .Select(x => data.Skip(width * x).Take(width))
                    .Select(r => string.Join(", ", r.Select(x => x.AsString)))) + "]";
            }
        }

        public int Height => height;

        public override NumericType NumericType => NumericType.Matrix;

        public BaseNumeric this[int index]
        {
            get
            {
                if (index < 0 || index > width * height)
                    throw new CriticalException($"Invalid coordinate: {index}!");

                return data[index];
            }
        }

        public BaseNumeric this[int col, int row]
        {
            get
            {
                if (col < 0 || col >= width || row < 0 || row >= height)
                    throw new CriticalException($"Invalid coordinates: {col};{row}!");

                return data[row * width + col];
            }
        }

        public int Width => width;
    }
}
