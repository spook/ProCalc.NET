﻿using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("IntFractionNumeric {AsString}")]
    public class IntFractionNumeric : FloatValueNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly long denominator;
        private readonly long numerator;
        // Private methods ----------------------------------------------------

        private long GCD(long a, long b)
        {
            long c;

            while (b != 0)
            {
                c = a % b;
                a = b;
                b = c;
            }

            return a;
        }

        private void Shorten(ref long numerator, ref long denominator)
        {
            long tempGCD = GCD(numerator, denominator);
            if (tempGCD > 1)
            {
                numerator /= tempGCD;
                denominator /= tempGCD;
            }

            if (denominator < 0)
            {
                numerator = -numerator;
                denominator = -denominator;
            }
        }

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(numerator);
            writer.Write(denominator);
        }

        // Internal methods ---------------------------------------------------

        internal IntFractionNumeric(BinaryReader reader)
        {
            numerator = reader.ReadInt64();
            denominator = reader.ReadInt64();

            if (denominator == 0)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            Shorten(ref numerator, ref denominator);
        }

        // Public methods -----------------------------------------------------

        public IntFractionNumeric(long numerator, long denominator)
        {
            if (denominator == 0)
                throw new ArithmeticsException(Errors.PROCALC_ERROR_ARITHMETICS_DIVISION_BY_ZERO);

            this.numerator = numerator;
            this.denominator = denominator;

            Shorten(ref this.numerator, ref this.denominator);
        }

        public override BaseNumeric Clone()
        {
            return new IntFractionNumeric(numerator, denominator);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => Converter.ToDms((double)numerator / (double)denominator);

        public override string AsHex => "N/A";

        public override string AsIntFraction => numerator.ToString() + "/" + denominator.ToString();

        public override string AsOct => "N/A";

        public override string AsString => ((double)numerator / (double)denominator).ToString();

        public long Denominator => denominator;

        public long Numerator => numerator;

        public override NumericType NumericType => NumericType.IntFraction;

        public override double RealValue => (double)numerator / (double)denominator;
    }
}
