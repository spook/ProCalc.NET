﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("VectorNumeric {AsString}")]
    public class VectorNumeric : SpatialNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly ScalarNumeric[] data;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(data.Length);

            for (int i = 0; i < data.Length; i++)
            {
                data[i].Save(writer);
            }
        }

        // Internal methods ---------------------------------------------------

        internal VectorNumeric(BinaryReader reader)
        {
            int count = reader.ReadInt32();

            if (count < 1)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            data = new ScalarNumeric[count];
            for (int i = 0; i < count; i++)
            {
                ScalarNumeric numeric = (ScalarNumeric)BaseNumeric.Create(reader);
                data[i] = numeric;
            }
        }

        // Public methods -----------------------------------------------------

        public VectorNumeric(ScalarNumeric[] data)
        {
            if (data == null)
                throw new CriticalException("Vector data is invalid");

            if (data.Length < 2)
                throw new CriticalException("Invalid vector dimensions!");

            if (data.Any(d => d == null))
                throw new CriticalException("Data contains null entries!");

            this.data = data.ToArray();
        }

        public override BaseNumeric Clone()
        {
            return new VectorNumeric(data);
        }

        // Public properties --------------------------------------------------
        
        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString => $"({string.Join(", ", data.Select(d => d.AsString))})";

        public override int Count => data.Length;

        public override NumericType NumericType => NumericType.Vector;

        public override ScalarNumeric this[int index] => data[index];
    }
}
