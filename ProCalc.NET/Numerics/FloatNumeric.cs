﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProCalc.NET.Common;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("FloatNumeric {AsString}")]
    public class FloatNumeric : FloatValueNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly double value;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(value);
        }

        // Internal methods ---------------------------------------------------

        internal FloatNumeric(BinaryReader reader)
        {
            value = reader.ReadDouble();

            if (!ValidateFloatValue(value))
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);
        }

        // Public methods -----------------------------------------------------

        public FloatNumeric(double value)
        {
            if (!ValidateFloatValue(value))
                throw new CriticalException("Floating point value shall be finite!");

            this.value = value;
        }

        public override BaseNumeric Clone()
        {
            return new FloatNumeric(value);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => Converter.ToDms(value);

        public override string AsHex => "N/A";
        
        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString => value.ToString();

        public override NumericType NumericType => NumericType.Float;

        public override double RealValue => value;

        public double Value => value;
    }
}
