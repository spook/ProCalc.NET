﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("ListNumeric {AsString}")]
    public class ListNumeric : BaseNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly BaseNumeric[] data;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(data.Length);

            for (int i = 0; i < data.Length; i++)
            {
                data[i].Save(writer);
            }
        }

        // Internal methods ---------------------------------------------------

        internal ListNumeric(BinaryReader reader)
        {
            int count = reader.ReadInt32();

            if (count < 1)
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);

            data = new BaseNumeric[count];
            for (int i = 0; i < count; i++)
            {
                BaseNumeric numeric = BaseNumeric.Create(reader);
                data[i] = numeric;
            }
        }

        // Public methods -----------------------------------------------------

        public ListNumeric(BaseNumeric[] data)
        {
            if (data == null)
                throw new CriticalException("List data is invalid");

            if (data.Length < 1)
                throw new CriticalException("Invalid list dimensions!");

            if (data.Any(d => d == null))
                throw new CriticalException("Data contains null entries!");

            this.data = data.ToArray();
        }

        // Public methods -----------------------------------------------------

        public override BaseNumeric Clone()
        {
            return new ListNumeric(data);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";
        
        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString
        {
            get
            {
                return $"{{{string.Join(", ", data.Select(d => d.AsString))}}}";
            }
        }

        public int Count => data.Length;

        public override NumericType NumericType => NumericType.List;

        public BaseNumeric this[int index]
        {
            get
            {
                if (index < 0 || index >= data.Length)
                    throw new CriticalException("Invalid index");

                return data[index];
            }
        }
    }
}
