﻿using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("MessageNumeric {AsString}")]
    public class MessageNumeric : BaseNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly Messages messageCode;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write((int)messageCode);
        }

        // Internal methods ---------------------------------------------------

        internal MessageNumeric(BinaryReader reader)
        {
            messageCode = (Messages)reader.ReadInt32();
        }

        // Public methods -----------------------------------------------------

        public MessageNumeric(Messages messageCode)
        {
            this.messageCode = messageCode;
        }

        public MessageNumeric(MessageNumeric source)
        {
            messageCode = source.MessageCode;
        }

        public override BaseNumeric Clone()
        {
            return new MessageNumeric(messageCode);
        }

        // Public properties --------------------------------------------------

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString => "N/A";

        public Messages MessageCode => messageCode;

        public override NumericType NumericType => NumericType.Message;
    }
}
