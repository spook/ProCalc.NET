﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    [DebuggerDisplay("ComplexNumeric {AsString}")]
    public class ComplexNumeric : ScalarNumeric
    {
        // Private fields -----------------------------------------------------

        private readonly double imPart;
        private readonly double realPart;

        // Protected methods --------------------------------------------------

        protected override void InternalSave(BinaryWriter writer)
        {
            writer.Write(realPart);
            writer.Write(imPart);
        }

        // Internal methods ---------------------------------------------------

        internal ComplexNumeric(BinaryReader reader)
        {
            realPart = reader.ReadDouble();
            imPart = reader.ReadDouble();

            if (!ValidateFloatValue(realPart) || !ValidateFloatValue(imPart))
                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CORRUPTED_STREAM);            
        }

        // Public methods -----------------------------------------------------

        public ComplexNumeric(double realPart, double imPart)
        {
            if (!ValidateFloatValue(realPart) || !ValidateFloatValue(imPart))
                throw new CriticalException("Complex number parts shall be finite!");

            this.realPart = realPart;
            this.imPart = imPart;
        }

        // Public methods ----------------------------------------------------

        public override BaseNumeric Clone()
        {
            return new ComplexNumeric(realPart, imPart);
        }

        public override string AsBin => "N/A";

        public override string AsDec => "N/A";

        public override string AsDMS => "N/A";

        public override string AsHex => "N/A";

        public override string AsIntFraction => "N/A";

        public override string AsOct => "N/A";

        public override string AsString
        {
            get
            {
                if (imPart > 0.0)
                    return realPart.ToString() + "+" + imPart.ToString() + "i";
                else
                    return realPart.ToString() + imPart.ToString() + "i";
            }
        }

        public double ImValue => imPart;

        public override NumericType NumericType => NumericType.Complex;

        public double RealValue => realPart;
    }
}
