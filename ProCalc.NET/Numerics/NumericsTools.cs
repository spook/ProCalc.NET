﻿using ProCalc.NET.Arithmetic;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Tokenizers;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    public class NumericsTools
    {
        private static DmsTokenizer tokenizer = new DmsTokenizer();

        internal static bool StrToDms(string number, ref BaseNumeric value)
        {
            long degrees;
            long minutes;
            long seconds;
            double secondsDec;
            bool secondsDecPresent;

            tokenizer.Reset();

            // *** Stopnie ***

            degrees = 0;

            int position = 0;
            DmsToken token = tokenizer.Process(number, ref position);
            if (token.TokenType != DmsTokenType.Degrees)
                throw new ConvertException(Errors.PROCALC_ERROR_CONVERT_CANNOT_CONVERT_STRING_TO_DMS);

            if (!long.TryParse(token.TokenStr, out degrees))
            {
                // Wartość spoza zakresu
                value = null;
                return false;
            }

            // *** Minuty ***

            minutes = 0;

            if (position < number.Length)
            {
                // Dwukropek
                token = tokenizer.Process(number, ref position);
                if (token.TokenType != DmsTokenType.Mincolon)
                    throw new ConvertException(Errors.PROCALC_ERROR_CONVERT_CANNOT_CONVERT_STRING_TO_DMS);

                // Wartość minut
                token = tokenizer.Process(number, ref position);
                if (token.TokenType != DmsTokenType.Minutes)
                    throw new ConvertException(Errors.PROCALC_ERROR_CONVERT_CANNOT_CONVERT_STRING_TO_DMS);

                // Minuty mają ścisłą postać dwucyfrowej liczby, nie da się przekroczyć zakresu

                minutes = long.Parse(token.TokenStr);
            }

            // *** Sekundy ***

            seconds = 0;
            secondsDec = 0.0;
            secondsDecPresent = false;

            if (position < number.Length)
            {
                // Dwukropek
                token = tokenizer.Process(number, ref position);
                if (token.TokenType != DmsTokenType.Seccolon)
                    throw new ConvertException(Errors.PROCALC_ERROR_CONVERT_CANNOT_CONVERT_STRING_TO_DMS);

                // Wartość sekund
                token = tokenizer.Process(number, ref position);
                if (token.TokenType != DmsTokenType.Seconds)
                    throw new ConvertException(Errors.PROCALC_ERROR_CONVERT_CANNOT_CONVERT_STRING_TO_DMS);

                int periodPos;
                if ((periodPos = token.TokenStr.IndexOf('.')) != -1)
			    {
                    string intPart = token.TokenStr.Substring(0, periodPos);

                    seconds = long.Parse(intPart);

                    string fracPart = "0" + token.TokenStr.Substring(periodPos, token.TokenStr.Length - periodPos);

                    secondsDec = double.Parse(fracPart, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture);

                    secondsDecPresent = true;
                }
			else
			{
                    seconds = long.Parse(token.TokenStr);

                    secondsDec = 0.0;
                    secondsDecPresent = false;
                }
            }

            if (position != number.Length)
                throw new ConvertException(Errors.PROCALC_ERROR_CONVERT_CANNOT_CONVERT_STRING_TO_DMS);

            if (secondsDecPresent)
            {
                // Część ułamkowa wymusza float
                double result = degrees + minutes / 60.0 + seconds / 3600.0 + secondsDec / 3600.0;

                value = new FloatNumeric(result);
                return true;
            }
            else
            {
                if (SafeArithmetics.SafeSignedMultiply(degrees, 3600, out long numNormalized) &&
                    SafeArithmetics.SafeSignedMultiply(minutes, 60, out long minNormalized) &&
                    SafeArithmetics.SafeSignedAdd(numNormalized, minNormalized, out long tmpNumerator) &&
                    SafeArithmetics.SafeSignedAdd(tmpNumerator, seconds, out long numerator))
                {
                    value = new IntFractionNumeric(numerator, 3600);
                    return true;
                }
                else
                {
                    double result = degrees + minutes / 60.0 + seconds / 3600.0 + secondsDec / 3600.0;

                    value = new FloatNumeric(result);
                    return true;
                }
            }
        }
    }
}
