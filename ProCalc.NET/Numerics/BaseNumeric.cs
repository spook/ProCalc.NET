﻿using ProCalc.NET.Exceptions;
using ProCalc.NET.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Numerics
{
    public abstract class BaseNumeric
    {
        // Protected methods --------------------------------------------------

        protected BaseNumeric()
        {

        }

        protected abstract void InternalSave(BinaryWriter writer);

        protected bool ValidateFloatValue(double value)
        {
            return !double.IsInfinity(value) && !double.IsNaN(value);
        }

        // Public methods -----------------------------------------------------

        public abstract BaseNumeric Clone();

        public static BaseNumeric Create(BinaryReader reader)
        {
            NumericType numericType = (NumericType)reader.ReadInt32();

            switch (numericType)
            {
                case NumericType.Int:
                    return new IntNumeric(reader);
                case NumericType.Float:
                    return new FloatNumeric(reader);
                case NumericType.IntFraction:
                    return new IntFractionNumeric(reader);
                case NumericType.Complex:
                    return new ComplexNumeric(reader);
                case NumericType.Matrix:
                    return new MatrixNumeric(reader);
                case NumericType.List:
                    return new ListNumeric(reader);
                case NumericType.String:
                    return new StringNumeric(reader);
                case NumericType.Bool:
                    return new BoolNumeric(reader);
                case NumericType.Message:
                    return new MessageNumeric(reader);
                case NumericType.LargeNumber:
                    return new LargeNumberNumeric(reader);
                case NumericType.Vector:
                    return new VectorNumeric(reader);
                default:
                    throw new CriticalException("Numeric type not supported!");
            }
        }

        public void Save(BinaryWriter writer)
        {
            var numericType = NumericType;
            writer.Write((int)numericType);

            InternalSave(writer);
        }

        // Public properties --------------------------------------------------

        public abstract string AsBin { get; }

        public abstract string AsDec { get; }

        public abstract string AsDMS { get; }

        public abstract string AsHex { get; }

        public abstract string AsIntFraction { get; }

        public abstract string AsOct { get; }

        public abstract string AsString { get; }

        public bool IsRealNumeric => NumericType == NumericType.Int || NumericType == NumericType.Float || NumericType == NumericType.IntFraction;

        public abstract NumericType NumericType { get; }
    }
}
