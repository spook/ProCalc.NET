﻿using ProCalc.NET.Expressions;
using ProCalc.NET.ExpressionContainers;
using ProCalc.NET.Numerics;
using ProCalc.NET.Resolvers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ProCalc.NET.Exceptions;
using ProCalc.NET.Arithmetic;
using ProCalc.NET.Common;
using ProCalc.NET.Grammar;
using ProCalc.NET.Types;

namespace ProCalc.NET
{
    public class ProCalcCore
    {
        private FunctionList fnList = new FunctionList();
        private VariableList varList = new VariableList();

        private ExpressionGrammar grammar;
        private Irony.Parsing.Parser parser;

        private BaseNumeric InternalExecute(BaseExpressionTreeItem item,
            List<BaseNumeric> callParams,
            List<BaseNumeric> @params,
            List<string> callStack,
            bool allowControlStatements,
            BaseExternalVariableResolver externalVariableResolver)
        {
            if (item == null)
                throw new CriticalException("Cannot evaluate empty element!");

            switch (item.ExpressionTreeNodeType)
            {
                case ExpressionTreeNodeType.Numeric:
                    {
                        NumericExpressionTreeLeaf numericLeaf = (NumericExpressionTreeLeaf)item;

                        return numericLeaf.Numeric.Clone();
                    }

                case ExpressionTreeNodeType.Identifier:
                    {
                        throw new CriticalException("Attempt to evaluate identifier (it seems, that identifier resolver failed)");
                    }

                case ExpressionTreeNodeType.Parameter:
                    {
                        ParameterExpressionTreeLeaf paramLeaf = (ParameterExpressionTreeLeaf)item;

                        if (@params == null)
                            throw new CriticalException("Attempt to evaluate parameter in InternalExecute function called for expression (with no call params passed)!");

                        if (paramLeaf.GetParamId() >= @params.Count)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_NOT_ENOUGH_CALL_PARAMETERS_PASSED, paramLeaf.Line, paramLeaf.Column);

                        return @params[paramLeaf.GetParamId()].Clone();
                    }

                case ExpressionTreeNodeType.CallParam:
                    {
                        CallParamExpressionTreeLeaf callParamLeaf = (CallParamExpressionTreeLeaf)item;

                        if (callParams == null)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_NO_CALL_PARAMETERS_PASSED, callParamLeaf.Line, callParamLeaf.Column);

                        if (callParamLeaf.ParamId >= callParams.Count)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_NOT_ENOUGH_CALL_PARAMETERS_PASSED, callParamLeaf.Line, callParamLeaf.Column);

                        return callParams[callParamLeaf.ParamId].Clone();
                    }

                case ExpressionTreeNodeType.Variable:
                    {
                        VariableExpressionTreeLeaf varLeaf = (VariableExpressionTreeLeaf)item;

                        // Próbujemy znaleźć definicję zmiennej
                        Variable var = varList.GetVariable(varLeaf.VariableName);

                        if (var == null)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_VARIABLE_NOT_FOUND, varLeaf.Line, varLeaf.Column);

                        return (var.GetNumeric().Clone());
                    }
                case ExpressionTreeNodeType.ExternalVariable:
                    {
                        ExternalVariableExpressionTreeLeaf extVarLeaf = (ExternalVariableExpressionTreeLeaf)item;

                        if (externalVariableResolver == null)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_NO_EXTERNAL_RESOLVER_SUPPLIED, extVarLeaf.Line, extVarLeaf.Column);

                        BaseNumeric externalVariableValue = externalVariableResolver.ResolveVariable(extVarLeaf.ExtVarName);

                        if (externalVariableValue == null)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_EXTERNAL_VARIABLE_NOT_FOUND, extVarLeaf.Line, extVarLeaf.Column);

                        return externalVariableValue.Clone();
                    }
                case ExpressionTreeNodeType.Constant:
                    {
                        ConstantExpressionTreeLeaf constLeaf = (ConstantExpressionTreeLeaf)item;

                        BaseNumeric constValue = Arithmetics.GetConstValue(constLeaf.ConstName);

                        if (constValue == null)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_INVALID_CONSTANT, constLeaf.Line, constLeaf.Column);

                        return constValue;
                    }
                case ExpressionTreeNodeType.UnaryOperator:
                    {
                        UnaryOperatorExpressionTreeNode unaryOperatorNode = (UnaryOperatorExpressionTreeNode)item;

                        BaseNumeric operand = null;
                        BaseNumeric result = null;

                        operand = InternalExecute(unaryOperatorNode.Operand, callParams, @params, callStack, allowControlStatements, externalVariableResolver);

                        try
                        {
                            result = (unaryOperatorNode.GetOpMethod())(operand);
                        }
                        catch (ArithmeticsException err)
                        {
                            throw new RuntimeException(err.ErrorCode, unaryOperatorNode.Line, unaryOperatorNode.Column);
                        }

                        return result;
                    }

                case ExpressionTreeNodeType.BinaryOperator:
                    {
                        BinaryOperatorExpressionTreeNode binaryOperatorNode = (BinaryOperatorExpressionTreeNode)item;

                        BaseNumeric left = null;
                        BaseNumeric right = null;
                        BaseNumeric result = null;

                        left = InternalExecute(binaryOperatorNode.Left, callParams, @params, callStack, allowControlStatements, externalVariableResolver);
                        right = InternalExecute(binaryOperatorNode.Right, callParams, @params, callStack, allowControlStatements, externalVariableResolver);

                        try
                        {
                            result = binaryOperatorNode.OpMethod(left, right);
                        }
                        catch (ArithmeticsException err)
                        {
                            throw new RuntimeException(err.ErrorCode, binaryOperatorNode.Line, binaryOperatorNode.Column);
                        }

                        return result;
                    }

                case ExpressionTreeNodeType.AssignmentOperator:
                    {
                        AssignmentOperatorExpressionTreeNode assignmentNode = (AssignmentOperatorExpressionTreeNode)item;

                        if (!allowControlStatements)
                            throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_CONTROL_STATEMENTS_NOT_ALLOWED, item.Line, item.Column);

                        if (assignmentNode.Left.ExpressionTreeNodeType == ExpressionTreeNodeType.Identifier)
                        {
                            IdentifierExpressionTreeLeaf identifierLeaf = (IdentifierExpressionTreeLeaf)(assignmentNode.Left);

                            // Dodajemy zmienną

                            BaseNumeric varValue = null;
                            Variable var = null;

                            varValue = InternalExecute(assignmentNode.Right, callParams, @params, callStack, allowControlStatements, externalVariableResolver);

                            var = new Variable(identifierLeaf.IdentifierName, varValue);
                            varValue = null;

                            varList.AddPending(var);

                            return new MessageNumeric(Types.Messages.PROCALC_MESSAGE_VARIABLE_ADDED);
                        }
                        else if (assignmentNode.Left.ExpressionTreeNodeType == ExpressionTreeNodeType.UserFunction)
                        {
                            UserFunctionExpressionTreeNode userFunctionNode = (UserFunctionExpressionTreeNode)(assignmentNode.Left);

                            // Dodajemy funkcję

                            Function fn = null;
                            BaseExpressionTreeItem treeRoot = null;
                            ExpressionTree fnTree = null;

                            treeRoot = (assignmentNode.Right.Clone());
                            treeRoot.Verify();

                            fnTree = new ExpressionTree(treeRoot);

                            fn = new Function(userFunctionNode.FunctionName,
                                userFunctionNode.                                ParamCount,
                                fnTree);

                            fnList.AddPending(fn);

                            return new MessageNumeric(Types.Messages.PROCALC_MESSAGE_FUNCTION_ADDED);
                        }
                        else
                            throw new CriticalException("Invalid structure of assignment node!");
                    }

                case ExpressionTreeNodeType.IndexerOperator:
                    {
                        IndexerOperatorExpressionTreeNode indexerNode = (IndexerOperatorExpressionTreeNode)item;

                        List<BaseNumeric> indexerParams = new List<BaseNumeric>();
                        BaseNumeric operand = null;
                        BaseNumeric result = null;

                        operand = InternalExecute(indexerNode.Operand, callParams, @params, callStack, allowControlStatements, externalVariableResolver);

                        for (int i = 0, i_max = indexerNode.ParamCount; i < i_max; i++)
                            indexerParams.Add(InternalExecute(indexerNode.GetParam(i), callParams, @params, callStack, allowControlStatements, externalVariableResolver));

                        try
                        {
                            result = indexerNode.GetIndexerMethod()(operand, indexerParams);
                        }
                        catch (ArithmeticsException err)
                        {
                            throw new RuntimeException(err.ErrorCode, indexerNode.Line, indexerNode.Column);
                        }

                        return result;
                    }

                case ExpressionTreeNodeType.BuiltinFunction:
                    {
                        BuiltinFunctionExpressionTreeNode builtinFunctionNode = (BuiltinFunctionExpressionTreeNode)item;

                        List<BaseNumeric> functionParams = new List<BaseNumeric>();
                        BaseNumeric result = null;

                        for (int i = 0, i_max = builtinFunctionNode.ParamCount; i < i_max; i++)
                            functionParams.Add(InternalExecute(builtinFunctionNode.GetParam(i), callParams, @params, callStack, allowControlStatements, externalVariableResolver));

                        try
                        {
                            result = builtinFunctionNode.FnMethod(functionParams);
                        }
                        catch (ArithmeticsException err)
                        {
                            throw new RuntimeException(err.ErrorCode, builtinFunctionNode.Line, builtinFunctionNode.Column);
                        }

                        return result;
                    }

                case ExpressionTreeNodeType.UserFunction:
                    {
                        UserFunctionExpressionTreeNode userFunctionNode = (UserFunctionExpressionTreeNode)item;

                        List<BaseNumeric> functionParams = new List<BaseNumeric>();
                        BaseNumeric result = null;

                        try
                        {
                            for (int i = 0, i_max = userFunctionNode.ParamCount; i < i_max; i++)
                                functionParams.Add(InternalExecute(userFunctionNode.GetParam(i), callParams, @params, callStack, allowControlStatements, externalVariableResolver));

                            Function function = fnList.GetFunction(userFunctionNode.FunctionName);

                            if (function == null)
                                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_FUNCTION_NOT_FOUND, userFunctionNode.Line, userFunctionNode.Column);

                            if (function.GetParamCount() != functionParams.Count)
                                throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_INVALID_PARAMETER_COUNT, userFunctionNode.Line, userFunctionNode.Column);

                            string lowerFnName = userFunctionNode.FunctionName.ToLower();
                            for (int i = 0, i_max = callStack.Count; i < i_max; i++)
                                if (callStack[i].ToLower() == lowerFnName)
                                    throw new RuntimeException(Errors.PROCALC_ERROR_RUNTIME_RECURRENCY_NOT_ALLOWED, userFunctionNode.Line, userFunctionNode.Column);

                            // TODO CallStack -> stack
                            callStack.Add(userFunctionNode.FunctionName);
                            result = InternalExecute(function.GetExpressionTree().GetRoot(), callParams, functionParams, callStack, allowControlStatements, externalVariableResolver);
                            callStack.RemoveAt(callStack.Count - 1);
                        }
                        catch (RuntimeException err)
                        {
                            // Błąd poleciał wewnątrz funkcji użytkownika, więc nie da się stwierdzić
                            // pozycji w wyrażeniu. Dlatego kod wyjątku jest przepisywany i zwracana jest pozycja
                            // wywołania funkcji użytkownika.
                            throw new RuntimeException(err.ErrorCode, userFunctionNode.Line, userFunctionNode.Column);
                        }

                        return result;
                    }

                case ExpressionTreeNodeType.Matrix:
                    {
                        MatrixExpressionTreeNode matrixNode = (MatrixExpressionTreeNode)item;

                        List<BaseNumeric> matrixParams = new List<BaseNumeric>();
                        BaseNumeric result = null;

                        for (int i = 0, i_max = matrixNode.ParamCount; i < i_max; i++)
                            matrixParams.Add(InternalExecute(matrixNode.GetParam(i), callParams, @params, callStack, allowControlStatements, externalVariableResolver));

                        try
                        {
                            result = matrixNode.MtMethod(matrixNode.Width, matrixNode.Height, matrixParams.ToArray());
                        }
                        catch (ArithmeticsException err)
                        {
                            throw new RuntimeException(err.ErrorCode, matrixNode.Line, matrixNode.Column);
                        }

                        return result;
                    }

                case ExpressionTreeNodeType.List:
                case ExpressionTreeNodeType.Point:
                case ExpressionTreeNodeType.Vector:
                    {
                        BaseSequenceExpressionTreeNode listNode = (BaseSequenceExpressionTreeNode)item;

                        List<BaseNumeric> listParams = new List<BaseNumeric>();
                        BaseNumeric result = null;

                        for (int i = 0, i_max = listNode.ParamCount; i < i_max; i++)
                            listParams.Add(InternalExecute(listNode.GetParam(i), callParams, @params, callStack, allowControlStatements, externalVariableResolver));

                        try
                        {
                            result = listNode.BuildMethod(listNode.Count, listParams.ToArray());
                        }
                        catch (ArithmeticsException err)
                        {
                            throw new RuntimeException(err.ErrorCode, listNode.Line, listNode.Column);
                        }

                        return result;
                    }
                default:
                    {
                        throw new CriticalException("Invalid element in expression tree!");
                    }
            }
        }

        public ProCalcCore()
        {
            grammar = new ExpressionGrammar();
            parser = new Irony.Parsing.Parser(grammar);
        }

        public CompiledExpression Compile(string expression,
           BaseExternalIdentifierResolver externalIdentifierResolver = null)
        {
            ExpressionTree expressionTree = null;
            Expression result = null;

            var parseTree = parser.Parse(expression);
            if (parseTree.Status == Irony.Parsing.ParseTreeStatus.Error)
            {
                if (parseTree.HasErrors())
                {
                    var message = parseTree.ParserMessages.FirstOrDefault(p => p.Level == Irony.ErrorLevel.Error);
                    throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, message?.Location.Line ?? -1, message?.Location.Column ?? -1);
                }
                else
                {
                    throw new SyntaxException(Errors.PROCALC_ERROR_SYNTAX_INVALID_EXPRESSION, -1, -1);
                }
            }

            expressionTree = new ExpressionTree(parseTree, externalIdentifierResolver);

            result = new Expression(expressionTree);

            return new CompiledExpression(result);
        }

        public BaseNumeric Execute(CompiledExpression expression,
           List<BaseNumeric> callParams = null,
            bool allowControlStatements = true,
            BaseExternalVariableResolver externalVariableResolver = null)
        {
            BaseNumeric result = null;

            try
            {
                // TODO convert to stack
                List<string> callStack = new List<string>();
                result = InternalExecute(expression.Expression.GetExpressionTree().GetRoot(), callParams, null, callStack, allowControlStatements, externalVariableResolver);

                fnList.AcceptPending();
                varList.AcceptPending();
            }
            catch
            {
                fnList.RejectPending();
                varList.RejectPending();

                throw;

            }

            return result;
        }

        public void SetVariableValue(string variableName, BaseNumeric value)
        {
            if (!CommonTools.CheckVariableIdentifier(variableName))
                throw new EngineException("Invalid variable name!");

            Variable var = new Variable(variableName, value);

            varList.AddVariable(var);
        }

        public BaseNumeric GetVariableValue(string variableName)
        {
            Variable variable = varList.GetVariable(variableName);

            if (variable != null)
                return variable.GetNumeric();
            else
                return null;
        }

        public void SaveState(BinaryWriter writer)
        {
            fnList.Save(writer);
            varList.Save(writer);
        }

        public void LoadState(BinaryReader reader)
        {
            try
            {
                fnList.Load(reader);
                varList.Load(reader);
            }
            catch
            {
                fnList.Clear();
                varList.Clear();
                throw;
            }
        }

        public void ClearState()
        {
            fnList.Clear();
            varList.Clear();
        }
    }
}
