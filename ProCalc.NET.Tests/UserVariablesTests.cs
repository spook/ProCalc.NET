﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProCalc.NET.Numerics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Tests
{
    [TestClass]
    public class UserVariablesTests
    {
        private ProCalcCore core;

        [TestInitialize]
        public void Initialize()
        {
            core = new ProCalcCore();
        }

        [TestMethod]
        public void UserVariableAssignmentTest()
        {
            // Arrange

            var compiled = core.Compile("a=2");
            var result = core.Execute(compiled);

            // Act

            compiled = core.Compile("a");
            result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(((IntNumeric)result).Value, 2);
        }
    }
}
