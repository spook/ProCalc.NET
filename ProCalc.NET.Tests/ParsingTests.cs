﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProCalc.NET.Numerics;

namespace ProCalc.NET.Tests
{
    [TestClass]
    public class ParsingTests
    {
        private ProCalcCore core;

        [TestInitialize]
        public void Initialize()
        {
            core = new ProCalcCore();
        }

        [TestMethod]
        public void BinaryParseTest()
        {
            // Act

            var compiled = core.Compile("0b111");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(7, ((IntNumeric)result).Value);
        }

        [TestMethod]
        public void ConstantParseTest()
        {
            // Act

            var compiled = core.Compile("pi");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(3.1415, ((FloatNumeric)result).Value, 0.0001);
        }

        [TestMethod]
        public void DMSParseTest1()
        {
            // Act

            var compiled = core.Compile("12:30");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntFractionNumeric));
            Assert.AreEqual(25, ((IntFractionNumeric)result).Numerator);
            Assert.AreEqual(2, ((IntFractionNumeric)result).Denominator);
        }

        [TestMethod]
        public void DMSParseTest2()
        {
            // Act

            var compiled = core.Compile("12:30:45");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntFractionNumeric));
            Assert.AreEqual(1001, ((IntFractionNumeric)result).Numerator);
            Assert.AreEqual(80, ((IntFractionNumeric)result).Denominator);
        }

        [TestMethod]
        public void DMSParseTest3()
        {
            // Act

            var compiled = core.Compile("12:30:45.12");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(12.51253333333, ((FloatNumeric)result).Value, 0.0000001);
        }

        [TestMethod]
        public void ExtendedIdentifierNotationTest()
        {
            // Act

            var compiled = core.Compile("a.b = 5");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(MessageNumeric));
        }

        [TestMethod]
        public void HexParseTest()
        {
            // Act

            var compiled = core.Compile("0hff");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(255, ((IntNumeric)result).Value);
        }

        [TestMethod]
        public void OctalParseTest()
        {
            // Act

            var compiled = core.Compile("0o10");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(8, ((IntNumeric)result).Value);
        }

        [TestMethod]
        public void VectorParseTest()
        {
            // Act

            var compiled = core.Compile("(1,2,3)");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));
            var point = (VectorNumeric)result;

            Assert.AreEqual(3, point.Count);
            Assert.AreEqual(1, ((IntNumeric)point[0]).Value);
            Assert.AreEqual(2, ((IntNumeric)point[1]).Value);
            Assert.AreEqual(3, ((IntNumeric)point[2]).Value);
        }

        [TestMethod]
        public void ListParseTest()
        {
            // Act

            var compiled = core.Compile("{1, 2, 3}");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(ListNumeric));
            var point = (ListNumeric)result;

            Assert.AreEqual(3, point.Count);
            Assert.AreEqual(1, ((IntNumeric)point[0]).Value);
            Assert.AreEqual(2, ((IntNumeric)point[1]).Value);
            Assert.AreEqual(3, ((IntNumeric)point[2]).Value);
        }
    }
}
