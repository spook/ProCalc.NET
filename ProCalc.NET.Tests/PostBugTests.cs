﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Tests
{
    [TestClass]
    public class PostBugTests
    {
        private ProCalcCore core;

        [TestInitialize]
        public void Initialize()
        {
            core = new ProCalcCore();
        }

        [TestMethod]
        public void BigNumSubtractTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("256L^16/7500000000L");

            try
            {
                var result = core.Execute(compiled);
            }
            catch
            {
                Assert.Fail("Exception thrown");
            }

            // Assert
        }

        [TestMethod]
        public void SubtractionTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("-1*1");

            try
            {
                var result = core.Execute(compiled);
            }
            catch
            {
                Assert.Fail("Exception thrown");
            }
        }
    }
}
