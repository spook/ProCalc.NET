﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProCalc.NET.Numerics;

namespace ProCalc.NET.Tests
{
    [TestClass]
    public class BuiltinFunctionsTests
    {
        private ProCalcCore core;

        [TestInitialize]
        public void Initialize()
        {
            core = new ProCalcCore();
        }

        [TestMethod]
        public void SqrtTest()
        {
            // Act

            var compiled = core.Compile("sqrt(2)");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(1.4142, ((FloatNumeric)result).Value, 0.0001);
        }
    }
}
