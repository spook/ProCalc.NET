﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProCalc.NET.Numerics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProCalc.NET.Tests
{
    [TestClass]
    public class UserFunctionsTests
    {
        private ProCalcCore core;

        [TestInitialize]
        public void Initialize()
        {
            core = new ProCalcCore();
        }

        [TestMethod]
        public void FunctionAssignmentTest()
        {
            // Arrange

            var compiled = core.Compile("f(x)=x+2");
            var result = core.Execute(compiled);

            // Act

            compiled = core.Compile("f(2)");
            result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(((IntNumeric)result).Value, 4);
        }
    }
}
