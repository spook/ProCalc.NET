﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProCalc.NET.Numerics;
using ProCalc.NET.Types;

namespace ProCalc.NET.Tests
{
    [TestClass]
    public class ArithmeticTests
    {
        private const double EPSILON = 0.00000001;

        private ProCalcCore core;

        [TestInitialize]
        public void Initialize()
        {
            core = new ProCalcCore();
        }

        [TestMethod]
        public void IntAddIntTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("2+2");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(((IntNumeric)result).Value, 4);
        }

        [TestMethod]
        public void IntAddFloatTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("2+2.0");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(((FloatNumeric)result).Value, 4.0, EPSILON);
        }

        [TestMethod]
        public void IntAddIntFractionTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("2+1/2");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntFractionNumeric));
            Assert.AreEqual(((IntFractionNumeric)result).Numerator, 5);
            Assert.AreEqual(((IntFractionNumeric)result).Denominator, 2);
        }

        [TestMethod]
        public void IntAddComplexTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("2+3i");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(ComplexNumeric));
            Assert.AreEqual(((ComplexNumeric)result).RealValue, 2.0, EPSILON);
            Assert.AreEqual(((ComplexNumeric)result).ImValue, 3.0, EPSILON);
        }

        [TestMethod]
        public void IntAddDmsTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("2+1:30:00");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntFractionNumeric));
            Assert.AreEqual(((IntFractionNumeric)result).Numerator, 7);
            Assert.AreEqual(((IntFractionNumeric)result).Denominator, 2);
        }

        [TestMethod]
        public void IntAddLargeNumberTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("2+2L");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(LargeNumberNumeric));
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(0), 4);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetMinExponent(), 0);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetMaxExponent(), 0);
        }

        [TestMethod]
        public void IntDivLargeNumberTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("1/2L");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(LargeNumberNumeric));
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(0), 0);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(-1), 5);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetMinExponent(), -1);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetMaxExponent(), 0);
        }

        [TestMethod]
        public void IntDivLargeNumberTest2()
        {
            // Arrange

            // Act

            var compiled = core.Compile("1/7L");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(LargeNumberNumeric));
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(0), 0);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(-1), 1);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(-2), 4);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(-3), 2);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(-4), 8);
        }

        [TestMethod]
        public void MatrixMultiplyMatrixTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("[1,2;3,4]*[1;2]");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(MatrixNumeric));
        }

        [TestMethod]
        public void LargeNumberDivideLargeNumberTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("4L/2L");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(LargeNumberNumeric));
            Assert.AreEqual("2L", ((LargeNumberNumeric)result).AsString);
        }

        [TestMethod]
        public void LargeNumberSubtractLargeNumberTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("12L-20L");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(LargeNumberNumeric));
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetDigit(0), 8);
            Assert.AreEqual(((LargeNumberNumeric)result).Value.GetSign(), Sign.sNegative);
        }

        [TestMethod]
        public void AndOperatorTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("1&2");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));
            Assert.AreEqual(0, ((IntNumeric)result).Value);
        }

        [TestMethod]
        public void FloatPowerTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("-1.0^2");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(1, ((FloatNumeric)result).RealValue, EPSILON);
        }

        [TestMethod]
        public void VectorLengthTest1()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Length((1,2,3))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(System.Math.Pow(1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0, 1.0 / 3.0), ((FloatNumeric)result).RealValue, EPSILON);
        }

        [TestMethod]
        public void VectorLengthTest2()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Length((3,3,3),(4,5,6))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(System.Math.Pow(1.0 * 1.0 + 2.0 * 2.0 + 3.0 * 3.0, 1.0 / 3.0), ((FloatNumeric)result).RealValue, EPSILON);
        }

        [TestMethod]
        public void VectorAddTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("(1, 2) + (3, 4)");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));

            var vector = result as VectorNumeric;

            Assert.IsInstanceOfType(vector[0], typeof(IntNumeric));
            Assert.IsInstanceOfType(vector[1], typeof(IntNumeric));

            Assert.AreEqual(4, ((IntNumeric)vector[0]).Value);
            Assert.AreEqual(6, ((IntNumeric)vector[1]).Value);
        }

        [TestMethod]
        public void VectorSubtractTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("(1, 2) - (3, 4)");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));

            var vector = result as VectorNumeric;

            Assert.IsInstanceOfType(vector[0], typeof(IntNumeric));
            Assert.IsInstanceOfType(vector[1], typeof(IntNumeric));

            Assert.AreEqual(-2, ((IntNumeric)vector[0]).Value);
            Assert.AreEqual(-2, ((IntNumeric)vector[1]).Value);
        }

        [TestMethod]
        public void VectorByScalarMultiplyTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("(1, 2) * 2");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));

            var vector = result as VectorNumeric;

            Assert.IsInstanceOfType(vector[0], typeof(IntNumeric));
            Assert.IsInstanceOfType(vector[1], typeof(IntNumeric));

            Assert.AreEqual(2, ((IntNumeric)vector[0]).Value);
            Assert.AreEqual(4, ((IntNumeric)vector[1]).Value);
        }

        [TestMethod]
        public void VectorByScalarDivideTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("(2, 4) / 2");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));

            var vector = result as VectorNumeric;

            Assert.IsInstanceOfType(vector[0], typeof(IntNumeric));
            Assert.IsInstanceOfType(vector[1], typeof(IntNumeric));

            Assert.AreEqual(1, ((IntNumeric)vector[0]).RealValue, EPSILON);
            Assert.AreEqual(2, ((IntNumeric)vector[1]).RealValue, EPSILON);
        }

        [TestMethod]
        public void DotProductTest()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Dot((1, 2), (3, 4))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(IntNumeric));

            Assert.AreEqual(11, ((IntNumeric)result).Value);
        }

        [TestMethod]
        public void ProjectTest1()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Project((2, 0), (0, 0), (2, 2))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));

            var vector = result as VectorNumeric;

            Assert.IsInstanceOfType(vector[0], typeof(FloatValueNumeric));
            Assert.IsInstanceOfType(vector[1], typeof(FloatValueNumeric));

            Assert.AreEqual(1, ((FloatValueNumeric)vector[0]).RealValue, EPSILON);
            Assert.AreEqual(1, ((FloatValueNumeric)vector[1]).RealValue, EPSILON);
        }

        [TestMethod]
        public void ProjectTest2()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Project((3, 1), (1, 1), (2, 2))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(VectorNumeric));

            var vector = result as VectorNumeric;

            Assert.IsInstanceOfType(vector[0], typeof(FloatValueNumeric));
            Assert.IsInstanceOfType(vector[1], typeof(FloatValueNumeric));

            Assert.AreEqual(2, ((FloatValueNumeric)vector[0]).RealValue, EPSILON);
            Assert.AreEqual(2, ((FloatValueNumeric)vector[1]).RealValue, EPSILON);
        }

        [TestMethod]
        public void VectorToLineDistanceTest1()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Distance((2, 0), (0, 0), (2, 2))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(1.4142, ((FloatNumeric)result).Value, 0.0001);
        }

        [TestMethod]
        public void VectorToLineDistanceTest2()
        {
            // Arrange

            // Act

            var compiled = core.Compile("Distance((3, 1), (1, 1), (2, 2))");
            var result = core.Execute(compiled);

            // Assert

            Assert.IsInstanceOfType(result, typeof(FloatNumeric));
            Assert.AreEqual(1.4142, ((FloatNumeric)result).Value, 0.0001);
        }
    }
}
